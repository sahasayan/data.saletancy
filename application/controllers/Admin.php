<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        if(!isset($_SESSION['type'])){
            return redirect('welcome','refresh');
        }
        if ($_SESSION['type'] == 's_admin') {
            return redirect('super_admin','refresh');
        }else if ($_SESSION['type'] == 'business') {
            return redirect('business','refresh');
        }else if ($_SESSION['type'] != 'admin'){
            return redirect('welcome','refresh');
        }
    }

	public function index(){
		$this->load->view('admin/home');
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */