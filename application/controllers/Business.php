<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

    function __construct() {
        parent::__construct();
        if(!isset($_SESSION['type'])){
            return redirect('welcome','refresh');
        }
        if ($_SESSION['type'] == 's_admin') {
            return redirect('super_admin','refresh');
        }else if ($_SESSION['type'] == 'admin') {
            return redirect('admin','refresh');
        }else if ($_SESSION['type'] != 'business'){
            return redirect('welcome','refresh');
        }
    }
    
	public function index(){
				$this->load->model('Lead_search_model');
				$data['query_result']= $this->Lead_search_model->get_recent_search();
				if(count($data['query_result']->result())>0){
					$data['result1'] = $data['query_result']->result()[count($data['query_result']->result())-1];
				}
				if(count($data['query_result']->result())>1){
					$data['result2'] = $data['query_result']->result()[count($data['query_result']->result())-2];
				}
				if(count($data['query_result']->result())>2){
					$data['result3'] = $data['query_result']->result()[count($data['query_result']->result())-3];
				}
				
        $this->load->view('business/recent_search', $data);
	}

    
    public function lead_lookup($var = null){    
        $this->load->helper('form'); 
        $this->load->helper("url"); 
        if ($var == null) {
            $data['query_result'] = null;
            $data['employee_data'] = null;
            $this->load->view('business/lead_lookup', $data);
        } else if($var == 'search') {
            
            //If parameter is 'search', this function will search for the company based on input
            $keyword = $this->input->post('searchBox');
            
            //Next if statement checks if the input contains characters and digits. Else it's a blank and database isn't searched.
            if (preg_match('/[a-zA-Z]/',$keyword) || preg_match('/\d/', $keyword)){
                $this->load->model('Lead_search_model');
                $data['query_result'] = $this->Lead_search_model->search_keyword($keyword);
								
								if(count($data['query_result']->result()) == 0){
										$suggested_name= $keyword;
										$suggested_emp_size= "";
										$suggested_revenue= "";
										$suggested_location= "";
										$suggested_comp_city= "";
										$suggested_comp_state= "";
										$suggested_contact= "";
										$suggested_industry= "";
										$suggested_sub_industry= "";
										$suggested_website= "";

										$this->Lead_search_model->add_company_suggestion($suggested_name, $suggested_emp_size, $suggested_revenue, $suggested_location, $suggested_comp_city, $suggested_comp_state, $suggested_contact, $suggested_industry, $suggested_sub_industry, $suggested_website);
										$data["searched_company"] = $keyword;
										$this->load->view('business/not_found', $data);
										return;
								}
                
                //If only one company matches the search query, this redirects to the view that will show only its leads.
                if(count($data['query_result']->result()) == 1){
										$_SESSION['company_searched'] = $data['query_result']->result()[0]->ID;
										$this->Lead_search_model->add_new_search($keyword, $data['query_result']->result()[0]->ID, $data['query_result']->result()[0]->name, $data['query_result']->result()[0]->website);
                    redirect('business/lead_lookup/' . $data['query_result']->result()[0]->ID);
                    return;
                }
                //Else, all matching results are displayed by loading the view with all results.
            }else{
                $data['query_result'] =null;
            }
            $data['employee_data'] = null;
            $this->load->view('business/lead_lookup', $data);
        }else {
            $this->load->model('Lead_search_model');
            $url = parse_url($_SERVER['REQUEST_URI']);
            $params = null;
            if(isset($url['query'])){
                parse_str($url['query'], $params);
            }
            $data['query_result'] = $this->Lead_search_model->get_company_by_id($var);
						$_SESSION['company_searched'] = $data['query_result']->result()[0]->ID;
            $data['employee_data'] = $this->Lead_search_model->get_employee_data_by_company($var, $params);
            $this->load->view('business/lead_lookup', $data);
        }
    }
    
    public function search_suggestions(){  
        $this->load->model("Lead_search_model");  
  $this->load->helper("url");  
  $this->load->helper('form'); 
        // process posted form data  
        $keyword = $this->input->post('term');  
        $data['response'] = 'false'; //Set default response  
        $query = $this->Lead_search_model->lookup_search_suggestions($keyword); //Search DB  
        if( ! empty($query) )  
        {  
            $data['response'] = 'true'; //Set response  
            $data['message'] = array(); //Create array  
            foreach( $query as $row )  
            {  
                $data['message'][] = array(   
                                        'id'=>$row->ID,  
                                        'value' => $row->name,  
                                        '' 
                                     );  //Add a row to array  
            }  
        }  

            echo json_encode($data); //echo json string if ajax request  
    }  
    
    public function list_maker(){
				$this->db->select('*');
				$this->db->from('leads_count');
				$data['result']=$this->db->get();
        $this->load->view('business/list_maker', $data);
	}
	
		public function list_maker_dashboard(){
				$this->load->model("List_maker_model");
				$data['query_result'] = $this->List_maker_model->get_past_downloads();
        $this->load->view('business/list_maker_dashboard', $data);
	}
   
    public function result_list(){
				$this->load->library('session');
			
        $this->load->model("List_maker_model");
        $emp_size = $this->input->post('emp_size_final_confirm'); 
        $funding = $this->input->post('funding_final_confirm'); 
        $comp_type = $this->input->post('comp_type_final_confirm'); 
        $sub_industry = $this->input->post('sub_industry_final_confirm'); 
        $department = $this->input->post('department_final_confirm'); 
        $location = $this->input->post('location_final_confirm'); 
			
				$query_result = $this->List_maker_model->make_list($emp_size, $funding, $comp_type, $sub_industry, $department, $location);
				$data['query_result'] = $query_result;
				$data['remaining_cred'] = $this->List_maker_model->get_rem_credits()->credits;
				$this->session->set_userdata('result_list',$query_result->result_array());
				
				$this->session->set_userdata('emp_size_filtered',$emp_size);
				$this->session->set_userdata('funding_filtered',$funding);
				$this->session->set_userdata('comp_type_filtered',$comp_type);
				$this->session->set_userdata('sub_ind_filtered',$sub_industry);
				$this->session->set_userdata('dept_filtered',$department);
				$this->session->set_userdata('loc_filtered',$location);
			
				echo count($query_result->result_array());
        
    }
	
		public function get_matching_leads(){
				$this->load->library('session');
			
        $this->load->model("List_maker_model");
        $emp_size = $this->input->post('emp_size_final_confirm'); 
        $funding = $this->input->post('funding_final_confirm'); 
        $comp_type = $this->input->post('comp_type_final_confirm'); 
        $sub_industry = $this->input->post('sub_industry_final_confirm'); 
        $department = $this->input->post('department_final_confirm'); 
        $location = $this->input->post('location_final_confirm'); 
			
				$query_result = $this->List_maker_model->make_list($emp_size, $funding, $comp_type, $sub_industry, $department, $location);
			
				echo count($query_result->result_array());
		}
    
		public function result_list_download(){
				ob_start();
				$this->load->helper('download');
				$this->load->library('session');
			
				$this->load->model("List_maker_model");
			
				$title = ['Emp ID', 'Company ID', 'Company Name', 'Employee Size', 'Revenue', 'Address', 'City', 'Industry', 'Sub Industry', 'Website',
								 'First Name', 'Last Name', 'Designation', 'LinkedIn', 'Email', 'Contact'];
			
				$data = $this->session->userdata('result_list');
        $number = $this->input->post('number_of_leads_download');

				$i=0;
				$fp = fopen('php://output', 'w');
				fputcsv($fp, $title);
			
				if($number > $this->List_maker_model->get_rem_credits()->credits){
					$number = $this->List_maker_model->get_rem_credits()->credits;
				}
			
				if($number > count($data)){
					$number = count($data);
				}
			
        foreach($data as $key) {
            fputcsv($fp,$key);
						$i++;
						if($i>=$number){
							break;
						}
        	}  
																 
				$this->List_maker_model->set_rem_credits($number);
				
				$this->List_maker_model->set_past_downloads(count($data), $number);
					
				$file_data = file_get_contents('php://output'); 
				$file_name = 'data.csv';
				header('Pragma: public');     // required
				header('Expires: 0');         // no cache
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Cache-Control: private',false);
				header('Content-Disposition: attachment; filename="'.basename($file_name).'"');  // Add the file name
				header('Content-Transfer-Encoding: binary');
				header('Connection: close');
				exit();
			
				$_SESSION['downloading']="true";
				
				force_download($file_name, $file_data);
				fclose($fp);
				$this->session->set_userdata('result_list','');
		}
	
	
	
	
		public function download_old_list(){
				$this->load->library('session');
			
        $this->load->model("List_maker_model");
        $emp_size = $this->input->post('emp_size_final_confirm'); 
        $funding = $this->input->post('funding_final_confirm'); 
        $comp_type = $this->input->post('comp_type_final_confirm'); 
        $sub_industry = $this->input->post('sub_industry_final_confirm'); 
        $department = $this->input->post('department_final_confirm'); 
        $location = $this->input->post('location_final_confirm'); 
			
				$query_result = $this->List_maker_model->make_list($emp_size, $funding, $comp_type, $sub_industry, $department, $location);
				$data = $query_result->result_array();
				$credits = $this->List_maker_model->get_rem_credits()->credits;
				ob_start();
				$this->load->helper('download');
				$this->load->library('session');
			
				$this->load->model("List_maker_model");
			
				$title = ['Emp ID', 'Company ID', 'Company Name', 'Employee Size', 'Revenue', 'Address', 'City', 'Industry', 'Sub Industry', 'Website',
								 'First Name', 'Last Name', 'Designation', 'LinkedIn', 'Email', 'Contact'];
			
        $start_number = $this->input->post('start_number');
        $end_number = $this->input->post('end_number');
			
				$number = (int)$end_number - (int)$start_number;
				$i=0;
				$fp = fopen('php://output', 'w');
				fputcsv($fp, $title);
			
				if($number > $this->List_maker_model->get_rem_credits()->credits){
					$number = $this->List_maker_model->get_rem_credits()->credits;
				}
			
				if($number > count($data)){
					$number = count($data);
				}
			
				if($number > $credits){
					$number = $credits;
				}
				
        foreach($data as $key) {
						if($i<$start_number){
							$i++;
							continue;
						}
						
            fputcsv($fp,$key);
						$i++;
						if($i>=(int)$number + (int)$start_number){
							break;
						}
        	}  
																 
				$this->List_maker_model->set_rem_credits($number);
				
					
				$file_data = file_get_contents('php://output'); 
				$file_name = 'data.csv';
				header('Pragma: public');     // required
				header('Expires: 0');         // no cache
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Cache-Control: private',false);
				header('Content-Disposition: attachment; filename="'.basename($file_name).'"');  // Add the file name
				header('Content-Transfer-Encoding: binary');
				header('Connection: close');
				exit();
			
				$_SESSION['downloading']="true";
				
				force_download($file_name, $file_data);
				fclose($fp);
        
    }
	
		public function delete_past_list($id){
			$this->load->model("List_maker_model");
			$this->List_maker_model->delete_past_list($id);
			redirect('business/list_maker_dashboard');
		}

    public function suggest(){
        $id= $this->input->post('id');
        $field_edit= $this->input->post('field_edit');
        $new_value= $this->input->post('new_value');
        $this->load->model('Lead_search_model');
        $this->Lead_search_model->add_suggestion($id, $field_edit, $new_value);
        $data['query_result'] = $this->Lead_search_model->get_company_by_id($id);
        redirect('business/lead_lookup/'.$id);
	}
    
    public function add_company_suggestion(){
        $this->load->model('Lead_search_model');
        
        $suggested_name= $this->input->post('suggested_name');
        $suggested_emp_size= $this->input->post('suggested_emp_size');
        $suggested_revenue= $this->input->post('suggested_revenue');
        $suggested_location= $this->input->post('suggested_location');
        $suggested_comp_city= $this->input->post('suggested_comp_city');
        $suggested_comp_state= $this->input->post('suggested_comp_state');
        $suggested_contact= $this->input->post('suggested_contact');
        $suggested_industry= $this->input->post('suggested_industry');
        $suggested_sub_industry= $this->input->post('suggested_sub_industry');
        $suggested_website= $this->input->post('suggested_website');
        
        $this->Lead_search_model->add_company_suggestion($suggested_name, $suggested_emp_size, $suggested_revenue, $suggested_location, $suggested_comp_city, $suggested_comp_state, $suggested_contact, $suggested_industry, $suggested_sub_industry, $suggested_website);
        
        redirect('business/lead_lookup');
    }
	
		
	
	
		public function get_more_credits(){
				$this->load->view('business/credits');
		}

}

/* End of file Business.php */
/* Location: ./application/controllers/Business.php */