<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


public function user_login()
   {

   	    $this->load->library('form_validation');

		    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|max_length[19]');
    

         if ($this->form_validation->run() ) {
           function test_input($data) {
           $data = trim($data);
           $data = stripslashes($data);
           $data = htmlspecialchars($data);
           return $data;
           }

         $email = test_input($this->input->post('email') );    
        
         $pass = test_input($this->input->post('password') );  
            
          $psd  = md5($pass) ;



            $this->load->model('Login_model');

           if($this->Login_model->login_valid($email,$psd)){

            $user_id = $this->Login_model->get_user_id_from_email($email);
            $user    = $this->Login_model->get_user($user_id);
               
            // set session user datas
             $_SESSION['user_id']      = (int)$user->id;
             $_SESSION['email']     = (string)$user->email;
             $_SESSION['logged_in']    = (bool)true;
             $_SESSION['type']     = (string)$user->u_type;
						 $_SESSION['user_credits']     = (string)$user->credits;


             if ($_SESSION['type'] == 's_admin') {
               return redirect('super_admin','refresh');
             }

             if ($_SESSION['type'] == 'admin') {
               return redirect('admin','refresh');
             }if ($_SESSION['type'] == 'business') {
               return redirect('business','refresh');
             }
             else{
               return redirect('welcome','refresh');
             }
            

           }else{

            return redirect('welcome');

        }

    }
           

         

}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */