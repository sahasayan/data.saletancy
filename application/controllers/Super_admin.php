<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        if(!isset($_SESSION['type'])){
            return redirect('welcome','refresh');
        }
        if ($_SESSION['type'] == 'business') {
            return redirect('business','refresh');
        }else if ($_SESSION['type'] == 'admin') {
            return redirect('admin','refresh');
        }else if ($_SESSION['type'] != 's_admin'){
            return redirect('welcome','refresh');
        }
    }

	public function index()
	{
	  $this->load->view('s_admin/home');
	}

	public function add_user()
    {
        $this->load->view('s_admin/add_user');
    }

	public function import_companies(){
			$this->load->view('s_admin/import_companies');
	}

	public function import_employees(){
			$this->load->view('s_admin/import_employees');
	}

		public function companies_csv_uploaded(){

					//get the csv file
					$file = $_FILES['csv']['tmp_name'];
					$handle = fopen($file,"r");


				$this->load->model('Add_user_model');
					//loop through the csv file and insert into database
					$i=0;
					while ($data = fgetcsv($handle,1000,",")){
							if($i==0){
								$i++;
								continue;
							}
							echo $data[0];
							if ($data[0]) {
									$this->Add_user_model->add_company($data[1],$data[2], $data[3], $data[4], $data[5], $data[6], $data[7], $data[8], $data[9], $data[10], $data[11]);
							}
					}
					redirect('super_admin/');

	}

	public function employees_csv_uploaded(){

					//get the csv file
					$file = $_FILES['csv']['tmp_name'];
					$handle = fopen($file,"r");


				$this->load->model('Add_user_model');
					//loop through the csv file and insert into database
					$i=0;
					while ($data = fgetcsv($handle,1000,",")){
							if($i==0){
								$i++;
								continue;
							}
							echo $data[0];
							if ($data[0]) {
									$this->Add_user_model->add_employee($data[1],$data[2], $data[3], $data[4], $data[5], $data[6], $data[7]);
							}
					}
					redirect('super_admin/');

	}

    public function show_suggestions(){
        $this->load->model('Lead_search_model');
        $data1 = $this->Lead_search_model->get_suggestions();
        $data = array('suggestions' => $data1);
        $this->load->view('s_admin/show_suggestions', $data);
    }

    public function apply_suggestion($id){
        $this->load->model('Lead_search_model');
        $this->Lead_search_model->apply_suggestion($id);
        $data1 = $this->Lead_search_model->get_suggestions();
        $data = array('suggestions' => $data1);
        redirect('super_admin/show_suggestions', $data);
    }

    public function delete_suggestion($id){
        $this->load->model('Lead_search_model');
        $this->Lead_search_model->delete_suggestion($id);
        $data1 = $this->Lead_search_model->get_suggestions();
        $data = array('suggestions' => $data1);
        redirect('super_admin/show_suggestions', $data);
    }


    public function add_useradmin(){

        $this->load->library('form_validation');


                $this->form_validation->set_rules('firstname', 'Firstname', 'required|alpha',
                        array('required' => 'You must provide a %s.', 'alpha' => '%s should contain alphabets only'));

                $this->form_validation->set_rules('lastname', 'Lastname', 'required|alpha',
                        array('required' => 'You must provide a %s.', 'alpha' => '%s should contain alphabets only'));


                $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[10]|alpha_numeric',
                        array('required' => 'You must provide a %s.', 'min_length' => '%s should contain atleast 5 characters',
                        'max_length' => '%s should contain atmost 10 characters', 'alpha_numeric' => '%s should contain alphabets and numbers only'));

                $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]',
                        array('required' => 'You must confirm the password', 'matches' => 'The two passwords do not match'));

                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]',
                        array('required' => 'You must provide an %s.', 'valid_email' => '%s is not valid', 'is_unique' => '%s already exist'));

                $this->form_validation->set_rules('u_type', 'UserType', 'required|alpha',
                        array('required' => 'You must provide an %s.', 'alpha' => 'You must enter a valid user type'));

                if ($this->form_validation->run() == FALSE)
                {
                        $this->load->view('s_admin/add_user');
                }else{
          $this->load->model('Add_user_model');
             $firstname= $this->input->post('firstname');
            $lastname= $this->input->post('lastname');
            $password= $this->input->post('password');
            $password1=md5($password);
						$company= $this->input->post('company');
            $jobtitle= $this->input->post('jobtitle');
            $telephone= $this->input->post('telephone');
            $email= $this->input->post('email');
            $u_type= 'admin';

           if( $this->Add_user_model->adduserdata($firstname, $lastname, $password1, $email, $u_type, $company, $jobtitle, $telephone))
           {
               return redirect('super_admin/admin_list', 'refresh');
           }
        }

    }
    public function admin_list(){
       $data1 = $this->db->get_where('user', array('u_type' => 'admin'));
       $data = array('admin' => $data1);
       $this->load->view('s_admin/admin_list', $data);
    }

    public function edit_admin($id){
        $this->load->model('Login_model');
        $data   = $this->Login_model->get_user($id);
        $this->load->view('s_admin/edit_user', $data);
    }

    public function update_admin($id){
         $this->load->library('form_validation');


        $this->form_validation->set_rules('firstname', 'Firstname', 'required|alpha',
                array('required' => 'You must provide a %s.', 'alpha' => '%s should contain alphabets only'));

        $this->form_validation->set_rules('lastname', 'Lastname', 'required|alpha',
                array('required' => 'You must provide a %s.', 'alpha' => '%s should contain alphabets only'));


        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[10]|alpha_numeric',
                array('required' => 'You must provide a %s.', 'min_length' => '%s should contain atleast 5 characters',
                'max_length' => '%s should contain atmost 10 characters', 'alpha_numeric' => '%s should contain alphabets and numbers only'));

        $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]',
                array('required' => 'You must confirm the password', 'matches' => 'The two passwords do not match'));

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email',
                array('required' => 'You must provide an %s.', 'valid_email' => '%s is not valid'));

        if ($this->form_validation->run() == FALSE){
                return redirect('super_admin/admin_list', 'refresh');
        }else{
            $this->load->model('Add_user_model');
            $firstname= $this->input->post('firstname');
            $lastname= $this->input->post('lastname');
            $company= $this->input->post('company');
            $jobtitle= $this->input->post('jobtitle');
            $telephone= $this->input->post('telephone');
            $password= $this->input->post('password');
            $password1=md5($password);
            $email= $this->input->post('email');
            $u_type= $this->input->post('u_type');
            $this->Add_user_model->update_userdata($id, $firstname, $lastname, $password1, $email, $u_type, $company, $jobtitle, $telephone);
            return redirect('super_admin/admin_list', 'refresh');
        }
    }


    public function delete_admin($id){
        $this->load->model('Login_model');
        $data   = $this->Login_model->delete_admin($id);
        return redirect('super_admin/admin_list', 'refresh');
    }





    public function show_suggested_companies(){
        $this->load->model('Lead_search_model');
        $data1 = $this->Lead_search_model->get_suggested_companies();
        $data = array('suggestions' => $data1);
        $this->load->view('s_admin/show_suggested_companies', $data);
    }

    public function add_suggested_companies($id){
        $this->load->model('Lead_search_model');
        $this->Lead_search_model->add_suggested_companies($id);
        $data1 = $this->Lead_search_model->get_suggested_companies();
        $data = array('suggestions' => $data1);
        redirect('super_admin/show_suggested_companies', $data);
    }

    public function delete_company_suggestion($id){
        $this->load->model('Lead_search_model');
        $this->Lead_search_model->delete_company_suggestion($id);
        $data1 = $this->Lead_search_model->get_suggested_companies();
        $data = array('suggestions' => $data1);
        redirect('super_admin/show_suggested_companies', $data);
    }


		public function users_list(){
       $data1 = $this->db->get_where('user', array('u_type' => 'business'));
       $data = array('admin' => $data1);
       $this->load->view('s_admin/users_list', $data);
    }

		public function block_user($id){
        $this->load->model('Login_model');
        $data   = $this->Login_model->block_user($id);
        return redirect('super_admin/users_list', 'refresh');
    }

		public function unblock_user($id){
        $this->load->model('Login_model');
        $data   = $this->Login_model->unblock_user($id);
        return redirect('super_admin/users_list', 'refresh');
    }





		public function update_leads_count(){
				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '1-10 employees');
				$result_1 = $this->db->get();
				echo (int)count($result_1->result_array()).' ';
				$data = array(
          'emp1' => (int)count($result_1->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);







				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '11-50 employees');
				$result_2 = $this->db->get();
				echo (int)count($result_2->result_array()).' ';
				$data = array(
          'emp2' => (int)count($result_2->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);




				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '51-200 employees');
				$result_3 = $this->db->get();
				echo (int)count($result_3->result_array()).' ';
				$data = array(
          'emp3' => (int)count($result_3->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);




				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '201-500 employees');
				$result_4 = $this->db->get();
				echo (int)count($result_4->result_array()).' ';
				$data = array(
          'emp4' => (int)count($result_4->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);





			$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '501-1000 employees');
				$result_5 = $this->db->get();
				echo (int)count($result_5->result_array()).' ';
				$data = array(
          'emp5' => (int)count($result_5->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);



				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '1001-5000 employees');
				$result_6 = $this->db->get();
				echo (int)count($result_6->result_array()).' ';
				$data = array(
          'emp6' => (int)count($result_6->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);





				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '5001-10,000 employees');
				$result_7 = $this->db->get();
				echo (int)count($result_7->result_array()).' ';
				$data = array(
          'emp7' => (int)count($result_7->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);





				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.emp_size', '10,001+ employees');
				$result_8 = $this->db->get();
				echo (int)count($result_8->result_array()).' ';
				$data = array(
          'emp8' => (int)count($result_8->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);










			
			
			
			
			
			
			
			
			$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '0-1 Crs');
				$result_1 = $this->db->get();
				echo (int)count($result_1->result_array()).' ';
				$data = array(
          'rev1' => (int)count($result_1->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);







				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '1-10 Crs');
				$result_2 = $this->db->get();
				echo (int)count($result_2->result_array()).' ';
				$data = array(
          'rev2' => (int)count($result_2->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);




				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '10-100 Crs');
				$result_3 = $this->db->get();
				echo (int)count($result_3->result_array()).' ';
				$data = array(
          'rev3' => (int)count($result_3->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);




				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '100-250 Crs');
				$result_4 = $this->db->get();
				echo (int)count($result_4->result_array()).' ';
				$data = array(
          'rev4' => (int)count($result_4->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);





			$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '250-500 Crs');
				$result_5 = $this->db->get();
				echo (int)count($result_5->result_array()).' ';
				$data = array(
          'rev5' => (int)count($result_5->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);



				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '500-1000 Crs');
				$result_6 = $this->db->get();
				echo (int)count($result_6->result_array()).' ';
				$data = array(
          'rev6' => (int)count($result_6->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);





				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '1000-2500 Crs');
				$result_7 = $this->db->get();
				echo (int)count($result_7->result_array()).' ';
				$data = array(
          'rev7' => (int)count($result_7->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);





				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '2500-5000 Crs');
				$result_8 = $this->db->get();
				echo (int)count($result_8->result_array()).' ';
				$data = array(
          'rev8' => (int)count($result_8->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);




				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.revenue', '5000+ Crs');
				$result_9 = $this->db->get();
				echo (int)count($result_9->result_array()).' ';
				$data = array(
          'rev9' => (int)count($result_9->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);






			
			
			






				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.industry', 'Media & Entertainment');
				$result = $this->db->get();
				echo (int)count($result->result_array()).' ';
				$data = array(
          'MediaEntertainment' => (int)count($result->result_array())
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);


				$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$this->db->where('company.industry', 'Networking & Telecommunications');  //CHANGE THE INDUSTRY NAME HERE.
				$result = $this->db->get();
				echo (int)count($result->result_array()).' ';
				$data = array(
          'NetworkingTelecommunications' => (int)count($result->result_array())  //PUT THE INDUSTRY NAME THERE WITHOUT SPACES
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);


				/*COPY PASTE THE REST SIMILARLY. TAKE INDUSTRY NAMES FROM FRONT END.* 1ST 3 LINES OF EVERY BLOCK REMAIN SAME.*/


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Energy & Utilities');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'EnergyUtilities' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'IT / ITES');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'ITITES' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Consumer Packaged Goods');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'ConsumerPackagedGoods' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Business Services');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'BusinessServices' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'BFSI');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'BFSI' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Retail & Trading');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'RetailTrading' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Metal & Mining');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'MetalMining' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Logistics & Transportation');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'LogisticsTransportation' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Aerospace & Defense');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'AerospaceDefense' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Automobile & Auto Ancillaries');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'AutomobileAutoAncillaries' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Chemicals');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Chemicals' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Electrical & Electronics');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'ElectricalElectronics' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Infrastructure');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Infrastructure' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Engineering');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Engineering' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Manufacturing & Production');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'ManufacturingProduction' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Textiles & Garments');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'TextilesGarments' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Education');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Education' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Government & NGO');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'GovernmentNGO' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Life Science');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'LifeScience' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $this->db->where('company.industry', 'Hospitality');
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Hospitality' => (int)count($result->result_array())
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);









			$this->db->select('employee.ID');
      	$this->db->from('company');
       	$this->db->join('employee', 'company.ID = employee.comp_id');
				$arrLoc = ['Ahmedabad', 'Surat', 'Vadodara', 'Mumbai', 'Pune', 'Nagpur']; //CHANGE THIS ARRAY WITH NAMES OF ALL CITIES IN THE ZONE
				$this->db->where_in('company.comp_city', $arrLoc);
				foreach($arrLoc as $aL ){
						$this->db->or_like('lower(company.address)', $aL);
				}
				$result = $this->db->get();
				echo (int)count($result->result_array()).' ';
				$data = array(
          'WestIndia' => (int)count($result->result_array())   //CHANGE THE NAME OF THE ZONE, NO SPACES
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $arrLoc = ['Bhopal', 'Indore', 'New Delhi', 'Delhi NCR']; //CHANGE THIS ARRAY WITH NAMES OF ALL CITIES IN THE ZONE
            $this->db->where_in('company.comp_city', $arrLoc);
            foreach($arrLoc as $aL ){
                $this->db->or_like('lower(company.address)', $aL);
            }
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'CentralIndia' => (int)count($result->result_array())   //CHANGE THE NAME OF THE ZONE, NO SPACES
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


//            $this->db->select('employee.ID');
//            $this->db->from('company');
//            $this->db->join('employee', 'company.ID = employee.comp_id');
//            $arrLoc = []; //CHANGE THIS ARRAY WITH NAMES OF ALL CITIES IN THE ZONE
//            $this->db->where_in('company.comp_city', $arrLoc);
//            foreach($arrLoc as $aL ){
//                $this->db->or_like('lower(company.address)', $aL);
//            }
//            $result = $this->db->get();
//            echo (int)count($result->result_array()).' ';
//            $data = array(
//                'EastIndia' => (int)count($result->result_array())   //CHANGE THE NAME OF THE ZONE, NO SPACES
//            );
//            $this->db->where('ID', 1);
//            $this->db->update('leads_count', $data);
//
//
//            $this->db->select('employee.ID');
//            $this->db->from('company');
//            $this->db->join('employee', 'company.ID = employee.comp_id');
//            $arrLoc = []; //CHANGE THIS ARRAY WITH NAMES OF ALL CITIES IN THE ZONE
//            $this->db->where_in('company.comp_city', $arrLoc);
//            foreach($arrLoc as $aL ){
//                $this->db->or_like('lower(company.address)', $aL);
//            }
//            $result = $this->db->get();
//            echo (int)count($result->result_array()).' ';
//            $data = array(
//                'NorthEastIndia' => (int)count($result->result_array())   //CHANGE THE NAME OF THE ZONE, NO SPACES
//            );
//            $this->db->where('ID', 1);
//            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $arrLoc = ['Gurgaon', 'Faridabad', 'Amritsar', 'Jaipur', 'Noida', 'Lucknow', 'Agra', 'Aligarh', 'Allahabad', 'Ballia']; //CHANGE THIS ARRAY WITH NAMES OF ALL CITIES IN THE ZONE
            $this->db->where_in('company.comp_city', $arrLoc);
            foreach($arrLoc as $aL ){
                $this->db->or_like('lower(company.address)', $aL);
            }
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'NorthIndia' => (int)count($result->result_array())   //CHANGE THE NAME OF THE ZONE, NO SPACES
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('employee.ID');
            $this->db->from('company');
            $this->db->join('employee', 'company.ID = employee.comp_id');
            $arrLoc = ['Bengaluru /Bangalore', 'Mysore', 'Vijayawada', 'Vishakhapatnam', 'Chennai', 'Coimbatore', 'Hyderabad /Secunderabad']; //CHANGE THIS ARRAY WITH NAMES OF ALL CITIES IN THE ZONE
            $this->db->where_in('company.comp_city', $arrLoc);
            foreach($arrLoc as $aL ){
                $this->db->or_like('lower(company.address)', $aL);
            }
            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'SouthIndia' => (int)count($result->result_array())   //CHANGE THE NAME OF THE ZONE, NO SPACES
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);














				$this->db->select('ID');
      	$this->db->from('employee');
				$this->db->where_in('designation', ['CMO', 'VP of Marketing', 'VP of Product Marketing', 'VP of Corporate Marketing', 'VP of Online Marketing', 'VP of public Marketing', 'VP of Content Marketing', 'Director of Marketing', 'Director of Digital Marketing', 'Director of Product Marketing', 'Director of Corporate Marketing', 'Director of Public Relations', 'Director of Content Marketing', 'Director of Online Marketing', 'Digital Marketing Manager', 'Online Marketing Manager', 'Product Marketing Manager', 'Public Relations Manager', 'Social Media Manager']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

				$result = $this->db->get();
				echo (int)count($result->result_array()).' ';
				$data = array(
          'Marketing' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
        );
				$this->db->where('ID', 1);
				$this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['Head of Sales', 'VP of Sales', 'VP of Sales Development', 'Director of Inside Sales', 'Director of Sales', 'Director of Sales Development', 'Sales Manager', 'SDR Manager', 'ISR Manager', 'Account Executive', 'Account Manager']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Sales' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['CIO', 'CISO', 'VP of IT', 'VP of Cloud Service', 'VP of IT infrastructure', 'Sys Admin', 'Director of IT', 'IT Manager']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'It' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['CPO', 'VP of Product', 'Director of Product', 'VP of Cloud Service']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Product' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['CTO', 'VP of Engineering', 'VP of Infrastructure', 'VP of Data Centers', 'Director of Engineering', 'Engineering Manager', 'Devops', 'SRE', 'System Engineer', 'Infrastructure Engineer', 'Cloud Engineer']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'EngineeringDept' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['CEO', 'Founder']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Founders' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['Chef People Office', 'Chef Talent Office', 'Head of HR', 'VP of HR', 'VP of Recruiting', 'VP of Acquisition', 'Head of Recruiting', 'Director of Talent Acquisition', 'Director fo Recruiting', 'Director of people', 'Director of Talent', 'Lead Recruiter', 'HR Director', 'HR Manager', 'Director of People Analytics']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'HR' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['CFO', 'VP of Finance', 'Director of Finance', 'VP of Payments', 'VP of Corporate Finance', 'Director fo Corporate Finance', 'Procurement', 'Accounting', 'Director of Recruiting', 'Finance System']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Finance' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);


            $this->db->select('ID');
            $this->db->from('employee');
            $this->db->where_in('designation', ['COO', 'VP of Operations', 'Director of Operations', 'Operations Manager']);  //CHANGE THE ARRAY WITH THE DESIGNATIONS UNDER THE SECTION

            $result = $this->db->get();
            echo (int)count($result->result_array()).' ';
            $data = array(
                'Operations' => (int)count($result->result_array())   //CHANGE THE SECTION NAME
            );
            $this->db->where('ID', 1);
            $this->db->update('leads_count', $data);
		}

        public function reset_credits()
        {
            $this->db->query('UPDATE user SET credits = ?', [
                500
            ]);

            echo 'done';
        }
}

/* End of file Super_admin.php */
/* Location: ./application/controllers/Super_admin.php */