<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function index()
    {
        $this->load->helper('form');
        $this->load->view('public/data_home');
    }

    public function signup()
    {

        $this->load->view('public/signup');
    }

    public function login()
    {
        $this->load->view('public/login');
    }

    public function register_user()
    {
        $this->load->library('form_validation');

        $url = parse_url($_SERVER['REQUEST_URI']);

        $referral_id = null;
        if (isset($url['query'])) {
            $referral_id = substr($url['query'], 1);
        }


        $this->form_validation->set_rules('firstname', 'Firstname', 'required|alpha',
            array('required' => 'You must provide a %s.', 'alpha' => '%s should contain alphabets only'));

        $this->form_validation->set_rules('lastname', 'Lastname', 'required|alpha',
            array('required' => 'You must provide a %s.', 'alpha' => '%s should contain alphabets only'));


        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]|max_length[10]|alpha_numeric',
            array('required' => 'You must provide a %s.', 'min_length' => '%s should contain atleast 5 characters',
                'max_length' => '%s should contain atmost 10 characters', 'alpha_numeric' => '%s should contain alphabets and numbers only'));

        $this->form_validation->set_rules('cpassword', 'Password Confirmation', 'required|matches[password]',
            array('required' => 'You must confirm the password', 'matches' => 'The two passwords do not match'));

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]|callback_business_email',
            array('required' => 'You must provide an %s.', 'valid_email' => '%s is not valid', 'is_unique' => '%s already exist'));

        $this->form_validation->set_rules('telephone', 'Telephone', 'required|numeric|min_length[10]|max_length[10]',
            array('required' => 'You must provide a %s.', 'numeric' => '%s should contain numbers only', 'min_length' => '%s should be exxactly 10 digits', 'max_length' => '%s should exactly 10 digits'));

        if ($this->form_validation->run() == FALSE) {
            echo json_encode($this->form_validation->error_array());
        } else {
            $this->load->model('add_user_model');
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $company = $this->input->post('company');
            $jobtitle = $this->input->post('jobtitle');
            $telephone = $this->input->post('telephone');
            $password = $this->input->post('password');
            $password1 = md5($password);
            $u_type = 'business';
            $this->add_user_model->adduserdata($firstname, $lastname, $password1, $email, $u_type, $company, $jobtitle, $telephone);
            if ($referral_id != null) {
                $this->add_user_model->addusercredits($referral_id, 25);
            }
            redirect('welcome', 'refresh');
        }
    }


    public function business_email($str)
    {
        $str = substr($str, strpos($str, "@") + 1);
        $arr = ["gmail.com", "yahoo.com", "hotmail.com",
            "aol.com", "hotmail.co.uk", "hotmail.fr", "msn.com", "yahoo.fr", "wanadoo.fr", "orange.fr", "comcast.net", "yahoo.co.uk", "yahoo.com.br",
            "yahoo.co.in", "live.com", "rediffmail.com", "free.fr", "gmx.de", "web.de", "yandex.ru", "ymail.com", "libero.it", "outlook.com", "uol.com.br",
            "bol.com.br", "mail.ru", "cox.net", "hotmail.it", "sbcglobal.net", "sfr.fr", "live.fr", "verizon.net", "live.co.uk", "googlemail.com",
            "yahoo.es", "ig.com.br", "live.nl", "bigpond.com", "terra.com.br", "yahoo.it", "neuf.fr", "yahoo.de", "alice.it", "rocketmail.com"];
        foreach ($arr as $i) {
            if ($str == $i) {
                $this->form_validation->set_message('business_email', 'The {field} field has to be business email');
                return FALSE;
            }
        }

        return TRUE;
    }


    public function verify_email($email_code)
    {
        $data = array(
            'is_blocked' => "false",
        );
        $this->db->where('md5(email)', $email_code);
        $this->db->update('user', $data);
        redirect('welcome');
    }

    public function forgot_password()
    {
        $email = $this->input->get('email') ?: '';

        if (empty($email)) {
            echo json_encode([
                'error' => true,
                'message' => 'Please enter your registered mail address'
            ]);

            return;
        }

        $user = $this->db->query('SELECT * FROM user WHERE email = ? LIMIT 1', [
            $email
        ])->result_array();

        if (empty($user)) {
            echo json_encode([
                'error' => true,
                'message' => $email . ' is not registered with us'
            ]);

            return;
        }

        // update DB
        $user = $user[0];

        $forgot_password_token = rand(9999, 999999);

        $this->db->query('UPDATE user SET forgot_password_token = ? WHERE id = ?', [
            $forgot_password_token,
            $user['id']
        ]);

        // send mail
        $this->load->library('email');

        $from = "support@saletancy.info";

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.sendgrid.net';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'apikey';
        $config['smtp_pass'] = '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';
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = 'TRUE';
        $config['newline'] = "\r\n";

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from($from);
        $this->email->to($user['email']);
        $this->email->subject('Forgot password');


        $message = '<p>Dear ' . $user['firstname'] . ',</p>';
        $message .= '<br>';
        $message .= '<a href="' . base_url('Welcome?token=' . $forgot_password_token . '&email=' . $user['email'] . '#changePassword') . '">click here</a> to continue';

        $this->email->message($message);
        $this->email->send();

        echo json_encode([
            'error' => false,
            'message' => 'Please check you inbox'
        ]);
    }

    public function change_password()
    {
        $token = $this->input->post('token');
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if (empty($token) || empty($email) || $token === null || $email === null) return;

        if (empty($password)) {
            echo json_encode([
                'error' => true,
                'message'  => 'Enter a password'
            ]);

            return;
        }

        $this->db->query('UPDATE user SET password = ? WHERE email = ? AND forgot_password_token = ?', [
            md5($password),
            $email,
            $token
        ]);

        if ($this->db->affected_rows() > 0) {
            $this->db->query('UPDATE user SET forgot_password_token = ? WHERE email = ?', [
                '',
                $email,
            ]);

            echo json_encode([
                'error' => false,
                'message' => 'Password has been changed successfully'
            ]);
        } else {
            echo json_encode([
                'error' => true,
                'message' => 'You have already changed the password'
            ]);
        }
    }

}
