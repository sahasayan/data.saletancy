<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_search_model extends CI_Model {

    
  public function search_keyword($keyword){
      $this->db->from('company');
    $this->db->like('name', $keyword);
    return $this->db->get();
  }
    public function get_company_by_id($id){
         $this->db->from('company');
        $this->db->where('id', $id);
        return $this->db->get();
    }
    public function get_employee_data_by_company($id, $params){
        if($params){
            $this->db->from('employee');
            $this->db->where('comp_id', $id);
            $array = array('designation' => $params['filter']);
            $this->db->like($array);
            return $this->db->get();
        }else{
            $this->db->from('employee');
            $this->db->where('comp_id', $id);
            return $this->db->get();
        }
    }
    
    public function lookup_search_suggestions($keyword){ 
         $this->db->select('*')->from('company'); 
        $this->db->like('name',$keyword,'after'); 
        //$this->db->or_like('iso',$keyword,'after'); 
        $query = $this->db->get();     
        return $query->result(); 
    } 
    
    public function get_suggestions(){
        $query = $this->db->select('suggestion.ID, suggestion.comp_id, company.name, company.emp_size, company.revenue, company.address, 
        company.industry, company.website, suggestion.field_edit, suggestion.new_value ')
                  ->from('suggestion')
                  ->join('company', 'company.ID = suggestion.comp_id')
                  ->get();
        return $query;
    }
    
    public function apply_suggestion($id){
        $this->db->from('suggestion');
        $this->db->where('ID', $id);
        $row = $this->db->get()->row();
        $this->db->where('ID', $row->comp_id);
        $this->db->update('company', array($row->field_edit => $row->new_value));
        $this->db->where('ID', $id);
        $this->db->delete('suggestion'); 
    }
    
    public function delete_suggestion($id){
        $this->db->where('ID', $id);
        $this->db->delete('suggestion'); 
    }
    
    public function add_suggestion($id, $field_edit, $new_value){
        $data = array(
          'comp_id' => $id ,
          'field_edit' => $field_edit ,
          'new_value' => $new_value
        );
            $this->db->insert('suggestion', $data);
            return true;
    }
    
    public function add_company_suggestion($suggested_name, $suggested_emp_size, $suggested_revenue, $suggested_location, $suggested_comp_city, $suggested_comp_state, $suggested_contact, $suggested_industry, $suggested_sub_industry, $suggested_website) {
        $data = array(
          'name' => $suggested_name ,
          'emp_size' => $suggested_emp_size ,
          'revenue' => $suggested_revenue,
          'address' => $suggested_location ,
          'comp_city' => $suggested_comp_city ,
          'comp_state' => $suggested_comp_state ,
          'contact' => $suggested_contact ,
          'industry' => $suggested_industry,
          'sub_industry' => $suggested_sub_industry ,
          'website' => $suggested_website
        );
        
        $this->db->insert('suggested_comp', $data);
    }
    
    
    public function get_suggested_companies(){
        $query = $this->db->select('ID, name, emp_size, revenue, address, comp_city, comp_state, contact, industry, sub_industry, website ')
                  ->from('suggested_comp')
                  ->get();
        return $query;
    }
    
    public function add_suggested_companies($id){
        $this->db->from('suggested_comp');
        $this->db->where('ID', $id);
        $row = $this->db->get()->row();
        
        $data = array(
          'name' => $row->name ,
          'emp_size' => $row->emp_size ,
          'revenue' => $row->revenue,
          'address' => $row->address ,
          'comp_city' => $row->comp_city ,
          'comp_state' => $row->comp_state ,
          'contact' => $row->contact ,
          'industry' => $row->industry,
          'sub_industry' => $row->sub_industry ,
          'website' => $row->website
        );
        
        $this->db->insert('company', $data);
        
        $this->db->where('ID', $id);
        $this->db->delete('suggested_comp'); 
    }
    
    public function delete_company_suggestion($id){
        $this->db->where('ID', $id);
        $this->db->delete('suggested_comp'); 
    }
    
    public function add_new_search($keyword, $comp_id, $comp_name, $website){
       
        $data = array(
          'user_id' =>  $_SESSION['user_id'],
          'keyword' =>  $keyword,
          'comp_id' =>  $comp_id,
          'comp_name' => $comp_name ,
          'website' => $website
        );
        
        $this->db->insert('recent_search', $data);
         
    }
	
		public function get_recent_search(){ 
         $this->db->select('*')->from('recent_search'); 
        $this->db->where('user_id', $_SESSION['user_id']); 
        //$this->db->or_like('iso',$keyword,'after'); 
        $query = $this->db->get();     
        return $query; 
    } 
}

