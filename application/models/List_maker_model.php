<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_maker_model extends CI_Model {

    
  public function make_list($emp_size, $revenue, $comp_type, $sub_industry, $department, $location){
      
			$low_emp_size = substr($emp_size, 0, strpos($emp_size, '-'));
			$high_emp_size = substr($emp_size, strpos($emp_size, '-')+1, strlen($emp_size)-1);
		
			$emp_size_range = ['1', '10', '50', '200', '500', '1000', '5000', '10000', '20000'];
			$i=0;
			foreach ($emp_size_range as $e){
					if ($e == $low_emp_size){
							$low_emp_size = $i;
							break;
					}
					$i++;
			}
			$i=0;
			foreach ($emp_size_range as $e){
					if ($e == $high_emp_size){
							$high_emp_size = $i;
							break;
					}
					$i++;
			}
			$emp_size_arr = [];
			for ($i=$low_emp_size; $i<$high_emp_size; $i++){
					if($emp_size_range[$i+1] == '20000'){
							array_push($emp_size_arr, '10,001+ employees');
					}else if($emp_size_range[$i+1] == '10000'){
							array_push($emp_size_arr, '5001-10,000 employees');
					}else if($emp_size_range[$i] == '1'){
						array_push($emp_size_arr, $emp_size_range[$i].'-'.$emp_size_range[$i+1].' employees');
					}else{
							array_push($emp_size_arr, (string)((int)$emp_size_range[$i]+1).'-'.$emp_size_range[$i+1].' employees');
					}
			}
		
		
		
			$low_rev = substr($revenue, 0, strpos($revenue, '-'));
			$high_rev = substr($revenue, strpos($revenue, '-')+1, strlen($revenue)-1);
		
			$rev_range = ['1', '1', '10', '100', '250', '500', '1000', '2500', '5000', '10000'];
			$i=0;
			foreach ($rev_range as $r){
					if ($r == $low_rev){
							$low_rev = $i;
							break;
					}
					$i++;
			}
			$i=0;
			foreach ($rev_range as $r){
					if ($r == $high_rev){
							$high_rev = $i;
							break;
					}
					$i++;
			}
		
			$rev_arr = [];
			for ($i=$low_rev; $i<$high_rev; $i++){
					if($rev_range[$i+1] == '10000'){
							array_push($rev_arr, '5000+ Crs');
					}else{
							array_push($rev_arr, $rev_range[$i].'-'.$rev_range[$i+1].' Crs');
					}
			}
		
			$states= [];
		
			if($location != ''){
				$arrLocTemp = explode(', ', strtolower($location));
				if (array_key_exists("Pune", $arrLocTemp) && array_key_exists("Mumbai", $arrLocTemp)  && array_key_exists("Nagpur", $arrLocTemp)) {
    				$states.push("Maharashtra");
				}
				if (array_key_exists("Ahmedabad", $arrLocTemp) && array_key_exists("Surat", $arrLocTemp)  && array_key_exists("Vadodra", $arrLocTemp)) {
    				$states.push("Gujarat");
				}
				if (array_key_exists("Bhopal", $arrLocTemp) && array_key_exists("Indore", $arrLocTemp)) {
    				$states.push("Madhya Pradesh");
				}
				if (array_key_exists("New Delhi", $arrLocTemp) && array_key_exists("Delhi NCR", $arrLocTemp)) {
    				$states.push("Delhi");
				}
				if (array_key_exists("Gurgaon", $arrLocTemp) && array_key_exists("Faridabad", $arrLocTemp)) {
    				$states.push("Haryana");
				}
				if (array_key_exists("Amritsar", $arrLocTemp)) {
    				$states.push("Punjab");
				}
				if (array_key_exists("Jaipur", $arrLocTemp)) {
    				$states.push("Rajasthan");
				}
				if (array_key_exists("Noida", $arrLocTemp) && array_key_exists("Ballia", $arrLocTemp)  && array_key_exists("Lucknow", $arrLocTemp) && array_key_exists("Agra", $arrLocTemp) && array_key_exists("Aligarh", $arrLocTemp)  && array_key_exists("Allahabad", $arrLocTemp)) {
    				$states.push("Uttar Pradesh");
				}
				if (array_key_exists("Bengaluru /Bangalore", $arrLocTemp) && array_key_exists("Mysore", $arrLocTemp)) {
    				$states.push("Karnataka");
				}
				if (array_key_exists("Vijayawada", $arrLocTemp) && array_key_exists("Vishakhapatnam", $arrLocTemp)) {
    				$states.push("Andhara Pradesh");
				}
				if (array_key_exists("Chennai", $arrLocTemp) && array_key_exists("Coimbatore", $arrLocTemp)) {
    				$states.push("Tamil Nadu");
				}
				if (array_key_exists("Hyderabad / Secunderabad", $arrLocTemp)) {
    				$states.push("Telangana");
				}
			}
      
      $this->db->select('employee.ID, employee.comp_id, company.name, company.emp_size, company.revenue, company.address, company.comp_city, 
        company.industry, company.sub_industry, company.website, employee.firstname, employee.lastname, employee.designation, employee.linkedin, employee.email, employee.contact');
      
      $this->db->from('company');
      
       $this->db->join('employee', 'company.ID = employee.comp_id');
      
      if($emp_size != ''){
       $this->db->where_in('company.emp_size', $emp_size_arr);
      }
      if($revenue != ''){
       $this->db->where_in('company.revenue', $rev_arr);
      }
     if($comp_type != null){
         $arrCompType = explode(', ', $comp_type);
         $this->db->where_in('company.comp_type', $arrCompType);
     }
      if($location != ''){
          $arrLoc = explode(', ', strtolower($location));
          $this->db->where_in('lower(company.comp_city)', $arrLoc);
					foreach($arrLoc as $aL ){
						$this->db->or_like('lower(company.address)', $aL);
						$this->db->or_like('lower(company.comp_state)', $aL);
					}
					foreach($states as $s ){
						$this->db->or_like('lower(company.comp_state)', strtolower($a));
					}
     }
      if($department != ''){
          $arrDept = explode(', ', strtolower($department));
          $this->db->where_in('lower(employee.designation)', $arrDept);
     }
      if($sub_industry != ''){
          $arrSubInd = explode(', ', $sub_industry);
          $this->db->where_in('company.sub_industry', $arrSubInd);
     }
       
       
       $query = $this->db->get();
		
		
        return $query;
  }
	
	public function set_past_downloads($total, $downloaded){
			$data = array(
					'user_id' => $_SESSION['user_id'] ,
					'emp_size_filter' => $this->session->userdata('emp_size_filtered') ,
					'comp_type_filter' => $this->session->userdata('comp_type_filtered'),
					'revenue_filter' => $this->session->userdata('funding_filtered') ,
					'location_filter' => $this->session->userdata('loc_filtered') ,
					'sub_industry_filter' => $this->session->userdata('sub_ind_filtered') ,
					'designation_filter' => $this->session->userdata('dept_filtered'),
					'total_leads' => $total ,
					'downloaded_leads' => $downloaded
				);
		
				$this->db->insert('past_downloads', $data);
		
				$this->session->unset_userdata('emp_size_filtered');
				$this->session->unset_userdata('comp_type_filtered');
				$this->session->unset_userdata('funding_filtered');
				$this->session->unset_userdata('loc_filtered');
				$this->session->unset_userdata('sub_ind_filtered');
				$this->session->unset_userdata('dept_filtered');
				
			return;
	}
	
	public function get_past_downloads(){
		$this->db->select('*');
			$this->db->from('past_downloads');
			$this->db->where('user_id', $_SESSION['user_id'] );
			return $this->db->get();
	}
	
	public function delete_past_list($id){
			$this->db->where('id', $id);
        $this->db->delete('past_downloads'); 
	}
	
	public function get_rem_credits(){
			$this->db->select('credits');
			$this->db->from('user');
			$this->db->where('id', $_SESSION['user_id'] );
			return $this->db->get()->row();
	}
	
	public function set_rem_credits($sub_cred){
		
			$this->load->model('List_maker_model');
		
			$curr_cred = $this->List_maker_model->get_rem_credits()->credits;
			
			$new_cred = array('credits' =>  $curr_cred - $sub_cred);
		
			$this->db->where('id', $_SESSION['user_id']);
      $this->db->update('user', $new_cred);
			$_SESSION['user_credits']=(string)$new_cred['credits'];
			return;
	}
}

