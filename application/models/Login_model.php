<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

   public function login_valid($email,$psd)
   {
       
        $this->db->select('id');
    $this->db->from('user');
    $this->db->where(['email'=>$email, 'password'=>$psd]);
   $q2 = $this->db->get()->row('id');

        if ( $q2 ) {
						$this->load->model('Login_model');
						$block_status = $this->Login_model->get_blocked_status($q2);
						
						if($block_status == "true"){
							return FALSE;
						}

            return $q2;
            
         }else{

            return FALSE;
         }
   }

		public function get_blocked_status($id){
				$this->db->select('is_blocked');
    		$this->db->from('user');
				$this->db->where('id', $id);
				return $this->db->get()->row('is_blocked');
		}

    public function get_user_id_from_email($email)
   {
     
     $this->db->select('id');
    $this->db->from('user');
    $this->db->where('email', $email);
    return $this->db->get()->row('id');

   }


    public function get_user($user_id) {
    
    $this->db->from('user');
    $this->db->where('id', $user_id);
    return $this->db->get()->row();
    
  }
  
    public function delete_admin($id){
        $this->db->where('id', $id);
        $this->db->delete('user'); 
    }
	
		 public function block_user($id){
        $data = array(
          'is_blocked' => "true"
        );
            $this->db->where('id', $id);
            $this->db->update('user', $data);
    }
	
		public function unblock_user($id){
        $data = array(
          'is_blocked' => "false"
        );
            $this->db->where('id', $id);
            $this->db->update('user', $data);
    }
	

}

/* End of file login_model.php */
/* Location: ./application/models/login_model.php */