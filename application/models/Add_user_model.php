<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_user_model extends CI_Model
{
    public function adduserdata($firstname, $lastname, $password1, $email, $u_type, $company, $jobtitle, $telephone)
    {
        $data = array(
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => $password1,
            'email' => $email,
            'u_type' => $u_type,
            'company' => $company,
            'job_title' => $jobtitle,
            'telephone' => $telephone,
            'date' => date('Y-m-j H:i:s'),
            'is_blocked' => "true",
            'credits' => 500
        );
        $this->db->insert('user', $data);
        $this->load->model('Add_user_model');
        $this->Add_user_model->send_validation_email($firstname, $email);
        return true;

    }

    public function send_validation_email($firstname, $email)
    {
        $this->load->library('email');

        $from = "support@saletancy.info";

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.sendgrid.net';
        $config['smtp_port'] = '587';
        $config['smtp_user'] = 'apikey';
        $config['smtp_pass'] = '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';
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = 'TRUE';
        $config['newline'] = "\r\n";

        $this->load->library('email', $config);
        $this->email->initialize($config);

        $this->email->from($from);
        $this->email->to($email);
        $this->email->subject('Welcome to Saletancy');

        $email_code = md5($email);

        $message = '<p>Dear ' . $firstname . ',</p>';
        $message .= '<p>Just wanted to reach out and thank you for joining the Saletancy community!</p>';
        $message .= '<br>';
        $message .= '<p style="text-align: center"><img src="https://media.giphy.com/media/ASd0Ukj0y3qMM/giphy.gif" alt="" style="width: 100%; max-width: 500px"></p>';
        $message .= '<br>';
        $message .= '<p>If you&#39;re curious, here&#39;s why you would use Saletancy:</p>';
        $message .= '
            <ol>
                <li>You need accurate emails and direct dials of decision-makers at specific companies in few minutes. </li>
                <li>You need a free way to access profile data of over 2.5 million contacts at over 65,000 businesses across</li>
                <li>You need contextual information to identify your potential customers such as location,revenue size, company type, top leadership, and more.</li>
                <li>You have sales and growth targets. We have the potential contacts and technology to help you reach them.</li>
                <li>Last but not least, you want to get a beautiful product in the hands of more prospects and customers!</li>
            </ol>
        ';
        $message .= '<br>';
        $message .= '<p>While we&#39;re in the pilot stage of this product, we would love your feedback. Please write directly to our CEO at gangesh@saletancy.com if you have any feedback, whether it&#39;s positive or negative.</p>';
        $message .= '<br>';
        $message .= '<p><a href="' . base_url('welcome/verify_email/' . $email_code) . '">Click here</a> to activate your account</p>';
        $message .= '<br>';
        $message .= '<p>Happy selling!</p>';

        $this->email->message($message);
        $this->email->send();
    }

    public function update_userdata($id, $firstname, $lastname, $password1, $email, $u_type, $company, $jobtitle, $telephone)
    {
        $data = array(
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password' => $password1,
            'email' => $email,
            'u_type' => $u_type,
            'company' => $company,
            'job_title' => $jobtitle,
            'telephone' => $telephone
        );
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }

    public function addusercredits($id, $credits)
    {
        $this->db->select('credits');
        $this->db->from('user');
        $this->db->where('id', $_SESSION['user_id']);
        $curr_cred = $this->db->get()->row()->credits;

        $new_cred = array('credits' => $curr_cred + $credits);

        $this->db->where('id', $id);
        $this->db->update('user', $new_cred);
        $_SESSION['user_credits'] = (string)$new_cred['credits'];
        return;
    }

    public function add_company($name, $website, $contact, $address, $comp_city, $comp_state, $emp_size, $revenue, $industry, $sub_industry, $comp_type)
    {
        $data = array(
            'name' => $name,
            'website' => $website,
            'contact' => $contact,
            'address' => $address,
            'comp_city' => $comp_city,
            'comp_state' => $comp_state,
            'emp_size' => $emp_size,
            'revenue' => $revenue,
            'industry' => $industry,
            'sub_industry' => $sub_industry,
            'comp_type' => $comp_type,
        );
        $this->db->insert('company', $data);
        return;
    }

    public function add_employee($comp_id, $firstname, $lastname, $designation, $linkedin, $email, $contact)
    {
        $data = array(
            'comp_id' => $comp_id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'designation' => $designation,
            'linkedin' => $linkedin,
            'email' => $email,
            'contact' => $contact,
        );
        $this->db->insert('employee', $data);
        return;
    }

}

