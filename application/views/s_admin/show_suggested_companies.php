<?php include 'include/header.php'; ?>
<?php include 'include/navbar.php'; ?>
<?php include 'include/side_navbar.php'; ?>
<head>
<script type="text/javascript">
function alerting(id){
    var url="<?php echo base_url();?>";
    var r=confirm("Do you want to delete this?");
        if (r==true)
          window.location = url+"super_admin/delete_company_suggestion/"+id;
        else
          return false;
        } 
</script>
</head>
<div id="page-wrapper">
<div class="container">
  <div class="col-md-12">
  
 <h3 class="text-center">SUGGESTED COMPANIES</h3>
      <table class="table table-responsive table-hover">
   <thead>
     <tr>
      <th>ID</th>
      <th>Company Name</th>
      <th>Employee Size</th>
      <th>Revenue</th>
      <th>Address</th>
      <th>City</th>
      <th>State</th>
      <th>Industry</th>
      <th>Sub_Industry</th>
      <th>Website</th>
      <th>Contact</th>
     </tr>
   </thead>
          <tbody>
          <?php 
foreach ($suggestions->result() as $row) {?>
     <tr>
       <td><?php echo $row->ID; ?></td>
         <td><?php echo $row->name; ?></td>
         <td><?php echo $row->emp_size; ?></td>
         <td><?php echo $row->revenue; ?></td>
         <td><?php echo $row->address; ?></td>
         <td><?php echo $row->comp_city; ?></td>
         <td><?php echo $row->comp_state; ?></td>
         <td><?php echo $row->industry; ?></td>
         <td><?php echo $row->website; ?></td>
         <td><?php echo $row->contact; ?></td>
         <td><a href="<?php echo base_url('super_admin/add_suggested_companies/'.$row->ID)?>">Apply</a></td>
         <td><?php echo "<button type='button' onclick='alerting($row->ID)'>Delete</button>";?></td>
     </tr>
<?php }
  ?>
              </tbody>
            </table>
</div>
</div>
<?php include 'include/footer.php'; ?>

    