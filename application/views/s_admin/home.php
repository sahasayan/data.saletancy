<?php include 'include/header.php'; ?>
<?php include 'include/navbar.php'; ?>
<?php include 'include/side_navbar.php'; ?>
 
  <?php if (isset($error)) : ?>
      <div class="col-md-12">
        <div class="alert alert-danger" role="alert">
          <?= $error ?>
        </div>
      </div>
    <?php endif; ?>

  <?php if (isset($success)) : ?>
      <div class="col-md-12">
        <div class="alert alert-success" role="alert">
          <?= $success ?>
        </div>
      </div>
    <?php endif; ?>

  <?php include 'include/footer.php'; ?>