
     
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
                </div>
                <ul class="nav" id="side-menu">
                    <li style="padding: 70px 0 0;">
                        <a href="<?php echo base_url('super_admin') ?>" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="profile.html" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Profile</a>
                    </li>
                    <li>
                        <a href="basic-table.html" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Basic Table</a>
                    </li>
                    <li>
                        <a href="fontawesome.html" class="waves-effect"><i class="fa fa-font fa-fw" aria-hidden="true"></i>Icons</a>
                    </li>
                     <li>
                        <a href="<?php echo base_url('super_admin/admin_list') ?>" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; Admins List</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('super_admin/show_suggestions') ?>" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; Show Suggestions</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('super_admin/show_suggested_companies') ?>" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; Show Suggested Companies</a>
                    </li>
										<li>
                        <a href="<?php echo base_url('super_admin/users_list') ?>" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; Users List</a>
                    </li>
										<li>
                        <a href="<?php echo base_url('super_admin/import_companies') ?>" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; Import Companies</a>
                    </li>
										<li>
                        <a href="<?php echo base_url('super_admin/import_employees') ?>" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp; Import Employees</a>
                    </li>
                    <li>
                        <a href="super_admin/reset_credits" target="_blank" class="waves-effect">Reset credits to 500</a>
                    </li>
                    <li>
                        <a href="blank.html" class="waves-effect"><i class="fa fa-columns fa-fw" aria-hidden="true"></i>Blank Page</a>
                    </li>
                    <li>
                        <a href="404.html" class="waves-effect"><i class="fa fa-info-circle fa-fw" aria-hidden="true"></i>Error 404</a>
                    </li>

                </ul>
              <!--   <div class="center p-20">
                     <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank" class="btn btn-danger btn-block waves-effect waves-light">Upgrade to Pro</a>
                 </div> -->
            </div>
            
        </div>