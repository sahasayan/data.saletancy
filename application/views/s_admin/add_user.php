<?php include 'include/header.php'; ?>
<?php include 'include/navbar.php'; ?>
<?php include 'include/side_navbar.php'; ?>
<div id="page-wrapper">
<div class="container">
  <div class="col-md-12">
  
    <?php echo form_open('super_admin/add_useradmin',['role'=>'form']); ?>
 
  <div class="input-group">
  <label>First Name:</label> 
  <input class="form-control" type="text" name="firstname" value="<?php echo set_value('firstname'); ?>">
  <span class="help-block error" ><?php echo form_error('firstname')?></span> 
  </div><br/>

  <div class="input-group">
  <label>Last Name:</label> 
  <input class="form-control" type="text" name="lastname" value="<?php echo set_value('lastname'); ?>">
  <span class="help-block error" ><?php echo form_error('lastname')?></span>
  </div><br/>
  
  <div class="input-group">
  <label>Password:</label>
  <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>">
  <span class="help-block error" ><?php echo form_error('password')?></span>
  </div><br/>

  <div class="input-group">
  <label>Confirm Password:</label>
  <input class="form-control" type="password" name="cpassword" value="<?php echo set_value('cpassword'); ?>">
  <span class="help-block error" ><?php echo form_error('cpassword')?></span>
  </div><br/>
  
  <div class="input-group">
  <label>Email:</label>
  <input class="form-control" type="text" name="email" value="<?php echo set_value('email'); ?>">
  <span class="help-block error" ><?php echo form_error('email')?></span>
  </div><br/>
  
  <div class="input-group">
  <label>User Type:</label>
  <input class="form-control" type="text" name="u_type" value="<?php echo set_value('u_type'); ?>">
  <span class="help-block error" ><?php echo form_error('u_type')?></span>
  </div><br/>

		
	<div class="input-group">
  <label>Company:</label>
  <input class="form-control" type="text" name="company" value="<?php echo set_value('company'); ?>">
  <span class="help-block error" ><?php echo form_error('company')?></span>
  </div><br/>
			
	<div class="input-group">
  <label>Job Title:</label>
  <input class="form-control" type="text" name="jobtitle" value="<?php echo set_value('jobtitle'); ?>">
  <span class="help-block error" ><?php echo form_error('jobtitle')?></span>
  </div><br/>
			
	<div class="input-group">
  <label>Telephone:</label>
  <input class="form-control" type="text" name="telephone" value="<?php echo set_value('telephone'); ?>">
  <span class="help-block error" ><?php echo form_error('telephone')?></span>
  </div><br/>
  
  
  <div class="input-group">
  <button type="submit" name="register" class="btn">Register</button>
  </div><br/>
</div>

</div>
</div>
<?php echo form_close(); ?>
<?php include 'include/footer.php'; ?>
