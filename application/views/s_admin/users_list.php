<?php include 'include/header.php'; ?>
<?php include 'include/navbar.php'; ?>
<?php include 'include/side_navbar.php'; ?>
<head>
<script type="text/javascript">
function alerting(id){
    var url="<?php echo base_url();?>";
    var r=confirm("Do you want to block this user?");
        if (r==true)
          window.location = url+"super_admin/block_user/"+id;
        else
          return false;
        } 
	
function alerting2(id){
    var url="<?php echo base_url();?>";
    var r=confirm("Do you want to unblock this user?");
        if (r==true)
          window.location = url+"super_admin/unblock_user/"+id;
        else
          return false;
        } 
</script>
</head>
<div id="page-wrapper">
<div class="container">
  <div class="col-md-12">
  
 <h3 class="text-center">USERS</h3>
      <table class="table table-responsive table-hover">
   <thead>
     <tr>
      <th>ID</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email</th>
      <th>Date</th>
			<th>Company</th>
      <th>Title</th>
			<th>Block Status</th>
     </tr>
   </thead>
          <tbody>
          <?php 
foreach ($admin->result() as $row) {?>
     <tr>
       <td><?php echo $row->id; ?></td>
       <td><?php echo $row->firstname; ?></td>
         <td><?php echo $row->lastname; ?></td>
         <td><?php echo $row->email; ?></td>
         <td><?php echo $row->date; ?></td>
			 		<td><?php echo $row->company; ?></td>
         <td><?php echo $row->job_title; ?></td>
			 	<td><?php echo $row->is_blocked; ?></td>
			 	<?php if($row->is_blocked == "false") { ?>
         <td><?php echo "<button type='button' onclick='alerting($row->id)'>Block</button>";?></td>
			 	<?php }
  			?>
			 
			 	<?php if($row->is_blocked == "true") { ?>
         <td><?php echo "<button type='button' onclick='alerting2($row->id)'>Unblock</button>";?></td>
			 	<?php }
  			?>
     </tr>
<?php }
  ?>
              </tbody>
            </table>
</div>
</div>
<?php include 'include/footer.php'; ?>

    