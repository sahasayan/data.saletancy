<?php include 'include/header.php'; ?>
<?php include 'include/navbar.php'; ?>
<?php include 'include/side_navbar.php'; ?>
<head>
<script type="text/javascript">
function alerting(id){
    var url="<?php echo base_url();?>";
    var r=confirm("Do you want to delete this?");
        if (r==true)
          window.location = url+"super_admin/delete_admin/"+id;
        else
          return false;
        } 
</script>
</head>
<div id="page-wrapper">
<div class="container">
  <div class="col-md-12">
  
 <h3 class="text-center">ADMINS</h3>
      <table class="table table-responsive table-hover">
   <thead>
     <tr>
      <th>ID</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Email</th>
      <th>Date</th>
     </tr>
   </thead>
          <tbody>
          <?php 
foreach ($admin->result() as $row) {?>
     <tr>
       <td><?php echo $row->id; ?></td>
       <td><?php echo $row->firstname; ?></td>
         <td><?php echo $row->lastname; ?></td>
         <td><?php echo $row->email; ?></td>
         <td><?php echo $row->date; ?></td>
         <td><a href="<?php echo base_url('super_admin/edit_admin/'.$row->id)?>">Edit</a></td>
         <td><?php echo "<button type='button' onclick='alerting($row->id)'>Delete</button>";?></td>
     </tr>
<?php }
  ?>
              </tbody>
            </table>
</div>
</div>
    <a href="<?php echo base_url('super_admin/add_user') ?>"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add Admin</a>
<?php include 'include/footer.php'; ?>

    