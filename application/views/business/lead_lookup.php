<?php $pg='lead_lookup';?>
<?php include 'include/header.php'; ?>
<title>Home</title>
<?php include 'include/afterLogin_navbar.php'; ?>
<style>
    .modal-dialog,
      .modal-content {
    /* 80% of window height */
    height: 80%;
    }

     .modal-body {
    /* 100% = dialog height, 120px = header + footer */
    height: 400px;
    overflow-y: scroll;
     }
    </style>
<div style="height:110px">
  
</div>
<!--==========================
     Search Filter
    ========================-->
    <div class="container">

    <form class="form" action="<?php echo base_url();?>business/lead_lookup/search" method="post">
        <div class="form-group">
            <div class="col-sm-12" style="text-align:right">
                <input type="text" class="form-control" id="searchComp" placeholder="Which Company Are You Trying To Contact?" name="searchBox" style="width:91%;height:60px;float:left;">
                <button type="submit" class="btn btn-lg btn-success fa fa-search" style="float:right;height:60px;"> Search</button>
								<ul style="width: 80%;">  
       							<div style="background-color: #f9f9f9; border: none; box-shadow: none;" class="well" id="result"></div>  
  							</ul> 
            </div>
        </div>
    </form>

</div>
<!--End of Search Filter-->
<br><br><br>

<?php if(isset($query_result) && count($query_result->result()) == 1) { 
	$row = $query_result->result()[0];
	?>
<div class="container" style="background-color:white;border-radius:40px">
    <div class="row" style="padding:50px 10px 50px 50px;">
        <div class="col-sm-12 col-lg-5">
            <div style="text-align:center;width:200px">
<!--                <img class="" src="http://saletancy.com/assest/img/Logo.png" alt="" width="150">-->
										<h2 style="color: red;"><?php echo $row->name[0] ?></h2>
            </div>
            
            <div class="card text-center" style="width: 14rem;border:0px">
                <div class="card-body">
                    <h5 class="card-title" style="display:inline-block"><?php echo $row->name; ?></h5><span class="fa fa-edit" data-toggle="modal" data-target="#company_name" style="margin-left:5px"></span>
                    <br><br>
									<p class="card-text" style="display:inline-block"><a href="//<?php echo $row->website ?>" target="_blank"><?php echo $row->website ?></a></p> <span class="fa fa-edit" data-toggle="modal" data-target="#website" style="margin-left:5px"></span><br><br>

                  </div>
            </div>

        </div>
        <div class="col-sm-12 col-lg-7">

            <div class="row mx-auto">
                <div class="col-sm-12 col-lg-4" style="padding-left:40px">
                    <div style="text-align:center;width:160px">
                        <img class="" src="http://saletancy.com/assest/img/Education-and-Training.png" alt="Card image cap" width="70">
                    </div>
                    <div class="card  text-center" style="width: 10rem;border:0px;">
                        <div class="card-body">
                            <h5 class="card-title"  style="display:inline-block"><?php echo str_replace('employees', '', $row->emp_size); ?></h5><br><span class="fa fa-edit" data-toggle="modal" data-target="#client_size" style="margin-left:5px"></span>
                            <p class="card-text">Employee Size</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 " style="padding-left:40px">
                    <div style="text-align:center;width:160px">
                        <img class="" src="http://saletancy.com/assest/img/Education-and-Training.png" alt="Card image cap" width="70">
                    </div>
                    <div class="card text-center" style="width: 10rem;border:0px">
                        <div class="card-body">
                            <h5 class="card-title" style="display:inline-block"><?php echo $row->revenue; ?></h5><br><span class="fa fa-edit" data-toggle="modal" data-target="#revenue"  style="margin-left:5px"></span>
                            <p class="card-text">Revenue Size</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4" style="padding-left:40px">
                    <div style="text-align:center;width:160px">
                        <img class="" src="http://saletancy.com/assest/img/Education-and-Training.png" alt="Card image cap" width="70">
                    </div>
                    <div class="card text-center" style="width: 10rem;border:0px">
                        <div class="card-body">
                            <h5 class="card-title" style="display:inline-block"><?php echo $row->comp_city; ?></h5><br><span class="fa fa-edit" data-toggle="modal" data-target="#location" style="margin-left:5px"></span>
                            <p class="card-text">LOCATION</p>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row">
                
            </div>
            <br>

            <div class="row mx-auto">
                <br>

                <div class="col-sm-6" style="padding-left:60px">
                <header class="wow fadeInUp" style="text-align:center">
                        <h4>Company Type</h4>
                        <p><br><br></p>
                    </header>
                    <header class="wow fadeInUp" style="text-align:center">
                        <h5 style="display:inline-block"><?php echo $row->comp_type ?></h5> <span class="fa fa-edit" data-toggle="modal" data-target="#company_type" style="margin-left:5px"></span>
                    </header>

                </div>
               
                <div class="col-sm-6" style="padding-left:60px">
                <header class="wow fadeInUp" style="text-align:center">
                        <h4>Industry</h4>
                        <p><br><br></p>
                    </header>
                    <header class="wow fadeInUp" style="text-align:center">
                    <h5 style="display:inline-block"><?php echo $row->sub_industry ?></h5> <span class="fa fa-edit" data-toggle="modal" data-target="#industry" style="margin-left:5px"></span>
                </header>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
?>

<br>

<!--==========================
     Search Filter by Designation
    ========================-->

<?php if(isset($employee_data)) { ?>
<div class="container">

    <form class="form" action="<?php echo current_url();?>" method = "get">
        <div class="form-grou">
            <div class="col-sm-12" style="text-align:right">
                <input type="text" class="form-control" placeholder="Filter By Designation" name="filter" style="width:91%;height:60px;float:left;">
                <button type="submit" class="btn btn-success btn-lg fa fa-search" style="float:right;height:60px;"> Search</button>
            </div>
        </div>
    </form>

</div>
<br><br><br><br><br>

<header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Top Leadership</h3>
</header>
<br>
<?php
foreach ($employee_data->result() as $row) {?>
<div class="container" style="background-color:white;border-radius:10px;border-left:15px solid green;height:10%">
    <div class="row " style="padding:18px 0px 50px 0px;">
        <div class="col-sm-12 col-lg-3">
            <header class="wow fadeInUp" style="text-align:center">
                <h4><?php echo $row->firstname . ' '. $row->lastname; ?></h4>
            </header>


        </div>
        <div class="col-sm-12 col-lg-3">
            <header class="wow fadeInUp" style="text-align:center">
                <h5><?php echo $row->designation; ?></h5>
            </header>
        </div>

        <div class="col-sm-12 col-lg-1">
            <header class="wow fadeInUp" style="text-align:center">
                <h5><a href="//<?php echo $row->linkedin; ?>" target="_blank"><i class="fa fa-linkedin" style="font-size:30px;color:#149dcc"></i></a></h5>
            </header>

        </div>
        
        <div class="col-sm-12 col-lg-2">
            <header class="wow fadeInUp" style="text-align:center;word-wrap:break-word;">
                <h5><?php echo $row->email; ?></h5>
            </header>
        </div>

        <div class="col-sm-12 col-lg-3">
            <header class="wow fadeInUp" style="text-align:center">
                <h5><?php echo $row->contact; ?></h5>
               
            </header>
        </div>
    </div>
</div>
<?php }													 
?>
		
		<?php }
    ?>

<!--End of Search Filter by designation-->
<br>
<br>
<!--==============================
         MODAL
    ============================-->
    <div class="modal fade" id="company_name" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top:100px">
    
      <!-- Modal Company Name-->
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        <br><br>
        <div class="modal-body">
            <header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Suggest Another Company ?</h3>
            </header><br><br>
            <form class="form" action="<?php echo base_url();?>business/suggest/" method="post">
        <div class="form-group">
            <div class="col-sm-12" style="text-align:right">
								<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            		<input type="hidden" name="field_edit" value="name"/>
                <input type="text" class="form-control" placeholder="Suggest Company Name" name="new_value" style="width:80%;height:50px;float:left;">
                <input type="submit" class="btn btn-lg btn-success" style="float:left;height:50px;" onclick="alert('Thank You. We appreciate your help to make salespeople life easier')" value="Suggest"> 
            </div>
        </div>
    </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="website" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top:100px">
    
      <!-- Modal Company Website-->
      <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        <br><br>
        <div class="modal-body">
            <header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Suggest Another Website ?</h3>
            </header><br><br>
            <form class="form" action="<?php echo base_url();?>business/suggest/" method="post">
        <div class="form-group">
            <div class="col-sm-12" style="text-align:right">
								<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            		<input type="hidden" name="field_edit" value="website"/>
                <input type="text" class="form-control" placeholder="e.g., www.xyz@dpac.domain" name="new_value" style="width:80%;height:50px;float:left;">
                <button type="submit" class="btn btn-lg btn-success" style="float:left;height:50px;" onclick="alert('Thank You. We appreciate your help to make salespeople life easier')" >Suggest</button>
            </div>
        </div>
    </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="location" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top:100px">
    
      <!-- Modal Company Location-->
      <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        <br><br>
        <div class="modal-body">
            <header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Suggest Another Location ?</h3>
            </header><br><br>
            <form class="form" action="<?php echo base_url();?>business/suggest/" method="post">
        <div class="form-group">
            <div class="col-sm-12" style="text-align:right">
								<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            		<input type="hidden" name="field_edit" value="comp_city"/>
                <input type="text" class="form-control" placeholder="Enter Location" name="new_value" style="width:80%;height:50px;float:left;">
                <button type="submit" class="btn btn-lg btn-success" style="float:left;height:50px;" onclick="alert('Thank You. We appreciate your help to make salespeople life easier')"> Suggest</button>
            </div>
        </div>
    </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="company_type" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top:100px">
    
      <!-- Modal Company Type-->
      <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        <br><br>
        <div class="modal-body">
            <header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Select Company Type</h3>
            </header><br><br>
            <form class="form" action="<?php echo base_url();?>business/suggest/" method="post">
							<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            		<input type="hidden" name="field_edit" value="comp_type"/>
            <div class="form-group">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6" style="text-align:left;">
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="India Top 1000"> India Top 1000</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="MNC"> MNC</label>
                            </div>
                        </div>
                        <div class="col-sm-6" style="text-align:left">
        
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="SME"> SME</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="Start Up"> Start Up</label>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
							<div style="text-align:center">
                        <button type="submit" class="btn btn-success btn-lg"  onclick="alert('Thank You. We appreciate your help to make salespeople life easier')"> Suggest</button>
    </div>
            
        </form>
        </div>
   
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="client_size" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top:100px">
    
      <!-- Modal Client Size-->
      <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        
        <div class="modal-body">
            <header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Select Employee Size</h3>
            </header><br><br>
            <form class="form" action="<?php echo base_url();?>business/suggest/" method="post">
							<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            <input type="hidden" name="field_edit" value="emp_size"/>
            <div class="form-group">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6" style="text-align:left;padding-left:140px">
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="1-10 employees"> 1 - 10</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="11-50 employees"> 11 - 50</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="51-200 employees"> 51 -200</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="201-500 employees"> 201 - 500</label>
                            </div>
                           
                        </div>
                        <div class="col-sm-6" style="text-align:left;padding-left:140px">
        										<div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="501-1000 employees"> 501 - 1000</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="1001-5000 employees"> 1,001 - 5,000</label>
                            </div>
                             <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="5001-10,000 employees"> 5,001 - 10,000</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="10,001+ employees"> 10,001+</label>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
							
							<div style="text-align:center">
                        <input  type="submit" class="btn btn-success btn-lg" onclick="alert('Thank You. We appreciate your help to make salespeople life easier')" value="Suggest"> 
    </div>
            
        </form>
        </div>
      
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="revenue" role="dialog">
    <div class="modal-dialog modal-lg" style="margin-top:100px">
    
      <!-- Modal Company Revenue-->
      <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        
        <div class="modal-body">
            <header class="section-header wow fadeInUp" style="text-align:center">
                <h3>Select Revenue Size</h3>
            </header><br><br>
            <form class="form" action="<?php echo base_url();?>business/suggest/" method="post">
							<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            <input type="hidden" name="field_edit" value="revenue"/>
            <div class="form-group">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6" style="text-align:left;padding-left:110px">
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="0-1 Crs"> 0 - 1  Crs</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="1-10 Crs"> 1 - 10  Crs</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="10-100 Crs"> 10 - 100  Crs</label>
                            </div>
                           <div class="radio">
                                <label><input type="radio" name="new_value" value="100-250 Crs"> 100 - 250  Crs</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="250-500 Crs"> 250 - 500  Crs</label>
                            </div>
                           
                        </div>
                        <div class="col-sm-6" style="text-align:left;padding-left:120px">
        
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="500-1000 Crs"> 500 - 1000 Crs</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="1000-2500 Crs"> 1000 - 2500  Crs</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="new_value" value="2500-5000 Crs"> 2500 - 5000  Crs</label>
                            </div>
                            <div class="radio" >
                                <label><input width="30" type="radio" name="new_value" value="5000+ Crs"> 5000+  Crs</label>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
							
							<div style="text-align:center">
                        <input type="submit" class="btn btn-success btn-lg" onclick="alert('Thank You. We appreciate your help to make salespeople life easier')" value="Suggest">
    </div>
            
        </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>
    


  <div class="modal fade" tabindex="-1" id="industry" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
        <div class="container">
            <!-- Modal Industry-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="container" style="">
                    <br>
                    <header class="section-header wow fadeInUp">
                        <h3 style="text-align:center">Suggest Another Industry ?</h3>
                    </header>
                    <!--From Here-->
                    <div class="modal-body">
                        <br>
                        <form action="<?php echo base_url();?>business/suggest/" method="post">
    <div class="col-sm-12">

	<input type="hidden" name="id" value="<?php echo $_SESSION['company_searched'] ?>"/>
            											<input type="hidden" name="field_edit" value="industry"/>


        <div type="" class="" data-toggle="collapse" data-target="#media">
            <h5> <span class="fa fa-arrow-right"></span> <label>Media & Entertainment</label></h5>
        </div>
        <div class="checkbox">
            <div id="media" class="collapse">

                <div class="checkbox">
                    <input onclick="set_industry('Advertising')" type="radio" value="Advertsing" name="new_value"><h6 style="display:inline-block"><h6 style="display:inline-block">Advertising</h6>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Associations')" type="radio" value="Associations" name="new_value"><h6 style="display:inline-block">Associations</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Broadcast')" type="radio" value="Broadcast" name="new_value"><h6 style="display:inline-block">Broadcast</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Newspapers & Magazines')" type="radio" value="Newspapers & Magazines" name="new_value"><h6 style="display:inline-block">Newspapers & Magazines</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Entertainment')" type="radio" value="Entertainment" name="new_value"><h6 style="display:inline-block">Entertainment</h2></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Printing & Publishing')" type="radio" value="Printing & Publishing" name="new_value"><h6 style="display:inline-block">Printing & Publishing</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#net">
            <h5> <span class="fa fa-arrow-right"></span> <label>Networking & Telecommunications</label></h5>
        </div>
        <div class="checkbox">
            <div id="net" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Telecommunications')" type="radio" value="Telecommunications" name="new_value"><h6 style="display:inline-block">Telecommunications</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Wireless Networking')" type="radio" value="Wireless networking" name="new_value"><h6 style="display:inline-block">Wireless Networking</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Network Managemnet')" type="radio" value="Network Management" name="new_value"><h6 style="display:inline-block">Network Managemnet</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#energy">
            <h5> <span class="fa fa-arrow-right"></span> <label>Energy & Utilities</label></h5>
        </div>
        <div class="checkbox">
            <div id="energy" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Electricity')" type="radio" value="Electricity" name="new_value"><h6 style="display:inline-block">Electricity</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Gas')" type="radio" value="Gas" name="new_value"><h6 style="display:inline-block">Gas</h4></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Petroleum')" type="radio" value="Petroleum" name="new_value"><h6 style="display:inline-block">Petroleum</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Waste Managemen')" type="radio" value="Waste Management" name="new_value"><h6 style="display:inline-block">Waste Management</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Water Resources')" type="radio" value="Water Resources" name="new_value"><h6 style="display:inline-block">Water Resources</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Environmenta')" type="radio" value="Environment" name="new_value"><h6 style="display:inline-block">Environment</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#it">
            <h5> <span class="fa fa-arrow-right"></span> <label>IT / ITES</label></h5>
        </div>
        <div class="checkbox">
            <div id="it" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Computer Leasing And Rental')" type="radio" value="Computer Leasing And Rental" name="new_value"><h6 style="display:inline-block">Computer Leasing And Rental</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('AdverComputer Maintenance & Servicestising')" type="radio" value="Computer Maintenance & Services" name="new_value"><h6 style="display:inline-block">Computer Maintenance & Services</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('IT Software')" type="radio" value="IT Software" name="new_value"><h6 style="display:inline-block">IT Software</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('IT Outsourcing')" type="radio" value="IT Outsourcing" name="new_value"><h6 style="display:inline-block">IT Outsourcing</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('System Integration')" type="radio" value="System Integration" name="new_value"><h6 style="display:inline-block">System Integration</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('It Consultancy')" type="radio" value="It Consultancy" name="new_value"><h6 style="display:inline-block">It Consultancy</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Hardware')" type="radio" value="Hardware" name="new_value"><h6 style="display:inline-block">Hardware</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#consumer">
            <h5> <span class="fa fa-arrow-right"></span> <label>Consumer Packaged Goods</label></h5>
        </div>
        <div class="checkbox">
            <div id="consumer" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Consumer Packaged Goods')" type="radio" value="Consumer Packaged Goods" name="new_value"><h6 style="display:inline-block">Consumer Packaged Goods</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Beverages & Tobacco')" type="radio" value="Beverages & Tobacco" name="new_value"><h6 style="display:inline-block">Beverages & Tobacco</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Food Manufacturing')" type="radio" value="Food Manufacturing" name="new_value"><h6 style="display:inline-block">Food Manufacturing</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('General Processing')" type="radio" value="General Processing" name="new_value"><h6 style="display:inline-block">General Processing</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Other Agro Products')" type="radio" value="Other Agro Products" name="new_value"><h6 style="display:inline-block">Other Agro Products</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#business">
            <h5> <span class="fa fa-arrow-right"></span> <label>Business Services</label></h5>
        </div>
        <div class="checkbox">
            <div id="business" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Household/health & Beauty')" type="radio" value="Household/health & Beauty" name="new_value"><h6 style="display:inline-block">Household/health & Beauty</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Accounting And Auditing')" type="radio" value="Accounting And Auditing" name="new_value"><h6 style="display:inline-block">Accounting And Auditing</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Legal Services')" type="radio" value="Legal Services" name="new_value"><h6 style="display:inline-block">Legal Services</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Bpo')" type="radio" value="Bpo" name="new_value"><h6 style="display:inline-block">Bpo</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Kpo')" type="radio" value="Kpo" name="new_value"><h6 style="display:inline-block">Kpo</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Hr Consultancy')" type="radio" value="Hr Consultancy" name="new_value"><h6 style="display:inline-block">Hr Consultancy</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Management Consulting')" type="radio" value="Management Consulting" name="new_value"><h6 style="display:inline-block">Management Consulting</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Professional Services')" type="radio" value="Professional Services" name="new_value"><h6 style="display:inline-block">Professional Services</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#bfsi">
            <h5> <span class="fa fa-arrow-right"></span> <label>BFSI</label></h5>
        </div>
        <div class="checkbox">
            <div id="bfsi" class="collapse">

                <div class="checkbox ">
                    <label><input onclick="set_industry('Retail Banking')" type="radio" value="Retail Banking" name="new_value"><h6 style="display:inline-block">Retail Banking</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Stock Broking')" type="radio" value="Stock Broking" name="new_value"><h6 style="display:inline-block">Stock Broking</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Retail Finance')" type="radio" value="Retail Finance" name="new_value"><h6 style="display:inline-block">Retail Finance</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Insurance')" type="radio" value="Insurance" name="new_value"><h6 style="display:inline-block">Insurance</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#retail">
            <h5> <span class="fa fa-arrow-right"></span> <label>Retail & Trading</label></h5>
        </div>
        <div class="checkbox">
            <div id="retail" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Retail')" type="radio" value="Retail" name="new_value"><h6 style="display:inline-block">Retail</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Retail- Apparel Manufacturing')" type="radio" value="Retail- Apparel Manufacturing" name="new_value"><h6 style="display:inline-block">Retail- Apparel Manufacturing</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Retail- Department Stores')" type="radio" value="Retail- Department Stores" name="new_value"><h6 style="display:inline-block">Retail- Department Stores</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Retail- Direct Marketing')" type="radio" value="Retail- Direct Marketing" name="new_value"><h6 style="display:inline-block">Retail- Direct Marketing</h6></label>
                </div>
                <div class="checkbox " 6
                                                <label><input onclick="set_industry('Retail- Food')" type="radio" value="Retail- Food" name="new_value"><h6 style="display:inline-block">Retail- Food</h6 style="display:inline-block"</label>
                                            </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Retail- Hard Goods')" type="radio" value="Retail- Hard Goods" name="new_value"><h6 style="display:inline-block">Retail- Hard Goods</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Retail- Pharmacy/drug')" type="radio" value="Retail- Pharmacy/drug" name="new_value"><h6 style="display:inline-block">Retail- Pharmacy/drug</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Retail- Specialty')" type="radio" value="Retail- Specialty" name="new_value"><h6 style="display:inline-block">Retail- Specialty</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Retail- Others')" type="radio" value="Retail- Others" name="new_value"><h6 style="display:inline-block">Retail- Others</h6></label>
                </div>

            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#metal">
            <h5> <span class="fa fa-arrow-right"></span> <label>Metal & Mining</label></h5>
        </div>
        <div class="checkbox">
            <div id="metal" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Metals')" type="radio" value="Metals(Name)" name="new_value"><h6 style="display:inline-block">Metals(Name)</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Mining')" type="radio" value="Mining(Name)" name="new_value"><h6 style="display:inline-block">Mining(Name)</h6></label>
                </div>
            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#logistic">
            <h5> <span class="fa fa-arrow-right"></span> <label>Logistics & Transportation</label></h5>
        </div>
        <div class="checkbox">
            <div id="logistic" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Car Rental')" type="radio" value="Car Rental" name="new_value"><h6 style="display:inline-block">Car Rental</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('AdvertiFreight Airlinessing')" type="radio" value="Freight Airlines" name="new_value"><h6 style="display:inline-block">Freight Airlines</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Freight Forwarders & Arrangers')" type="radio" value="Freight Forwarders & Arrangers" name="new_value"><h6 style="display:inline-block">Freight Forwarders & Arrangers</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Freight Services')" type="radio" value="Freight Services" name="new_value"><h6 style="display:inline-block">Freight Services</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Lodging')" type="radio" value="Lodging" name="new_value"><h6 style="display:inline-block">Lodging</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Passenger Airlines')" type="radio" value="Passenger Airlines" name="new_value"><h6 style="display:inline-block">Passenger Airlines</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Rail')" type="radio" value="Rail" name="new_value"><h6 style="display:inline-block">Rail</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Shipping')" type="radio" value="Shipping" name="new_value"><h6 style="display:inline-block">Shipping</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Courier & Postal')" type="radio" value="Courier & Postal" name="new_value"><h6 style="display:inline-block">Courier & Postal</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Travel & Transportation')" type="radio" value="Travel & Transportation" name="new_value"><h6 style="display:inline-block">Travel & Transportation</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Travel Agencies')" type="radio" value="Travel Agencies" name="new_value"><h6 style="display:inline-block">Travel Agencies</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Urban Transportation')" type="radio" value="Urban Transportation" name="new_value"><h6 style="display:inline-block">Urban Transportation</h6></label>
                </div>

            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#aero">
            <h5> <span class="fa fa-arrow-right"></span> <label>Aerospace & Defense</label></h5>
        </div>
        <div class="checkbox">
            <div id="aero" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Aerospace')" type="radio" value="Aerospace" name="new_value"><h6 style="display:inline-block">Aerospace</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Defense')" type="radio" value="Defense" name="new_value"><h6 style="display:inline-block">Defense</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('GIS')" type="radio" value="GIS" name="new_value"><h6 style="display:inline-block">GIS</h6></label>
                </div>

            </div>
        </div>


    </div>
    <div class="col-sm-12">



        <div type="" class="" data-toggle="collapse" data-target="#automobile">
            <h5> <span class="fa fa-arrow-right"></span> <label>Automobile & Auto Ancillaries</label></h5>
        </div>
        <div class="checkbox">
            <div id="automobile" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Automotive')" type="radio" value="Automotive" name="new_value"><h6 style="display:inline-block">Automotive</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Automobile Manufacturer')" type="radio" value="Automobile Manufacturer" name="new_value"><h6 style="display:inline-block">Automobile Manufacturer</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Auto Parts Manufacture')" type="radio" value="Auto Parts Manufacture" name="new_value"><h6 style="display:inline-block">Auto Parts Manufacture</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Automotive Suppliers')" type="radio" value="Automotive Suppliers" name="new_value"><h6 style="display:inline-block">Automotive Suppliers</h6></label>
                </div>
                <div class="checkbox ">
                    <label><input onclick="set_industry('Heavy Equipment')" type="radio" value="Heavy Equipment" name="new_value"><h6 style="display:inline-block">Heavy Equipment</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#chemical">
            <h5> <span class="fa fa-arrow-right"></span> <label>Chemicals</label></h5>
        </div>
        <div class="checkbox">
            <div id="chemical" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Chemicals')" type="radio" value="Chemicals" name="new_value"><h6 style="display:inline-block">Chemicals</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#electrical">
            <h5> <span class="fa fa-arrow-right"></span> <label>Electrical & Electronics</label></h5>
        </div>
        <div class="checkbox">
            <div id="electrical" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Electrical')" type="radio" value="Electrical" name="new_value"><h6 style="display:inline-block">Electrical</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Electronics')" type="radio" value="Electronics" name="new_value"><h6 style="display:inline-block">Electronics</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#infrastructure">
            <h5> <span class="fa fa-arrow-right"></span> <label>Infrastructure</label></h5>
        </div>
        <div class="checkbox">
            <div id="infrastructure" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Construction')" type="radio" value="Construction" name="new_value"><h6 style="display:inline-block">Construction</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Cement')" type="radio" value="Cement" name="new_value"><h4>Cement</h4></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Building Materials')" type="radio" value="Building Materials" name="new_value"><h6 style="display:inline-block">Building Materials</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Real Estate')" type="radio" value="Real Estate" name="new_value"><h6 style="display:inline-block">Real Estate</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#engineering">
            <h5> <span class="fa fa-arrow-right"></span> <label>Engineering</label></h5>
        </div>
        <div class="checkbox">
            <div id="engineering" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Fabrication & Assembly')" type="radio" value="Fabrication & Assembly" name="new_value"><h6 style="display:inline-block">Fabrication & Assembly</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Designing')" type="radio" value="Designing" name="new_value"><h6 style="display:inline-block">Designing</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#manufacturing">
            <h5> <span class="fa fa-arrow-right"></span> <label>Manufacturing & Production</label></h5>
        </div>
        <div class="checkbox">
            <div id="manufacturing" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Product Name')" type="radio" value="Product Name" name="new_value"><h6 style="display:inline-block">Product Name</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Industrial Goods')" type="radio" value="Industrial Goods" name="new_value"><h6 style="display:inline-block">Industrial Goods</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Machinery')" type="radio" value="Machinery" name="new_value"><h6 style="display:inline-block">Machinery</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Furniture')" type="radio" value="Furniture" name="new_value"><h6 style="display:inline-block">Furniture</h6></label>
                </div>

            </div>
        </div>


        <div type="" class="" data-toggle="collapse" data-target="#textile">
            <h5> <span class="fa fa-arrow-right"></span> <label>Textiles & Garments</label></h5>
        </div>
        <div class="checkbox">
            <div id="textile" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Textiles')" type="radio" value="Textiles" name="new_value"><h6 style="display:inline-block">Textiles</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Garments')" type="radio" value="Garments" name="new_value"><h6 style="display:inline-block">Garments</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Leather & Footwear')" type="radio" value="Leather & Footwear" name="new_value"><h6 style="display:inline-block">Leather & Footwear</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Fabrics')" type="radio" value="Fabrics" name="new_value"><h6 style="display:inline-block">Fabrics</h6></label>
                </div>

            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#education">
            <h5> <span class="fa fa-arrow-right"></span> <label>Education</label></h5>
        </div>
        <div class="checkbox">
            <div id="education" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Basic/secondary Education')" type="radio" value="Basic/secondary Education" name="new_value"><h6 style="display:inline-block">Basic/secondary Education</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Tertiary/colleges')" type="radio" value="Tertiary/colleges" name="new_value"><h6 style="display:inline-block">Tertiary/colleges</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('University')" type="radio" value="University" name="new_value"><h6 style="display:inline-block">University</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Higher Education')" type="radio" value="Higher Education" name="new_value"><h6 style="display:inline-block">Higher Education</h6></label>
                </div>

            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#government">
            <h5> <span class="fa fa-arrow-right"></span> <label>Government & NGO</label></h5>
        </div>
        <div class="checkbox">
            <div id="government" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Central/federal')" type="radio" value="Central/federal" name="new_value"><h6 style="display:inline-block">Central/federal</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Provincial/state')" type="radio" value="Provincial/state" name="new_value"><h6 style="display:inline-block">Provincial/state</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Local')" type="radio" value="Local" name="new_value"><h6 style="display:inline-block">Local</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('NGO')" type="radio" value="NGO" name="new_value"><h6 style="display:inline-block">NGO</h6></label>
                </div>

            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#lifescience">
            <h5> <span class="fa fa-arrow-right"></span> <label>Life Science</label></h5>
        </div>
        <div class="checkbox">
            <div id="lifescience" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Pharmaceuticals')" type="radio" value="Pharmaceuticals" name="new_value"><h6 style="display:inline-block">Pharmaceuticals</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Hospitals')" type="radio" value="Hospitals" name="new_value"><h6 style="display:inline-block">Hospitals</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Clinics & Laboratories')" type="radio" value="Clinics & Laboratories" name="new_value"><h6 style="display:inline-block">Clinics & Laboratories</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Medical Research')" type="radio" value="Medical Research" name="new_value"><h6 style="display:inline-block">Medical Research</h6></label>
                </div>

            </div>
        </div>

        <div type="" class="" data-toggle="collapse" data-target="#hospitality">
            <h5> <span class="fa fa-arrow-right"></span> <label>Hospitality</label></h5>
        </div>
        <div class="checkbox">
            <div id="hospitality" class="collapse">

                <div class="checkbox">
                    <label><input onclick="set_industry('Hotels & Resorts')" type="radio" value="Hotels & Resorts" name="new_value"><h6 style="display:inline-block">Hotels & Resorts</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Restaurants & Clubs')" type="radio" value="Restaurants & Clubs" name="new_value"><h6 style="display:inline-block">Restaurants & Clubs</h6></label>
                </div>
                <div class="checkbox">
                    <label><input onclick="set_industry('Travel & Tourism')" type="radio" value="Travel & Tourism" name="new_value"><h6 style="display:inline-block">Travel & Tourism</h6></label>
                </div>

            </div>
        </div>

			</div>
			
				<div style="text-align:center">
                    <button type="submit" class="btn btn-success btn-lg" onclick="alert('Thank You. We appreciate your help to make salespeople life easier')"> Suggest</button>

        </div>

    </div>
</form>

</div>
                </div>
                
                <div class="modal-footer"></div>
            </div>

        </div>
    </div>
</div>
    <!--End of Modal-->
   
<?php include 'include/footer.php'; ?>
					
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css" type="text/css" media="all" />  
<link rel="stylesheet" href="//static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/   css" media="all" />  
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready( function() {  
            $("#searchComp").autocomplete({  
                minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>business/search_suggestions",  
                        dataType: 'json',  
                        type: 'POST',  
                        data: req,  
                        success:      
                        function(data){  
                            if(data.response =="true"){  
                                add(data.message);  
                            }  
                        },  
                    });  
                },  
                     
            });  
        }); 

</script> 