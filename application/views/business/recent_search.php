<?php $pg='recent_search';?>
<?php include 'include/header.php'; ?>
<title>Home</title>
<?php include 'include/afterLogin_navbar.php'; ?>
<div style="height:110px">
</div>


<!--==========================
     Search Filter
    ========================-->
    <div class="container">

    <form class="form" action="<?php echo base_url();?>business/lead_lookup/search" method="post">
        <div class="form-group">
            <div class="col-sm-12" style="text-align:right">
                <input type="text" class="form-control" id="search" placeholder="Which Company Are You Trying To Contact?" name="searchBox" style="width:91%;height:60px;float:left;">
                <button type="submit" class="btn btn-lg btn-success fa fa-search" style="float:right;height:60px;"> Search</button>
								<ul style="width: 80%;">  
       							<div style="background-color: #f9f9f9; border: none; box-shadow: none;" class="well" id="result"></div>  
  							</ul>  
            </div>
        </div>
    </form>

</div>
<br><br><br>
<!--End of Search Filter-->
<header class="section-header wow fadeInUp">
                <h3>RECENT SEARCH</h3>
 </header><br>

 <section id="service">
    <div class="container">
        <div class="row about-cols">
					<?php if (isset($result1)) { ?>
            <div class="col-md-4 wow fadeInUp">
            <a href="<?php echo base_url('business/lead_lookup/').$result1->comp_id ?>">  <div class="about-col single-service">
                    <div class="img">
<!--                        <img src="http://saletancy.com/assest/img/Logo.png" alt="" class="img-fluid" width="150">-->
														<h2 style="color: red;"><?php echo $result1->comp_name[0] ?></h2>
                    </div>
                    <p style="font-size:20px">
                    <?php echo $result1->comp_name ?>
                  </p>
                  <a style="font-size:17px" href="//<?php echo $result1->website ?>" target="_blank">
                    <?php echo $result1->website ?>
                  </a>
                </div></a>
            </div>
					<?php }
				?>
					<?php if (isset($result2)) { ?>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
            <a href="<?php echo base_url('business/lead_lookup/').$result2->comp_id ?>">  <div class="about-col single-service">
                    <div class="img">
<!--                    	<img src="http://saletancy.com/assest/img/Logo.png" alt="" class="img-fluid" width="150">-->
													<h2 style="color: red;"><?php echo $result2->comp_name[0] ?></h2>
                    </div>
                    <p style="font-size:20px">
                      <?php echo $result2->comp_name ?>
                    </p>
                    <a style="font-size:17px" href="//<?php echo $result2->website ?>" target="_blank">
                      <?php echo $result2->website ?>
                    </a>
                    
                </div></a>
            </div>
					<?php }
				?>
					<?php if (isset($result3)) { ?>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.3s">
            <a href="<?php echo base_url('business/lead_lookup/').$result3->comp_id ?>">   <div class="about-col single-service">
                    <div class="img">
<!--                    		<img src="http://saletancy.com/assest/img/Logo.png" alt="" class="img-fluid" width="150">-->
														<h2 style="color: red;"><?php echo $result3->comp_name[0] ?></h2>
                    </div>
                    <p style="font-size:20px">
                    <?php echo $result3->comp_name ?>
                  </p>
                  <a style="font-size:17px" href="//<?php echo $result3->website ?>" target="_blank">
                    <?php echo $result3->website ?>
                  </a>
                    
                </div></a>
            </div>
					<?php }
				?>
        </div>
    </div>
</section>
<div style="height:26px">
</div>

<?php include 'include/footer.php'; ?>

<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css" type="text/css" media="all" />
<link rel="stylesheet" href="//static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready( function() {  
            $("#search").autocomplete({  
                minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>business/search_suggestions",  
                        dataType: 'json',  
                        type: 'POST',  
                        data: req,  
                        success:      
                        function(data){  
                            if(data.response =="true"){  
                                add(data.message);  
                            }  
                        },  
                    });  
                },  
                     
            });  
        }); 

</script> 