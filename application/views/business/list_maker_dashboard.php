<?php $pg='list_maker';?>
<?php include 'include/header.php'; ?>
<title>Home</title>
<?php include 'include/afterLogin_navbar.php'; ?>
<div style="height:110px">
</div>
<!--Get credit Section-->
<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">

        <div class="card" style="width: 57rem;height:4rem;background-color:rgba(255,255,255,0.6)">
  <div class="card-body">
    <h5 class="card-title" style="float:left"><div style="float:left;margin-top:-10px"><span style="font-size:45px;color:green"><?php echo $_SESSION['user_credits'] ?></span></div>&nbsp; List Credits Left</h5>
    <a href="<?php echo base_url('business/get_more_credits')?>"><h5 class="card-title" style="float:right;color:#28a745;">GET CREDITS</h5></a>
     
    <p class="card-text" style="float:right;margin-right:10px;font-size:15px">Want <b>500 free list credits</b> every week?</p>
  </div>
</div>

        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
<!--End of Get credit-->
<br>

<div class="container">
    <div class="row" style="min-height:304px">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="card " style="width: 57rem;height:auto;">
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <p class="card-text" style="float:left;display:inline-block"><h6>Get remaining leads from a previous list</h6></p>
                    <a href="<?php echo base_url('business/list_maker')?>" class="btn btn-success btn-lg" style="float:right;margin-top:-30px">+ Create New Lead List</a>
                    <br><br>
							
										
								<?php if(isset($query_result)) { ?>	
							
								<?php
foreach ($query_result->result() as $row) {?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div style="border:1px solid green;">

                                <div style="padding:15px">
                                    <p class="card-text" style="float:left;display:inline-block"><h7>List Name</h7></p><br>
                                    <h5 class="card-title">Lead List #<?php echo $row->ID ?></h5>
                                    <a class="btn btn-outline-success" data-toggle="modal" data-target="#myModal<?php echo $row->ID ?>" style="float:right;margin-top:-53px;margin-right:10px">Details</a>
                                </div>

<!--
                                <div class="container">
                                    <div class="row">
                                        <div class="col" style="border:1px solid green">
                                            <p class="card-text" style="float:left;display:inline-block;"><h7>List Name</h7></p><br>
                                            <h5 class="card-title">N/A</h5>


                                        </div>

                                        <div class="col" style="border:1px solid green">
                                            <p class="card-text" style="float:left;display:inline-block;"><h7>List Name</h7></p><br>
                                            <h5 class="card-title">N/A</h5>


                                        </div>

                                        <div class="col" style="border:1px solid green">
                                            <p class="card-text" style="float:left;display:inline-block;"><h7>List Name</h7></p><br>
                                            <h5 class="card-title">N/A</h5>


                                        </div>

                                        <div class="col" style="border:1px solid green">
                                            <p class="card-text" style="float:left;display:inline-block;"><h7>List Name</h7></p><br>
                                            <h5 class="card-title">N/A</h5>


                                        </div>

                                        <div class="col" style="border:1px solid green">
                                            <p class="card-text" style="float:left;display:inline-block;"><h7>List Name</h7></p><br>
                                            <h5 class="card-title">N/A</h5>


                                        </div>

                                        <div class="col" style="border:1px solid green">
                                            <p class="card-text" style="float:left;display:inline-block;"><h7>List Name</h7></p><br>
                                            <h5 class="card-title">IT</h5>


                                        </div>
                                    </div>
                                </div>
-->




                            </div>
                        </div>
                    </div>
							
							
										<div class="modal fade" id="myModal<?php echo $row->ID ?>">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content" style="width:0px;margin-left:-70px">
      
        <!-- Modal Header -->
       
        
        <!-- Modal body -->
        <div class="modal-body">
        <br><br><br>
       

        <div class="card " style="width: 57rem;height:30rem">
                <div class="card-body">
                <button type="button" class="close" style="float:right;margin-top:-20px">&times;</button>

                    <h5 class="card-title">Lead List #2</h5>
                    <p class="card-text" style="float:left;display:inline-block"><h6></h6></p>
                    <a href="<?php echo base_url('business/delete_past_list/').$row->ID ?>" class="btn btn-outline-danger btn-lg" style="float:right;margin-top:-30px;margin-right:30px">&times;  Delete List</a>
                    <br><br>
                    <hr>
							
                      <div class="row">
                        <div class="col-sm-3">
                        <h5 class="card-title">Employee :</h5>
                        </div>
                        <div class="col-sm-9">
                         <h6 class="card-text"><?php echo $row->emp_size_filter ?> Employees</h6>
                        </div>
                      </div>
					
											<div class="row">
                        <div class="col-sm-3">
                        <h5 class="card-title">Company Type :</h5>
                        </div>
                        <div class="col-sm-9">
                         <h6 class="card-text"><?php echo $row->comp_type_filter ?></h6>
                        </div>
                      </div>
					
											<div class="row">
                        <div class="col-sm-3">
                        <h5 class="card-title">Revenue :</h5>
                        </div>
                        <div class="col-sm-9">
                         <h6 class="card-text"><?php echo $row->revenue_filter ?></h6>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-3">
                        <h5 class="card-title">Location :</h5>
                        </div>
                        <div class="col-sm-9">
                         <h6 class="card-text"><?php echo $row->location_filter ?></h6>
                        </div>
                      </div>
					
											<div class="row">
                        <div class="col-sm-3">
                        <h5 class="card-title">Sub-Industry :</h5>
                        </div>
                        <div class="col-sm-9">
                         <h6 class="card-text"><?php echo $row->sub_industry_filter ?></h6>
                        </div>
                      </div>
					
					
											<div class="row">
                        <div class="col-sm-3">
                        <h5 class="card-title">Designation :</h5>
                        </div>
                        <div class="col-sm-9">
                         <h6 class="card-text"><?php echo $row->designation_filter ?></h6>
                        </div>
                      </div>
                    <hr>
                    <br>
                    <div style="text-align:center">
                    <form action="<?php echo base_url('business/download_old_list') ?>" method="post">
											<input name="emp_size_final_confirm" type="hidden" value="<?php echo $row->emp_size_filter ?>">
											<input name="funding_final_confirm" type="hidden" value="<?php echo $row->revenue_filter ?>">
											<input name="comp_type_final_confirm" type="hidden" value="<?php echo $row->comp_type_filter ?>">
											<input name="sub_industry_final_confirm" type="hidden" value="<?php echo $row->sub_industry_filter ?>">
											<input name="department_final_confirm" type="hidden" value="<?php echo $row->designation_filter ?>">
											<input name="location_final_confirm" type="hidden" value="<?php echo $row->location_filter ?>">
											<input name="start_number" type="hidden" value="0">
											<input name="end_number" type="hidden" value="<?php echo $row->downloaded_leads ?>">
											<button type="submit" class="btn btn-success " style="float:left; margin-left:200px">Get Previous Lead<br> (<?php echo $row->downloaded_leads ?>)</button>
										</form>
										<form action="<?php echo base_url('business/download_old_list') ?>" method="post">
											<input name="emp_size_final_confirm" type="hidden" value="<?php echo $row->emp_size_filter ?>">
											<input name="funding_final_confirm" type="hidden" value="<?php echo $row->revenue_filter ?>">
											<input name="comp_type_final_confirm" type="hidden" value="<?php echo $row->comp_type_filter ?>">
											<input name="sub_industry_final_confirm" type="hidden" value="<?php echo $row->sub_industry_filter ?>">
											<input name="department_final_confirm" type="hidden" value="<?php echo $row->designation_filter ?>">
											<input name="location_final_confirm" type="hidden" value="<?php echo $row->location_filter ?>">
											<input name="start_number" type="hidden" value="<?php echo $row->downloaded_leads ?>">
											<input name="end_number" type="hidden" value="<?php echo $row->total_leads ?>">
											<button type="submit" class="btn btn-success " style="margin-right:110px">Get Remaining Lead<br> (<?php echo ((int)$row->total_leads - (int)$row->downloaded_leads) ?>)</button>
										</form>

                     </div>
                    
                  </div>
        </div>
        <!-- Modal footer -->
       
        
      </div>
    </div>
  </div>
</div>
							
							
							
							
							
							
								
							
							
							
							
							
							
							
							
							
                            <br><br>
               
						<?php }													 
?>	
<?php }
?>
                    
</div>

<br><br>

<!--========================
        Detail Modal
==========================-->


<!--End of Modal-->
</div>
        </div>
    </div>
</div><br>
<?php include 'include/footer.php'; ?>

