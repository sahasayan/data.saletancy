/*====================================
            Button Highlighting
    ==================================== */
    /*Location */

    // Location
    $(".filterlocation").click(function(){
        count=0;
       $(".cityopt").each(function(index){
            if($(this).css("background-color") == "rgb(255, 255, 255)")        
              count++;
       });
       if(count>0)
             $(".location_select").addClass("onselect"); 
       else
             $(".location_select").removeClass("onselect"); 
    });

// West India
$(".wifilter").click(function(){
   
    $(".wi_select").addClass("onselect"); 

});
// Centeral India
$(".cifilter").click(function(){
   
    $(".ci_select").addClass("onselect"); 

});
// East India
$(".eifilter").click(function(){
   
    $(".ei_select").addClass("onselect"); 

});
// North East India
$(".neifilter").click(function(){
   
    $(".nei_select").addClass("onselect"); 

});
// North India
$(".nifilter").click(function(){
   
    $(".ni_select").addClass("onselect"); 

});
// South India
$(".sifilter").click(function(){
   
    $(".si_select").addClass("onselect"); 

});
// Revenue
$(".othersfilter").click(function(){
   
    $(".others_select").addClass("onselect"); 

});


    // End of Location

    

// Company Types
$(".companyfilter").click(function(){
    count=0;
   $(".companyopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".company_select").addClass("onselect"); 
   else
         $(".company_select").removeClass("onselect"); 
});

// Revenue
$(".revenuefilter").click(function(){
   
         $(".revenue_select").addClass("onselect"); 
  
});

// Industries
$(".industryfilter").click(function(){
   
    $(".industry_select").addClass("onselect"); 

});


    // Marketing
   $(".marketfilter").click(function(){
        count=0;
       $(".marketopt").each(function(index){
            if($(this).css("background-color") == "rgb(255, 255, 255)")        
              count++;
       });
       if(count>0)
             $(".market_select").addClass("onselect"); 
       else
             $(".market_select").removeClass("onselect"); 
});

// Sales
$(".salefilter").click(function(){
    count=0;
   $(".saleopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".sale_select").addClass("onselect"); 
   else
         $(".sale_select").removeClass("onselect"); 
});

 //It
$(".itfilter").click(function(){
    count=0;
   $(".itopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".it_select").addClass("onselect"); 
   else
         $(".it_select").removeClass("onselect"); 
});

//Product
$(".productfilter").click(function(){
    count=0;
   $(".productopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".product_select").addClass("onselect"); 
   else
         $(".product_select").removeClass("onselect"); 
});

//Engineering
$(".engfilter").click(function(){
    count=0;
   $(".engopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".eng_select").addClass("onselect"); 
   else
         $(".eng_select").removeClass("onselect"); 
});

//Founder
$(".founderfilter").click(function(){
    count=0;
   $(".founderopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".founder_select").addClass("onselect"); 
   else
         $(".founder_select").removeClass("onselect"); 
});

//Hr
$(".hrfilter").click(function(){
    count=0;
   $(".hropt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".hr_select").addClass("onselect"); 
   else
         $(".hr_select").removeClass("onselect"); 
});

//Finance
$(".financefilter").click(function(){
    count=0;
   $(".financeopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".finance_select").addClass("onselect"); 
   else
         $(".finance_select").removeClass("onselect"); 
});

//Operations
$(".operationfilter").click(function(){
    count=0;
   $(".operationopt").each(function(index){
        if($(this).css("background-color") == "rgb(255, 255, 255)")        
          count++;
   });
   if(count>0)
         $(".operation_select").addClass("onselect"); 
   else
         $(".operation_select").removeClass("onselect"); 
});



/* End of Button Highlighting*/


// Check all
   /*    ==========================================
               States and City
      =========================================*/
           
// Punjab ============
$("#punjab_all").change(function () {
    $("input:checkbox.punjab1").prop('checked', $(this).prop("checked"));
});
$(".punjab1").change(function () {
    _tot = $(".punjab1").length
    _tot_checked = $(".punjab1:checked").length;

    if (_tot != _tot_checked) {
        $("#punjab_all").prop('checked', false);
    }
});

// Rajasthan ==========
$("#rj_all").change(function () {
    $("input:checkbox.rj1").prop('checked', $(this).prop("checked"));
});
$(".rj1").change(function () {
    _tot = $(".rj1").length
    _tot_checked = $(".rj1:checked").length;

    if (_tot != _tot_checked) {
        $("#rj_all").prop('checked', false);
    }
});

// Telengana ==========
$("#telengana_all").change(function () {
    $("input:checkbox.telengana1").prop('checked', $(this).prop("checked"));
});
$(".telengana1").change(function () {
    _tot = $(".telengana1").length
    _tot_checked = $(".telengana1:checked").length;

    if (_tot != _tot_checked) {
        $("#telengana_all").prop('checked', false);
    }
});
      
// Gujarat
$("#gujarat_all").change(function () {
    $("input:checkbox.gujarat1").prop('checked', $(this).prop("checked"));
});
$(".gujarat1").change(function () {
    _tot = $(".gujarat1").length
    _tot_checked = $(".gujarat1:checked").length;

    if (_tot != _tot_checked) {
        $("#gujarat_all").prop('checked', false);
    }
});

// Maharashtra
$("#maharashtra_all").change(function () {
    $("input:checkbox.maharashtra1").prop('checked', $(this).prop("checked"));
});
$(".maharashtra1").change(function () {
    _tot = $(".maharashtra1").length
    _tot_checked = $(".maharashtra1:checked").length;

    if (_tot != _tot_checked) {
        $("#maharashtra_all").prop('checked', false);
    }
});

// MP
$("#mp_all").change(function () {
    $("input:checkbox.mp1").prop('checked', $(this).prop("checked"));
});
$(".mp1").change(function () {
    _tot = $(".mp1").length
    _tot_checked = $(".mp1:checked").length;

    if (_tot != _tot_checked) {
        $("#mp_all").prop('checked', false);
    }
});

// Delhi
$("#delhi_all").change(function () {
    $("input:checkbox.delhi1").prop('checked', $(this).prop("checked"));
});
$(".delhi1").change(function () {
    _tot = $(".delhi1").length
    _tot_checked = $(".delhi1:checked").length;

    if (_tot != _tot_checked) {
        $("#delhi_all").prop('checked', false);
    }
});

// Haryana
$("#haryana_all").change(function () {
    $("input:checkbox.haryana1").prop('checked', $(this).prop("checked"));
});
$(".haryana1").change(function () {
    _tot = $(".haryana1").length
    _tot_checked = $(".haryana1:checked").length;

    if (_tot != _tot_checked) {
        $("#haryana_all").prop('checked', false);
    }
});

// UP
$("#up_all").change(function () {
    $("input:checkbox.up1").prop('checked', $(this).prop("checked"));
});
$(".up1").change(function () {
    _tot = $(".up1").length
    _tot_checked = $(".up1:checked").length;

    if (_tot != _tot_checked) {
        $("#up_all").prop('checked', false);
    }
});

// Karnataka
$("#kt_all").change(function () {
    $("input:checkbox.kt1").prop('checked', $(this).prop("checked"));
});
$(".kt1").change(function () {
    _tot = $(".kt1").length
    _tot_checked = $(".kt1:checked").length;

    if (_tot != _tot_checked) {
        $("#kt_all").prop('checked', false);
    }
});

// Andhra Pradesh
$("#anp_all").change(function () {
    $("input:checkbox.anp1").prop('checked', $(this).prop("checked"));
});
$(".anp1").change(function () {
    _tot = $(".anp1").length
    _tot_checked = $(".anp1:checked").length;

    if (_tot != _tot_checked) {
        $("#anp_all").prop('checked', false);
    }
});

// Tamil Nadu
$("#td_all").change(function () {
    $("input:checkbox.td1").prop('checked', $(this).prop("checked"));
});
$(".td1").change(function () {
    _tot = $(".td1").length
    _tot_checked = $(".td1:checked").length;

    if (_tot != _tot_checked) {
        $("#td_all").prop('checked', false);
    }
});



    //   End of States and City


/*    ==========================================
               Industries
      =========================================*/
    
    
    //    Media & Entertainment
       $("#media_all").change(function () {
        $("input:checkbox.media1").prop('checked', $(this).prop("checked"));
    });
    $(".media1").change(function () {
        _tot = $(".media1").length
        _tot_checked = $(".media1:checked").length;

        if (_tot != _tot_checked) {
            $("#media_all").prop('checked', false);
        }
    });

// Networking & Telecommunications
    $("#net_all").change(function () {
        $("input:checkbox.net1").prop('checked', $(this).prop("checked"));
    });
    $(".net1").change(function () {
        _tot = $(".net1").length
        _tot_checked = $(".net1:checked").length;

        if (_tot != _tot_checked) {
            $("#net_all").prop('checked', false);
        }
    });

    // Energy & Utilities
    $("#energy_all").change(function () {
        $("input:checkbox.energy1").prop('checked', $(this).prop("checked"));
    });
    $(".energy1").change(function () {
        _tot = $(".energy1").length
        _tot_checked = $(".energy1:checked").length;

        if (_tot != _tot_checked) {
            $("#energy_all").prop('checked', false);
        }
    });

    // IT / ITES
    $("#it_all").change(function () {
        $("input:checkbox.it1").prop('checked', $(this).prop("checked"));
    });
    $(".it1").change(function () {
        _tot = $(".it1").length
        _tot_checked = $(".it1:checked").length;

        if (_tot != _tot_checked) {
            $("#it_all").prop('checked', false);
        }
    });

    // Consumer Packaged Goods
    $("#consumer_all").change(function () {
        $("input:checkbox.consumer1").prop('checked', $(this).prop("checked"));
    });
    $(".consumer1").change(function () {
        _tot = $(".consumer1").length
        _tot_checked = $(".consumer1:checked").length;

        if (_tot != _tot_checked) {
            $("#consumer_all").prop('checked', false);
        }
    });

    // Business Services
    $("#business_all").change(function () {
        $("input:checkbox.business1").prop('checked', $(this).prop("checked"));
    });
    $(".business1").change(function () {
        _tot = $(".business1").length
        _tot_checked = $(".business1:checked").length;

        if (_tot != _tot_checked) {
            $("#business_all").prop('checked', false);
        }
    });

    // BFSI
    $("#bfsi_all").change(function () {
        $("input:checkbox.bfsi1").prop('checked', $(this).prop("checked"));
    });
    $(".bfsi1").change(function () {
        _tot = $(".bfsi1").length
        _tot_checked = $(".bfsi1:checked").length;

        if (_tot != _tot_checked) {
            $("#bfsi_all").prop('checked', false);
        }
    });

    // Retail & Trading
    $("#retail_all").change(function () {
        $("input:checkbox.retail1").prop('checked', $(this).prop("checked"));
    });
    $(".retail1").change(function () {
        _tot = $(".retail1").length
        _tot_checked = $(".retail1:checked").length;

        if (_tot != _tot_checked) {
            $("#retail_all").prop('checked', false);
        }
    });

    // Metal & Mining
    $("#metal_all").change(function () {
        $("input:checkbox.metal1").prop('checked', $(this).prop("checked"));
    });
    $(".metal1").change(function () {
        _tot = $(".metal1").length
        _tot_checked = $(".metal1:checked").length;

        if (_tot != _tot_checked) {
            $("#metal_all").prop('checked', false);
        }
    });

    // Logistics & Transportation
    $("#logistic_all").change(function () {
        $("input:checkbox.logistic1").prop('checked', $(this).prop("checked"));
    });
    $(".logistic1").change(function () {
        _tot = $(".logistic1").length
        _tot_checked = $(".logistic1:checked").length;

        if (_tot != _tot_checked) {
            $("#logistic_all").prop('checked', false);
        }
    });

    // Aerospace & Defense
    $("#aero_all").change(function () {
        $("input:checkbox.aero1").prop('checked', $(this).prop("checked"));
    });
    $(".aero1").change(function () {
        _tot = $(".aero1").length
        _tot_checked = $(".aero1:checked").length;

        if (_tot != _tot_checked) {
            $("#aero_all").prop('checked', false);
        }
    });

    // Automobile & Auto Ancillaries
    $("#automobile_all").change(function () {
        $("input:checkbox.automobile1").prop('checked', $(this).prop("checked"));
    });
    $(".automobile1").change(function () {
        _tot = $(".automobile1").length
        _tot_checked = $(".automobile1:checked").length;

        if (_tot != _tot_checked) {
            $("#automobile_all").prop('checked', false);
        }
    });

    // Chemicals
    $("#chemical_all").change(function () {
        $("input:checkbox.chemical1").prop('checked', $(this).prop("checked"));
    });
    $(".chemical1").change(function () {
        _tot = $(".chemical1").length
        _tot_checked = $(".chemical1:checked").length;

        if (_tot != _tot_checked) {
            $("#chemical_all").prop('checked', false);
        }
    });

    // Electrical & Electronics
    $("#electrical_all").change(function () {
        $("input:checkbox.electrical1").prop('checked', $(this).prop("checked"));
    });
    $(".electrical1").change(function () {
        _tot = $(".electrical1").length
        _tot_checked = $(".electrical1:checked").length;

        if (_tot != _tot_checked) {
            $("#electrical_all").prop('checked', false);
        }
    });

    // Infrastructure
    $("#infrastructure_all").change(function () {
        $("input:checkbox.infrastructure1").prop('checked', $(this).prop("checked"));
    });
    $(".infrastructure1").change(function () {
        _tot = $(".infrastructure1").length
        _tot_checked = $(".infrastructure1:checked").length;

        if (_tot != _tot_checked) {
            $("#infrastructure_all").prop('checked', false);
        }
    });

    // Engineering
    $("#engineering_all").change(function () {
        $("input:checkbox.engineering1").prop('checked', $(this).prop("checked"));
    });
    $(".engineering1").change(function () {
        _tot = $(".engineering1").length
        _tot_checked = $(".engineering1:checked").length;

        if (_tot != _tot_checked) {
            $("#engineering_all").prop('checked', false);
        }
    });

    // Manufacturing & Production
    $("#manufacturing_all").change(function () {
        $("input:checkbox.manufacturing1").prop('checked', $(this).prop("checked"));
    });
    $(".manufacturing1").change(function () {
        _tot = $(".manufacturing1").length
        _tot_checked = $(".manufacturing1:checked").length;

        if (_tot != _tot_checked) {
            $("#manufacturing_all").prop('checked', false);
        }
    });

    // Textiles & Garments
    $("#textile_all").change(function () {
        $("input:checkbox.textile1").prop('checked', $(this).prop("checked"));
    });
    $(".textile1").change(function () {
        _tot = $(".textile1").length
        _tot_checked = $(".textile1:checked").length;

        if (_tot != _tot_checked) {
            $("#textile_all").prop('checked', false);
        }
    });

    // Education
    $("#education_all").change(function () {
        $("input:checkbox.education1").prop('checked', $(this).prop("checked"));
    });
    $(".education1").change(function () {
        _tot = $(".education1").length
        _tot_checked = $(".education1:checked").length;

        if (_tot != _tot_checked) {
            $("#education_all").prop('checked', false);
        }
    });

    // Government & NGO
    $("#government_all").change(function () {
        $("input:checkbox.government1").prop('checked', $(this).prop("checked"));
    });
    $(".government1").change(function () {
        _tot = $(".government1").length
        _tot_checked = $(".government1:checked").length;

        if (_tot != _tot_checked) {
            $("#government_all").prop('checked', false);
        }
    });

    // Life Science
    $("#lifescience_all").change(function () {
        $("input:checkbox.lifescience1").prop('checked', $(this).prop("checked"));
    });
    $(".lifescience1").change(function () {
        _tot = $(".lifescience1").length
        _tot_checked = $(".lifescience1:checked").length;

        if (_tot != _tot_checked) {
            $("#lifescience_all").prop('checked', false);
        }
    });

    // Hospitality
    $("#hospital_all").change(function () {
        $("input:checkbox.hospital1").prop('checked', $(this).prop("checked"));
    });
    $(".hospital1").change(function () {
        _tot = $(".hospital1").length
        _tot_checked = $(".hospital1:checked").length;

        if (_tot != _tot_checked) {
            $("#hospital_all").prop('checked', false);
        }
    });

    