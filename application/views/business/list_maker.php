<?php $pg='list_maker';?>
<?php include 'include/header.php'; ?>
<title>Home</title>
<?php include 'include/afterLogin_navbar.php'; ?>
<style>
    .button {
        background-color: #28a745; /* Green */
        color: white;
        border: none;
        border-radius: 5px;
        /*color: white;*/
        padding: 16px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 5px 5px;
        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
        cursor: pointer;
        /* width:100px;*/
        /* height: 120px*/
    }
        .button:hover {
            border-shadow: 5px solid #1e7e34;
            /* font-size:19px;  */
            box-shadow: 5px 7px 20px #8c8c8c;
        }
    /*=====================*/

    /*=====================*/
</style>
<script type="text/javascript">
    $(document).ready(function () {
        var user_id = window.location.search.substr(1).substr(3);
        $('#final_confirm_form_submit').click(function () {
            document.getElementById('progress_bar_leads').style.width = "30%";
            document.getElementById('progress_bar_leads').innerHTML = "25%";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Business/result_list/",
                data: $("#final_confirm_form").serialize(),
                success: function (res) {
                    console.log(res);
                    document.getElementById('number_of_results').innerHTML = res;
                    setTimeout(setSliders(res), 500);
                    document.getElementById('progress_bar_leads').style.width = "100%";
                    document.getElementById('progress_bar_leads').innerHTML = "100%";
                    document.getElementsByClassName('carousel-control-next')[0].style.display = "";
                   
									
                }
            });
        });
    });
		var slide_number=1;
		function next_clicked(){
			slide_number++;
			if(slide_number==4){
					document.getElementsByClassName('carousel-control-next')[0].style.display = "none";
			}
		}
		function prev_clicked(){
			if(slide_number>1){
				slide_number--;
				if(slide_number==3){
					document.getElementsByClassName('carousel-control-next')[0].style.display = "";
			}
			}
		}

    function setSliders(res) {
        //console.log(document.getElementById('ex13').setAttribute('data-slider-max', res));
        $("#ex13").slider('setAttribute', 'max', parseInt(res));
        $("#ex13").slider('refresh');
    }

    function resetLeadsNumber() {
        set_hidden_values();
        document.getElementById('number_of_leads_available1').innerHTML = "(loading...)";
        document.getElementById('number_of_leads_available2').innerHTML = "(loading...)";
        document.getElementById('number_of_leads_available3').innerHTML = "(loading...)";
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Business/get_matching_leads/",
            data: $("#final_confirm_form").serialize(),
            success: function (res) {
                //console.log(res);
                document.getElementById('number_of_leads_available1').innerHTML = res;
                document.getElementById('number_of_leads_available2').innerHTML = res;
                document.getElementById('number_of_leads_available3').innerHTML = res;
            }
        });
    }
</script>

<script type="text/javascript">
    var arrLoc = [];
    var arrInd = [];
    var arrSubInd = [];
    var arrDept = [];
    var arrCompType = [];
    var emp_size = '';
    var funding = '';
    function set_emp_size(value) {
        emp_size = value;
        console.log(emp_size);
        document.getElementById('emp_size_confirm').innerHTML = value;
        resetLeadsNumber();
    }
    function set_comp_type(name) {
        for (i = 0; i < arrCompType.length; i++) {
            if (name === arrCompType[i]) {
                arrCompType.splice(i, 1);
                return;
            }
        }
        arrCompType.push(name);
        for (i = 0; i < arrCompType.length; i++) {
            console.log(arrCompType[i]);
        }
        resetLeadsNumber();
    }

    function display_comp_type() {
        document.getElementById('comp_type_confirm').innerHTML = arrCompType.join(", ");
    }

    function set_funding(value) {
        console.log(value);
        funding = value;
        document.getElementById('funding_confirm').innerHTML = value;
        resetLeadsNumber();
    }
    function set_loc(name) {
        for (i = 0; i < arrLoc.length; i++) {
            if (name === arrLoc[i]) {
                arrLoc.splice(i, 1);
                resetLeadsNumber();
                return;
            }
        }
        arrLoc.push(name);
        for (i = 0; i < arrLoc.length; i++) {
            console.log(arrLoc[i]);
        }
        resetLeadsNumber();
    }
    function display_loc() {
        document.getElementById('loc_confirm').innerHTML = arrLoc.join(", ");
    }
    function set_industry(name) {
        var indName = "";
        if (name == "Advertising" || name == "Associations" || name == "Broadcast" || name == "Newspapers & Magazines" || name == "Entertainment" || name == "Printing & Publishing") {
            indName = "Media & Entertainment";
        } else if (name == "Telecommunications" || name == "Wireless Networking" || name == "Network Management") {
            indName = "Networking & Telecommunications";
        } else if (name == "Electricity" || name == "Gas" || name == "Petroleum" || name == "Waste Management" || name == "Water Resources" || name == "Environment") {
            indName = "Energy & Utilities";
        } else if (name == "Computer Leasing And Rental" || name == "Computer Maintenance & Services" || name == "IT Software" || name == "IT Outsourcing" || name == "System Integration" || name == "It Consultancy" || name == "Hardware") {
            indName = "IT / ITES";
        } else if (name == "Consumer Packaged Goods" || name == "Beverages & Tobacco" || name == "Food Manufacturing" || name == "General Processing" || name == "Other Agro Products") {
            indName = "Consumer Packaged Goods";
        } else if (name == "Household/health & Beauty" || name == "Accounting And Auditing" || name == "Legal Services" || name == "Bpo" || name == "Kpo" || name == "Hr Consultancy" || name == "Management Consulting" || name == "Professional Services") {
            indName = "Business Services";
        } else if (name == "Retail Banking" || name == "Stock Broking" || name == "Retail Finance" || name == "Insurance") {
            indName = "BFSI";
        } else if (name == "Retail" || name == "Retail- Apparel Manufacturing" || name == "Retail- Department Stores" || name == "Retail- Direct Marketing" || name == "Retail- Food" || name == "Retail- Hard Goods" || name == "Retail- Pharmacy/drug" || name == "Retail- Specialty" || name == "Retail- Others") {
            indName = "Retail & Trading";
        } else if (name == "Metals(Name)" || name == "Mining(Name)") {
            indName = "Metal & Mining";
        }
        else if (name == "Car Rental" || name == "Freight Airlines" || name == "Freight Forwarders & Arrangers" || name == "Freight Services" || name == "Lodging" || name == "Passenger Airlines" || name == "Rail" || name == "Shipping" || name == "Courier & Postal" || name == "Travel & Transportation" || name == "Travel Agencies" || name == "Urban Transportation") {
            indName = "Logistics & Transportation";
        }
        else if (name == "Aerospace" || name == "Defense" || name == "GIS") {
            indName = "Aerospace & Defense";
        }
        else if (name == "Automotive" || name == "Automobile Manufacturer" || name == "Auto Parts Manufacture" || name == "Automotive Suppliers" || name == "Heavy Equipment") {
            indName = "Automobile & Auto Ancillaries";
        }
        else if (name == "Chemicals") {
            indName = "Chemicals";
        }
        else if (name == "Electrical" || name == "Electronics") {
            indName = "Electrical & Electronics";
        }
        else if (name == "Construction" || name == "Cement" || name == "Building Materials" || name == "Rental Estate") {
            indName = "Infrastructure";
        }
        else if (name == "Fabrication & Assembly" || name == "Designing") {
            indName = "Engineering";
        }
        else if (name == "Product Name" || name == "Industrial Goods" || name == "Machinery" || name == "Furniture") {
            indName = "Manufacturing & Production";
        }
        else if (name == "Textiles" || name == "Garments" || name == "Leather & Footwear" || name == "Fabrics") {
            indName = "Textiles & Garments";
        }
        else if (name == "Basic/secondary Education" || name == "Tertiary/colleges" || name == "University" || name == "Higher Education") {
            indName = "Education";
        }
        else if (name == "Central/federal" || name == "Provincial/state" || name == "Local" || name == "NGO") {
            indName = "Government & NGO"
        }
        else if (name == "Pharmaceuticals" || name == "Hospitals" || name == "Medical Research" || name == "Clinics & Laboratories") {
            indName = "Life Science";
        }
        else if (name == "Hotels & Resorts" || name == "Restaurants & Clubs" || name == "Travel & Tourism") {
            indName = "Hospitality";
        }


        for (i = 0; i < arrSubInd.length; i++) {
            if (name === arrSubInd[i]) {
                arrSubInd.splice(i, 1);
                resetLeadsNumber();
                return;
            }
        }
        arrSubInd.push(name);

        //            for (i = 0; i < arrSubInd.length; i++) {
        //                console.log(arrSubInd[i]);
        //            }

        for (i = 0; i < arrInd.length; i++) {
            if (indName === arrInd[i]) {
                resetLeadsNumber();
                return;
            }
        }
        arrInd.push(indName);
        resetLeadsNumber();
    }
    function display_industry() {
        document.getElementById('ind_confirm').innerHTML = arrSubInd.join(", ");
    }
    function set_dept(name) {
        for (i = 0; i < arrDept.length; i++) {
            if (name === arrDept[i]) {
                arrDept.splice(i, 1);
                resetLeadsNumber();
                return;
            }
        }
        arrDept.push(name);
        for (i = 0; i < arrDept.length; i++) {
            console.log(arrDept[i]);
        }
        resetLeadsNumber();
    }
    function display_dept() {
        document.getElementById('dept_confirm').innerHTML = arrDept.join(", ");
        set_hidden_values();
				next_clicked();
    }
    function add_other_city() {
        var allFields = document.getElementsByClassName('other_city_name');
        for (i = 0; i < allFields.length; i++) {
            if (allFields[i].value != '') {
                arrLoc.push(allFields[i].value);
                allFields[i].value = '';
                resetLeadsNumber();
                return;
            }
        }
        resetLeadsNumber();
    }
    function set_hidden_values() {
        if (emp_size != '') {
            document.getElementById('emp_size_final_confirm').value = emp_size;
        }
        if (funding != '') {
            document.getElementById('funding_final_confirm').value = funding;
        }
        document.getElementById('comp_type_final_confirm').value = arrCompType.join(", ");
        document.getElementById('location_final_confirm').value = arrLoc.join(", ");
        document.getElementById('department_final_confirm').value = arrDept.join(", ");
        document.getElementById('sub_industry_final_confirm').value = arrSubInd.join(", ");
    }
	
		function set_industry_all(name){
			 for (i = 0; i < arrSubInd.length; i++) {
            if (name === arrSubInd[i]) {
                return;
            }
        }
        arrSubInd.push(name);
				resetLeadsNumber();
		}
		function set_loc_all(name){
			 for (i = 0; i < arrLoc.length; i++) {
            if (name === arrLoc[i]) {
                return;
            }
        }
        arrLoc.push(name);
				resetLeadsNumber();
		}
		function set_dept_all(name){
			 for (i = 0; i < arrDept.length; i++) {
            if (name === arrDept[i]) {
                return;
            }
        }
        arrDept.push(name);
				resetLeadsNumber();
		}
		function media_all_select(checkboxElem){
	if(checkboxElem.checked){
		set_industry_all("Advertising");
		set_industry_all("Associations");
		set_industry_all("Broadcast");
		set_industry_all("Entertainment");
		set_industry_all("Newspapers & Magazines");
		set_industry_all("Printing & Publishing");
	}else{
		set_industry("Advertising");
		set_industry("Associations");
		set_industry("Broadcast");
		set_industry("Entertainment");
		set_industry("Newspapers & Magazines");
		set_industry("Printing & Publishing");
	}
}

function telecommunications_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Telecommunications");
        set_industry_all("Wireless Networking");
        set_industry_all("Network Managemnet");
    }else{
        set_industry("Telecommunications");
        set_industry("Wireless Networking");
        set_industry("Network Managemnet");
    }
}

function energy_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Electricity");
        set_industry_all("Gas");
        set_industry_all("Petroleum");
        set_industry_all("Waste Managemen");
        set_industry_all("Water Resources");
        set_industry_all("Environment");
    }else{
        set_industry("Electricity");
        set_industry("Gas");
        set_industry("Petroleum");
        set_industry("Waste Managemen");
        set_industry("Water Resources");
        set_industry("Environment");
    }
}

function ites_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Computer Leasing And Rental");
        set_industry_all("Computer Maintenance & Services");
        set_industry_all("IT Software");
        set_industry_all("IT Outsourcing");
        set_industry_all("System Integration");
        set_industry_all("It Consultancy");
        set_industry_all("Hardware");
    }else{
        set_industry("Computer Leasing And Rental");
        set_industry("Computer Maintenance & Services");
        set_industry("IT Software");
        set_industry("IT Outsourcing");
        set_industry("System Integration");
        set_industry("It Consultancy");
        set_industry("Hardware");
    }
}

function consumer_packaged_goods_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Consumer Packaged Goods");
        set_industry_all("Beverages & Tobacco");
        set_industry_all("Food Manufacturing");
        set_industry_all("General Processing");
        set_industry_all("Other Agro Products");
    }else{
        set_industry("Consumer Packaged Goods");
        set_industry("Beverages & Tobacco");
        set_industry("Food Manufacturing");
        set_industry("General Processing");
        set_industry("Other Agro Products");
    }
}

function business_services_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Household/health & Beauty");
        set_industry_all("Accounting And Auditing");
        set_industry_all("Legal Services");
        set_industry_all("Bpo");
        set_industry_all("Kpo");
        set_industry_all("Hr Consultancy");
        set_industry_all("Management Consulting");
        set_industry_all("Professional Services");
    }else{
        set_industry("Household/health & Beauty");
        set_industry("Accounting And Auditing");
        set_industry("Legal Services");
        set_industry("Bpo");
        set_industry("Kpo");
        set_industry("Hr Consultancy");
        set_industry("Management Consulting");
        set_industry("Professional Services");
    }
}

function bfsi_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Retail Banking");
        set_industry_all("Stock Broking");
        set_industry_all("Retail Finance");
        set_industry_all("Insurance");
    }else{
        set_industry("Retail Banking");
        set_industry("Stock Broking");
        set_industry("Retail Finance");
        set_industry("Insurance");
    }
}

function retail_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Retail");
        set_industry_all("Retail- Apparel Manufacturing");
        set_industry_all("Retail- Department Stores");
        set_industry_all("Retail- Direct Marketing");
        set_industry_all("Retail- Food");
        set_industry_all("Retail- Hard Goods");
        set_industry_all("Retail- Pharmacy/drug");
        set_industry_all("Retail- Speciality");
        set_industry_all("Retail- Others");
    }else{
        set_industry("Retail");
        set_industry("Retail- Apparel Manufacturing");
        set_industry("Retail- Department Stores");
        set_industry("Retail- Direct Marketing");
        set_industry("Retail- Food");
        set_industry("Retail- Hard Goods");
        set_industry("Retail- Pharmacy/drug");
        set_industry("Retail- Speciality");
        set_industry("Retail- Others");
    }
}

function metal_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Metals(Name)");
        set_industry_all("Mining(Name)");
    }else{
        set_industry("Metals(Name)");
        set_industry("Mining(Name)");
    }
}

function logistics_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Car Rental");
        set_industry_all("Freight Airlines");
        set_industry_all("Freight Forwarders & Arrangers");
        set_industry_all("Freight Services");
        set_industry_all("Lodging");
        set_industry_all("Passenger Airlines");
        set_industry_all("Rail");
        set_industry_all("Shipping");
        set_industry_all("Courier & Postal");
        set_industry_all("Travel & Transportation");
        set_industry_all("Travel Agencies");
        set_industry_all("Urban Transportation");
    }else{
        set_industry("Car Rental");
        set_industry("Freight Airlines");
        set_industry("Freight Forwarders & Arrangers");
        set_industry("Freight Services");
        set_industry("Lodging");
        set_industry("Passenger Airlines");
        set_industry("Rail");
        set_industry("Shipping");
        set_industry("Courier & Postal");
        set_industry("Travel & Transportation");
        set_industry("Travel Agencies");
        set_industry("Urban Transportation");
    }
}

function aerospace_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Aerospace");
        set_industry_all("Defense");
        set_industry_all("GIS");
    }else{
        set_industry("Aerospace");
        set_industry("Defense");
        set_industry("GIS");
    }
}

function automobile_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Automotive");
        set_industry_all("Automobile Manufacturer");
        set_industry_all("Auto Parts Manufacture");
        set_industry_all("Automotive Suppliers");
        set_industry_all("Heavy Equipment");
    }else{
        set_industry("Automotive");
        set_industry("Automobile Manufacturer");
        set_industry("Auto Parts Manufacture");
        set_industry("Automotive Suppliers");
        set_industry("Heavy Equipment");
    }
}

function chemicals_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Chemicals");
    }else{
        set_industry("Chemicals");
    }
}

function electrical_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Electrical");
        set_industry_all("Electronics");
    }else{
        set_industry("Electrical");
        set_industry("Electronics");
    }
}

function infrastructure_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Construction");
        set_industry_all("Cement");
        set_industry_all("Building Materials");
        set_industry_all("Real Estate");
    }else{
        set_industry("Construction");
        set_industry("Cement");
        set_industry("Building Materials");
        set_industry("Real Estate");
    }
}

function engineering_industry_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Fabrication & Assembly");
        set_industry_all("Designing");
    }else{
        set_industry("Fabrication & Assembly");
        set_industry("Designing");
    }
}

function manufacturing_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Product Name");
        set_industry_all("Industrial Goods");
        set_industry_all("Machinery");
        set_industry_all("Furniture");
    }else{
        set_industry("Product Name");
        set_industry("Industrial Goods");
        set_industry("Machinery");
        set_industry("Furniture");
    }
}

function textiles_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Textiles");
        set_industry_all("Garments");
        set_industry_all("Leather & Footwear");
        set_industry_all("Fabrics");
    }else{
        set_industry("Textiles");
        set_industry("Garments");
        set_industry("Leather & Footwear");
        set_industry("Fabrics");
    }
}

function education_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Basic/secondary Education");
        set_industry_all("Tertiary/colleges");
        set_industry_all("University");
        set_industry_all("Higher Education");
    }else{
        set_industry("Basic/secondary Education");
        set_industry("Tertiary/colleges");
        set_industry("University");
        set_industry("Higher Education");
    }
}

function government_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Central/federal");
        set_industry_all("Provincial/state");
        set_industry_all("Local");
        set_industry_all("NGO");
    }else{
        set_industry("Central/federal");
        set_industry("Provincial/state");
        set_industry("Local");
        set_industry("NGO");
    }
}

function life_science_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Pharmaceuticals");
        set_industry_all("Hospitals");
        set_industry_all("Clinics & Laboratories");
        set_industry_all("Medical Research");
    }else{
        set_industry("Pharmaceuticals");
        set_industry("Hospitals");
        set_industry("Clinics & Laboratories");
        set_industry("Medical Research");
    }
}

function hospitality_science_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_industry_all("Hotels & Resorts");
        set_industry_all("Restaurants & Clubs");
        set_industry_all("Travel & Tourism");
    }else{
        set_industry("Hotels & Resorts");
        set_industry("Restaurants & Clubs");
        set_industry("Travel & Tourism");
    }
}













function gujarat_all_select(checkboxElem){
	if(checkboxElem.checked){
		set_loc_all("Ahmedabad");
		set_loc_all("Surat");
		set_loc_all("Vadodra");
	}else{
		set_loc("Ahmedabad");
		set_loc("Surat");
		set_loc("Vadodra");
	}
}

function maharashtra_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Mumbai");
        set_loc_all("Pune");
        set_loc_all("Nagpur");
    }else{
        set_loc("Mumbai");
        set_loc("Pune");
        set_loc("Nagpur");
    }
}

function madhyapradesh_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Bhopal");
        set_loc_all("Indore");
    }else{
        set_loc("Bhopal");
        set_loc("Indore");
    }
}

function delhi_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("New Delhi");
        set_loc_all("Delhi NCR");
    }else{
        set_loc("New Delhi");
        set_loc("Delhi NCR");
    }
}

function haryana_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Gurgaon");
        set_loc_all("Faridabad");
    }else{
        set_loc("Gurgaon");
        set_loc("Faridabad");
    }
}

function punjab_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Amritsar");
    }else{
        set_loc("Amritsar");
    }
}

function rajasthan_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Jaipur");
    }else{
        set_loc("Jaipur");
    }
}

function uttarpradesh_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Noida");
        set_loc_all("Lucknow");
        set_loc_all("Agra");
        set_loc_all("Aligarh");
        set_loc_all("Allahabad");
        set_loc_all("Ballia");
    }else{
        set_loc("Noida");
        set_loc("Lucknow");
        set_loc("Agra");
        set_loc("Aligarh");
        set_loc("Allahabad");
        set_loc("Ballia");
    }
}

function karnataka_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Bengaluru /Bangalore");
        set_loc_all("Mysore");
    }else{
        set_loc("Bengaluru /Bangalore");
        set_loc("Mysore");
    }
}

function andharapradesh_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Vijayawada");
        set_loc_all("Vishakhapatnam");
    }else{
        set_loc("Vijayawada");
        set_loc("Vishakhapatnam");
    }
}

function tamilnadu_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Chennai");
        set_loc_all("Coimbatore");
    }else{
        set_loc("Chennai");
        set_loc("Coimbatore");
    }
}

function telangana_all_select(checkboxElem){
    if(checkboxElem.checked){
        set_loc_all("Hyderabad / Secunderabad");
    }else{
        set_loc("Hyderabad / Secunderabad");
    }
}













function marketing_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
		set_dept_all("CMO");
		set_dept_all("VP of Product Marketing");
		set_dept_all("VP of Corporate Marketing");
		set_dept_all("VP of Online Marketing");
		set_dept_all("VP of public Marketing");
		set_dept_all("VP of Content Marketing");
		set_dept_all("Director of Marketing");
		set_dept_all("Director of Digital Marketing");
		set_dept_all("Director of Product Marketing");
		set_dept_all("Director of Public Relations");
		set_dept_all("Director of Content Marketing");
		set_dept_all("Director of Online Marketing");
		set_dept_all("Digital Marketing Manager");
		set_dept_all("Online Marketing Manager");
		set_dept_all("Product Marketing Manager");
		set_dept_all("Public Relations Manager");
		set_dept_all("Social Media Manager");
	}else{
		set_dept("CMO");
		set_dept("VP of Product Marketing");
		set_dept("VP of Corporate Marketing");
		set_dept("VP of Online Marketing");
		set_dept("VP of public Marketing");
		set_dept("VP of Content Marketing");
		set_dept("Director of Marketing");
		set_dept("Director of Digital Marketing");
		set_dept("Director of Product Marketing");
		set_dept("Director of Public Relations");
		set_dept("Director of Content Marketing");
		set_dept("Director of Online Marketing");
		set_dept("Digital Marketing Manager");
		set_dept("Online Marketing Manager");
		set_dept("Product Marketing Manager");
		set_dept("Public Relations Manager");
		set_dept("Social Media Manager");
	}
}

function sales_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("Head of Sales");
        set_dept_all("VP of Sales");
        set_dept_all("VP of Sales Department");
        set_dept_all("Director of Inside Sales");
        set_dept_all("Director of Sales");
        set_dept_all("Director of Sales Development");
        set_dept_all("Sales Manager");
        set_dept_all("SDR Manager");
        set_dept_all("ISR Manager");
        set_dept_all("Account Executive");
        set_dept_all("Account Manager");
    }else{
        set_dept("Head of Sales");
        set_dept("VP of Sales");
        set_dept("VP of Sales Department");
        set_dept("Director of Inside Sales");
        set_dept("Director of Sales");
        set_dept("Director of Sales Development");
        set_dept("Sales Manager");
        set_dept("SDR Manager");
        set_dept("ISR Manager");
        set_dept("Account Executive");
        set_dept("Account Manager");
    }
}

function it_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("CIO");
        set_dept_all("CISO");
        set_dept_all("VP of IT");
        set_dept_all("VP of Cloud Service");
        set_dept_all("VP of IT infrastructure");
        set_dept_all("Sys Admin");
        set_dept_all("Director of IT");
        set_dept_all("IT Manager");
    }else{
        set_dept("CIO");
        set_dept("CISO");
        set_dept("VP of IT");
        set_dept("VP of Cloud Service");
        set_dept("VP of IT infrastructure");
        set_dept("Sys Admin");
        set_dept("Director of IT");
        set_dept("IT Manager");
    }
}

function product_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("CFO");
        set_dept_all("VP of Product");
        set_dept_all("Director of Product");
        set_dept_all("VP of Cloud Service");
    }else{
        set_dept("CFO");
        set_dept("VP of Product");
        set_dept("Director of Product");
        set_dept("VP of Cloud Service");
    }
}

function engineering_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("CTO");
        set_dept_all("VP of Engineering");
        set_dept_all("VP of Infrastructure");
        set_dept_all("VP of Data Centers");
        set_dept_all("Director of Engineering");
        set_dept_all("Engineering Manager");
        set_dept_all("Devops");
        set_dept_all("SRE");
        set_dept_all("System Engineer");
        set_dept_all("Infrastructure Engineer");
        set_dept_all("Cloud Engineer");
    }else{
        set_dept("CTO");
        set_dept("VP of Engineering");
        set_dept("VP of Infrastructure");
        set_dept("VP of Data Centers");
        set_dept("Director of Engineering");
        set_dept("Engineering Manager");
        set_dept("Devops");
        set_dept("SRE");
        set_dept("System Engineer");
        set_dept("Infrastructure Engineer");
        set_dept("Cloud Engineer");
    }
}

function founders_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("CEO");
        set_dept_all("Founder");
    }else{
        set_dept("CEO");
        set_dept("Founder");
    }
}

function hr_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("Chef People Office");
        set_dept_all("Chef Talent Office");
        set_dept_all("Head of HR");
        set_dept_all("VP of HR");
        set_dept_all("VP of Recruiting");
        set_dept_all("VP of Talent Acquisition");
        set_dept_all("Head of Recruiting");
        set_dept_all("Director of Talent");
        set_dept_all("Director fo Recruiting");
        set_dept_all("Director of People");
        set_dept_all("Director of Talent");
        set_dept_all("Lead Recruiter");
        set_dept_all("HR Director");
        set_dept_all("HR Manager");
        set_dept_all("Director of People Analytics");
    }else{
        set_dept("Chef People Office");
        set_dept("Chef Talent Office");
        set_dept("Head of HR");
        set_dept("VP of HR");
        set_dept("VP of Recruiting");
        set_dept("VP of Talent Acquisition");
        set_dept("Head of Recruiting");
        set_dept("Director of Talent");
        set_dept("Director fo Recruiting");
        set_dept("Director of People");
        set_dept("Director of Talent");
        set_dept("Lead Recruiter");
        set_dept("HR Director");
        set_dept("HR Manager");
        set_dept("Director of People Analytics");
    }
}

function finance_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("CFO");
        set_dept_all("VP of Finance");
        set_dept_all("Director of Finance");
        set_dept_all("VP of Payments");
        set_dept_all("VP of Corporate Finance");
        set_dept_all("Director fo Corporate Finance");
        set_dept_all("Procurement");
        set_dept_all("Accounting");
        set_dept_all("Director of Recruiting");
        set_dept_all("Finance System");
    }else{
        set_dept("CFO");
        set_dept("VP of Finance");
        set_dept("Director of Finance");
        set_dept("VP of Payments");
        set_dept("VP of Corporate Finance");
        set_dept("Director fo Corporate Finance");
        set_dept("Procurement");
        set_dept("Accounting");
        set_dept("Director of Recruiting");
        set_dept("Finance System");
    }
}

function operations_all_select(element){
    if(element.style.backgroundColor == "rgb(255, 255, 255)"){
        set_dept_all("COO");
        set_dept_all("VP of Operations");
        set_dept_all("Director of Operations");
        set_dept_all("Operations Manager");
    }else{
        set_dept("COO");
        set_dept("VP of Operations");
        set_dept("Director of Operations");
        set_dept("Operations Manager");
    }
}
	
	
function none_selected(){
	arrLoc = [];
  arrInd = [];
  arrSubInd = [];
	arrCompType = [];
	funding = '';
	display_comp_type()
	display_loc()
	display_industry();
	document.getElementById('funding_confirm').innerHTML = '';
	document.getElementById('funding_final_confirm').value = '';
	resetLeadsNumber();
}
	
		
</script>



<div style="height:110px">
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="card" style="width: 57rem;height:4rem;background-color:rgba(255,255,255,0.6)">
                <div class="card-body">
                    <h5 class="card-title" style="float:left"><div style="float:left;margin-top:-10px"><span style="font-size:45px;color:green"><?php echo $_SESSION['user_credits'] ?></span></div>&nbsp; List Credits Left</h5>
                    <a href=""><h5 class="card-title" style="float:right;color:#28a745;">GET CREDITS</h5></a>
                    <p class="card-text" style="float:right;margin-right:10px;font-size:15px">Want <b>500 free list credits</b> every week?</p>
                </div>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
<!--End of Get credit-->
<br>
<div class="container">
    <div id="demo" class="carousel slide" data-ride="carousel" data-wrap="false" data-interval="false">
        <!-- Indicators -->
        <!-- <ul class="carousel-indicators" style="background-color:black">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
        <li data-target="#demo" data-slide-to="3"></li>
        <li data-target="#demo" data-slide-to="4"></li>
        <li data-target="#demo" data-slide-to="5"></li>
            </ul>-->
        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:24rem">
                            <div class="card-body">
                                <h5 class="card-title">How many of the 5000+ Employees would you like?</h5><br>
                                <p class="card-text"><h6>Drag the Slider to pick the range</h6></p>
                                <!--Employee Range Slider--><br>
                                <br>
                                <input class="ex18b" type="text"
                                       data-provide="slider"
                                       data-slider-ticks="[1, 2, 3,4,5,6,7,8, 9]"
                                       data-slider-ticks-labels='["1", "10", "50","200", "500","1000","5,000","10,000","10,000+"]'
                                       data-slider-min="1"
                                       data-slider-max="20000"
                                       data-slider-step="1"
                                       data-slider-value="3"
                                       data-slider-tooltip="hide"
                                       style="width:500px" />
                                <br>

                                <script>
                                    $(document).ready(function () {
																				var rangeVal1, rangeVal2, rangeVal3, rangeVal4, rangeVal5, rangeVal6, rangeVal7, rangeVal8;
																				<?php foreach ($result->result() as $row) {?>
																				rangeVal1= <?php echo $row->emp1; ?>;
																				rangeVal2= <?php echo $row->emp2; ?>;
																				rangeVal3= <?php echo $row->emp3; ?>;
																				rangeVal4= <?php echo $row->emp4; ?>;
																				rangeVal5= <?php echo $row->emp5; ?>;
																				rangeVal6= <?php echo $row->emp6; ?>;
																				rangeVal7= <?php echo $row->emp7; ?>;
																				rangeVal8= <?php echo $row->emp8; ?>;
																				<?php }		
																		?>

                                        var realValues = [1, 10, 50, 200, 500, 1000, 5000, 10000, 20000];
                                        var labelValues = ['1', '10', '50', '200','500', '1000', '5000', '10000', '20000'];

                                        var r = $('.ex18b').slider({
                                            max: 8,
                                            min: 0,
                                            step: 1,
                                            ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                                            formatter: function (val) {
                                                return labelValues[val];
                                            }
                                        })

                                            .on('change', function (data) {
                                                console.log(data);
																						
																								if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==10){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1;
																								}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==50){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2;
																								}else if(realValues[data.value.newValue[0]]==50 && realValues[data.value.newValue[1]]==200){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal3;
																								}else if(realValues[data.value.newValue[0]]==200 && realValues[data.value.newValue[1]]==500){
																									console.log(rangeVal1);
																									document.getElementById('lead_count_available1').innerHTML=rangeVal4;
																								}else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==1000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal5;
																								}else if(realValues[data.value.newValue[0]]==1000 && realValues[data.value.newValue[1]]==5000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal6;
																								}else if(realValues[data.value.newValue[0]]==5000 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==10000 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal8;
																								}
																							
																								else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==50){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2;
																								}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==200){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2 + rangeVal3;
																								}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==500){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2 + rangeVal3 + rangeVal4;
																								}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==1000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5;
																								}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==5000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6;
																								}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal1 + rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7 + rangeVal8;
																								}
																							
																								else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==200){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2 + rangeVal3;
																								}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==500){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2 + rangeVal3 + rangeVal4;
																								}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==1000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5;
																								}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==5000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6;
																								}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal2 + rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7 + rangeVal8;
																								}
																							
																							
																							
																								else if(realValues[data.value.newValue[0]]==50 && realValues[data.value.newValue[1]]==500){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal3 + rangeVal4;
																								}else if(realValues[data.value.newValue[0]]==50 && realValues[data.value.newValue[1]]==1000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal3 + rangeVal4 + rangeVal5;
																								}else if(realValues[data.value.newValue[0]]==50 && realValues[data.value.newValue[1]]==5000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6;
																								}else if(realValues[data.value.newValue[0]]==50 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==50 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal3 + rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7 + rangeVal8;
																								}
																							
																							
																								else if(realValues[data.value.newValue[0]]==200 && realValues[data.value.newValue[1]]==1000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal4 + rangeVal5;
																								}else if(realValues[data.value.newValue[0]]==200 && realValues[data.value.newValue[1]]==5000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal4 + rangeVal5 + rangeVal6;
																								}else if(realValues[data.value.newValue[0]]==200 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==200 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal4 + rangeVal5 + rangeVal6 + rangeVal7 + rangeVal8;
																								}
																							
																							
																								else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==5000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal5 + rangeVal6;
																								}else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal5 + rangeVal6 + rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal5 + rangeVal6 + rangeVal7 + rangeVal8;
																								}
																							
																							
																								else if(realValues[data.value.newValue[0]]==1000 && realValues[data.value.newValue[1]]==10000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal6 + rangeVal7;
																								}else if(realValues[data.value.newValue[0]]==1000 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML= rangeVal6 + rangeVal7 + rangeVal8;
																								}
																							
																								else if(realValues[data.value.newValue[0]]==5000 && realValues[data.value.newValue[1]]==20000){
																									document.getElementById('lead_count_available1').innerHTML=rangeVal7 + rangeVal8;
																								}
																							
                                                set_emp_size(realValues[data.value.newValue[0]] + '-' + realValues[data.value.newValue[1]]);
                                                
                                            })
                                            ;
                                    })



                                    $(".ex18b").slider({
                                        min: 0,
                                        max: 20000,
                                        value: [1, 2],
                                        labelledby: ['ex18-label-2a', 'ex18-label-2b']
                                    });
                                    $(".ex18b").slider({
                                        min: 0,
                                        max: 20000,
                                        value: 5,
                                        labelledby: 'ex18-label-1'
                                    });
                                </script>
                                <!--End of Range Slider-->
                                <br>
																<h6 class="card-text">There Are <span id="lead_count_available1" style="color:red"><b> NA </b></span> Leads Available In This Employee Range</h6>
																<br><br>
                                <h6 class="card-text">There Are <span id="number_of_leads_available1" style="color:red"><b> NA </b></span> Leads Available In Your Range</h6>
                            </div>
                            <div>
                                <button class="btn btn-outline-success" style="margin-bottom:10px" data-target="#demo" data-slide-to="1" onclick="next_clicked()">Apply Filter</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:24rem">
                            <div class="card-body">
                                <h5 class="card-title">Second, do you want to filter companies by any of these?</h5>
                                <p class="card-text">Pick as many as you want</p>
                                <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
                                <br>
                                <div class="container" style="width:80%">
                                    <div class="row justify-content-center">
                                        <button class="col sel button disab" data-toggle="modal" data-target="#" onclick="none_selected()">
                                            None<br>
                                        </button>
                                        <button class="col btn btn-success button company_select" data-toggle="modal" data-target="#company">
                                            Company Types
                                        </button>
                                        <button class="col btn btn-success button revenue_select" data-toggle="modal" data-target="#revenue">
                                            Revenue
                                        </button>
                                    </div>
                                    <div class="row justify-content-center">
                                        <button class="col btn btn-success button location_select" data-toggle="modal" data-target="#location">
                                            Location
                                        </button>
                                        <button class="col btn btn-success button industry_select" data-toggle="modal" data-target="#industry">
                                            Industry
                                        </button>
                                    </div>
                                </div><br>
                                <p class="card-text">There Are <span id="number_of_leads_available2"></span> Leads Available Based on The Filters You Have Choosen</p>
                            </div>
                            <div>
                                <button class="btn btn-outline-success" style="margin-bottom:10px" data-target="#demo" data-slide-to="2" onclick="next_clicked()">Apply Filter</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:26rem">
                            <div class="card-body">
                                <h5 class="card-title">Third,Which Department are you prospects in?</h5>
                                <p class="card-text">Pick as many as you want</p>
                                <!--  <a href="#" class="btn btn-primary">Go somewhere</a>-->
                                <br>
																<?php foreach ($result->result() as $row) {?>
                                <div class="container" style="width:80%">
                                    <div class="row justify-content-center">
                                        <button class="col btn btn-success button market_select" data-toggle="modal" data-target="#marketing">
                                            Marketing<br><h6 style="font-size:12px"><?php echo $row->Marketing; ?> Leads Available</h6>
                                        </button>
                                        <button class="col btn btn-success button sale_select" data-toggle="modal" data-target="#sales">
                                            Sales<br><h6 style="font-size:12px"><?php echo $row->Sales; ?> Leads Available</h6>
                                        </button>
                                        <button class="col btn btn-success button it_select" data-toggle="modal" data-target="#infot">
                                            It<br><h6 style="font-size:12px"><?php echo $row->It; ?> Leads Available</h6>
                                        </button>
                                    </div>
                                    <div class="row justify-content-center">
                                        <button class="col btn btn-success button product_select" data-toggle="modal" data-target="#product">
                                            Product<br><h6 style="font-size:12px"><?php echo $row->Product; ?> Leads Available</h6>
                                        </button>
                                        <button class="col btn btn-success button eng_select" data-toggle="modal" data-target="#eng">
                                            Engineering<br><h6 style="font-size:12px"><?php echo $row->EngineeringDept; ?> Leads Available</h6>
                                        </button>
                                        <button class="col btn btn-success button founder_select" data-toggle="modal" data-target="#founder">
                                            Founders<br><h6 style="font-size:12px"><?php echo $row->Founders; ?> Leads Available</h6>
                                        </button>
                                    </div>
                                    <div class="row justify-content-center">
                                        <button class="col btn btn-success button hr_select" data-toggle="modal" data-target="#hr">
                                            HR<br><h6 style="font-size:12px"><?php echo $row->HR; ?> Leads Available</h6>
                                        </button>
                                        <button class="col btn btn-success button finance_select" data-toggle="modal" data-target="#finance">
                                            Finance<br><h6 style="font-size:12px"><?php echo $row->Finance; ?> Leads Available</h6>
                                        </button>
                                        <button class="col btn btn-success button operation_select" data-toggle="modal" data-target="#operation">
                                            Operations<br><h6 style="font-size:12px"><?php echo $row->Operations; ?> Leads Available</h6>
                                        </button>
                                    </div>
                                </div>
															<?php }		
																		?>
                            </div>
                            <div>
                                <h6 class="card-text" style="margin-top: 0; margin-bottom: 10px;">There Are <span id="number_of_leads_available3" style="color:red"><b> NA </b></span> Leads Available In Your Range</h6>
                                <button class="btn btn-outline-success" data-target="#demo" data-slide-to="3" onclick="display_dept()">Apply Filter</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:30rem">
                            <div class="card-body">
                                <h5 class="card-title">Finally, Confirm that I got your list right?</h5>
                                <p class="card-text"></p>
                                <!--<a href="#" class="btn btn-primary">Go somewhere</a>-->
                                <div class="container">
                                    <div class="row">
                                        <div class=" col-sm-12">
                                            <h6 style="float:left">
                                                Company size:
                                            </h6>
                                            <h6><span style="float:right;color:#28a745" id="emp_size_confirm"></span></h6><br><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-sm-12">
                                            <h6 style="float:left">
                                                Department:
                                            </h6>
                                            <h6><span style="float:right;color:#28a745" id="dept_confirm"></span></h6><br><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-sm-12">
                                            <h6 style="float:left">
                                                Location:
                                            </h6>
                                            <h6><span style="float:right;color:#28a745" id="loc_confirm"></span></h6><br><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-sm-12">
                                            <h6 style="float:left">
                                                Funding:
                                            </h6>
                                            <h6><span style="float:right;color:#28a745" id="funding_confirm"></span></h6><br><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-sm-12">
                                            <h6 style="float:left">
                                                Sub-Industry:
                                            </h6>
                                            <h6><span style="float:right;color:#28a745" id="ind_confirm"></span></h6><br><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-sm-12">
                                            <h6 style="float:left">
                                                Company Type:
                                            </h6>
                                            <h6><span style="float:right;color:#28a745" id="comp_type_confirm"></span></h6><br><hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <form method="post" name="final_confirm_form" id="final_confirm_form">
                                    <input type="hidden" id="emp_size_final_confirm" name="emp_size_final_confirm" />
                                    <input type="hidden" id="comp_type_final_confirm" name="comp_type_final_confirm" />
                                    <input type="hidden" id="funding_final_confirm" name="funding_final_confirm" />
                                    <input type="hidden" id="location_final_confirm" name="location_final_confirm" />
                                    <input type="hidden" id="sub_industry_final_confirm" name="sub_industry_final_confirm" />
                                    <input type="hidden" id="department_final_confirm" name="department_final_confirm" />
                                    <button type="button" id="final_confirm_form_submit" class="btn btn-outline-success" style="margin-bottom:10px" data-target="#demo" data-slide-to="4" onclick="next_clicked()">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:24rem">
                            <div class="card-body">
                                <h5 class="card-title">We Are Working Hard To Find You The Right People</h5><br>
                                <p class="card-text">Shovelling Data Into The Server...</p>
                                <div class="container">
                                    <br><br>
                                    <div class="progress">
                                        <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" style="width:0%;" id="progress_bar_leads"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:24rem">
                            <div class="card-body">
                                <h5 class="card-title">How Many Of The <span id="number_of_results"></span> Would You Like ?</h5>
                                <br><br><br><br>
                                <input id="ex13" type="text" data-slider-min="0" data-slider-max="" data-slider-ticks-snap-bounds="30" style="width:400px" />
                                <br>
                                <span id="ex9CurrentSliderValLabel">Current Slider Value: <span id="ex9SliderVal"></span></span>
                                <script>
                                    $("#ex13").slider({
                                        //																	ticks: [0, 5000],
                                        //																	ticks_labels: ['Credits 0', 'Credits 5000'],
                                        ticks_snap_bounds: 30,
                                        tooltip: 'always'
                                    });

                                    $('#ex13').slider({
                                        formatter: function (value) {
                                            $("#ex9SliderVal").text(value);
                                            document.getElementById('number_of_leads_download').value = value;
                                        }
                                    });

                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:30rem">
                            <div class="card-body">
                                <h5 class="card-title">Excellent You Are All Set Download The Leads !</h5>
                                <p class="card-text"></p>
                                <a href="#" class="btn btn-success button" data-target="#demo" data-slide-to="7" onclick="next_clicked()">Get My Leads</a>
                                <br><br>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="col-sm-12" style="border:2px solid #deeaee;">
                                                <br>
                                                <h5 class="card-title"><span style="float:center;color:#28a745">$0 / Month</span></h5><hr>
                                                150 lead credits/week<hr>
                                                150 lead credits/week<hr>
                                                150 lead credits/week<hr>
                                                <a href="#" class="btn btn-success button" style="background-color:white;color:#28a745;border:1px solid #28a745">Your Current Plan</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="col-sm-12" style="border:2px solid #deeaee;">
                                                <br>
                                                <h5 class="card-title"><span style="float:center;color:#28a745">Share For Leads</span></h5><hr>
                                                150 lead credits/week<hr>
                                                150 lead credits/week<hr>
                                                150 lead credits/week<hr>
                                                <a href="#" class="btn  button" style="background-color:white;color:#28a745;border:1px solid #28a745">I Want More Leads</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="col-sm-12" style="border:2px solid #deeaee;">
                                                <br>
                                                <h5 class="card-title"><span style="float:center;color:#28a745">Enterprise Plan</span></h5><hr>
                                                150 lead credits/week<hr>
                                                150 lead credits/week<hr>
                                                150 lead credits/week<hr>
                                                <a href="#" class="btn  button" style="background-color:white;color:#28a745;border:1px solid #28a745">Enterprise Plan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="carousel-item" id="av">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="card text-center" style="width: 57rem;height:21rem">
                            <div class="card-body">
                                <h5 class="card-title">Excellent ! You're All Set ! Just Download The Leads !</h5>
                                <p class="card-text"></p><br><br>
                                <form action="<?php echo base_url('business/result_list_download') ?>" method="post">
                                    <input type="hidden" id="number_of_leads_download" name="number_of_leads_download" placeholder="Number of Leads">
                                    <button class="btn btn-success" type="submit">DOWNLOAD LEADS</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>
        <!-- Left and right controls -->
        <a class="carousel-control-prev" style="margin-left:-70px;" href="#demo" data-slide="prev" onclick="prev_clicked()">
            <!--<span class="carousel-control-prev-icon"></span>-->
            <span style="color:green;letter-spacing:4px;margin-right:10px"><b>PREV</b></span>   <img src="<?php echo base_url('assest')?>/img/Arrow-2.png" width="60">
        </a>
        <a class="carousel-control-next" style="margin-right:-60px;opacity:0.9" href="#demo" data-slide="next" onclick="next_clicked()">
            <!--<span class="carousel-control-next-icon"></span>-->
            <img src="<?php echo base_url('assest')?>/img/Arrow 1.png" width="60"> <span style="color:green;letter-spacing:4px;margin-left:10px"><b>NEXT</b></span>
        </a>
    </div>
    <br><br>
    <!--===============================
       MODAL
    ===============================-->
    <div class="modal fade" id="company">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">What Your Company Type Is?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
													<?php foreach ($result->result() as $row) {?>
                            <button class="col companyopt sel button" onclick="set_comp_type('India Top 1000')">
                                India Top 1000<br>
                            </button>
                            <button class="col companyopt sel button" onclick="set_comp_type('MNC')">
                                MNC<br>
                            </button>
                            <button class="col companyopt sel button" onclick="set_comp_type('SME')">
                                SME<br>
                            </button>
                            <button class="col companyopt sel button" onclick="set_comp_type('Start Up')">
                                Start Up<br>
                            </button>
												<?php }		
																		?>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn companyfilter  btn-outline-success" onclick="display_comp_type()">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--Revenue-->
    <div class="modal fade" id="revenue">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">What Revenue Do Your Target Company have?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-sm-12">
                                <div class="card text-center">
                                    <div class="card-body">
                                        <br>
                                        <br>
                                        <br>
                                        <input class="ex18c" type="text"
                                               data-provide="slider"
                                               data-slider-ticks="[1, 2, 3,4,5,6,7,8,9,10]"
                                               data-slider-ticks-labels='["Rs.0 Cr", "1", "10","100","250","500","1000","2500", "5000", "Rs.5000+"]'
                                               data-slider-min="1"
                                               data-slider-max="10000"
                                               data-slider-step="1"
                                               data-slider-value="3"
                                               data-slider-tooltip="hide"
                                               style="width:500px" />
                                        <br><br>
                                       

                                        <script>
                                            $(document).ready(function () {
																							
																								var range2Val1, range2Val2, range2Val3, range2Val4, range2Val5, range2Val6, range2Val7, range2Val8, range2Val9;
																									<?php foreach ($result->result() as $row) {?>
																									range2Val1= <?php echo $row->rev1; ?>;
																									range2Val2= <?php echo $row->rev2; ?>;
																									range2Val3= <?php echo $row->rev3; ?>;
																									range2Val4= <?php echo $row->rev4; ?>;
																									range2Val5= <?php echo $row->rev5; ?>;
																									range2Val6= <?php echo $row->rev6; ?>;
																									range2Val7= <?php echo $row->rev7; ?>;
																									range2Val8= <?php echo $row->rev8; ?>;
																									range2Val8= <?php echo $row->rev8; ?>;
																									range2Val9= <?php echo $row->rev9; ?>;
																									<?php }		
																							?>
                                                var realValues = [0, 1, 10, 100, 250, 500, 1000, 2500, 5000, 10000];
                                                var labelValues = ['0', '1', '10', '100', '250', '500', '1000', '2500', '5000', '10000'];

                                                var r = $('.ex18c').slider({
                                                    max: 9,
                                                    min: 0,
                                                    step: 1,
                                                    ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                                                    formatter: function (val) {
                                                        return labelValues[val];
                                                    }
                                                })

                                                    .on('change', function (data) {
                                                        console.log(data);
																											
																											
																												if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==1){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==10){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2;
																												}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==100){
																													document.getElementById('lead_count_available2').innerHTML=range2Val3;
																												}else if(realValues[data.value.newValue[0]]==100 && realValues[data.value.newValue[1]]==250){
																													document.getElementById('lead_count_available2').innerHTML=range2Val4;
																												}else if(realValues[data.value.newValue[0]]==250 && realValues[data.value.newValue[1]]==500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val5;
																												}else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==1000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val6;
																												}else if(realValues[data.value.newValue[0]]==1000 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val7;
																												}else if(realValues[data.value.newValue[0]]==2500 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val8;
																												}else if(realValues[data.value.newValue[0]]==5000 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val9;
																												}
																											
																												
																												else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==10){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==100){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==250){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3 + range2Val4;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3 + range2Val4 + range2Val5;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==1000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==0 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val1 + range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8 + range2Val9;
																												}
																											
																											
																												else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==100){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==250){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3 + range2Val4;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3 + range2Val4 + range2Val5;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==1000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==1 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val2 + range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8 + range2Val9;
																												}
																											
																											
																											
																												else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==250){
																													document.getElementById('lead_count_available2').innerHTML= range2Val3 + range2Val4;
																												}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==500){
																													document.getElementById('lead_count_available2').innerHTML= range2Val3 + range2Val4 + range2Val5;
																												}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==1000){
																													document.getElementById('lead_count_available2').innerHTML= range2Val3 + range2Val4 + range2Val5 + range2Val6;
																												}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7;
																												}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==10 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML= range2Val3 + range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8 + range2Val9;
																												}
																											
																											
																												else if(realValues[data.value.newValue[0]]==100 && realValues[data.value.newValue[1]]==500){
																													document.getElementById('lead_count_available2').innerHTML= range2Val4 + range2Val5;
																												}else if(realValues[data.value.newValue[0]]==100 && realValues[data.value.newValue[1]]==1000){
																													document.getElementById('lead_count_available2').innerHTML= range2Val4 + range2Val5 + range2Val6;
																												}else if(realValues[data.value.newValue[0]]==100 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val4 + range2Val5 + range2Val6 + range2Val7;
																												}else if(realValues[data.value.newValue[0]]==100 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==100 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML= range2Val4 + range2Val5 + range2Val6 + range2Val7 + range2Val8 + range2Val9;
																												}
																											
																											
																											
																												else if(realValues[data.value.newValue[0]]==250 && realValues[data.value.newValue[1]]==1000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val5 + range2Val6;
																												}else if(realValues[data.value.newValue[0]]==250 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val5 + range2Val6 + range2Val7;
																												}else if(realValues[data.value.newValue[0]]==250 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val5 + range2Val6 + range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==250 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val5 + range2Val6 + range2Val7 + range2Val8 + range2Val9;
																												}
																											
																												
																												else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==2500){
																													document.getElementById('lead_count_available2').innerHTML=range2Val6 + range2Val7;
																												}else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML= range2Val6 + range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==500 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val6 + range2Val7 + range2Val8 + range2Val9;
																												}
																											
																											
																												else if(realValues[data.value.newValue[0]]==1000 && realValues[data.value.newValue[1]]==5000){
																													document.getElementById('lead_count_available2').innerHTML= range2Val7 + range2Val8;
																												}else if(realValues[data.value.newValue[0]]==1000 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val7 + range2Val8 + range2Val9;
																												}
																											
																												else if(realValues[data.value.newValue[0]]==2500 && realValues[data.value.newValue[1]]==10000){
																													document.getElementById('lead_count_available2').innerHTML=range2Val8 + range2Val9;
																												}
																											
                                                        set_funding(realValues[data.value.newValue[0]] + '-' + realValues[data.value.newValue[1]]);
                                                     
                                                    })
                                                    ;
                                            })



                                            $(".ex18c").slider({
                                                min: 0,
                                                max: 10000,
                                                value: [1, 2],
                                                labelledby: ['ex18-label-2a', 'ex18-label-2b']
                                            });
                                            $(".ex18c").slider({
                                                min: 0,
                                                max: 10000,
                                                value: 5,
                                                labelledby: 'ex18-label-1'
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
								<h6 class="card-text" style="margin: auto;">There Are <span id="lead_count_available2" style="color:red"><b> NA </b></span> Leads Available In This Revenue Range</h6>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn revenuefilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Revenue-->
    <!--=========================
                Location
    ========================-->
    <div class="modal fade" id="location">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Where Are Your Target Companies Located?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
									<?php foreach ($result->result() as $row) {?>
                    <div class="container">
                        <div class="row justify-content-center">
													
                            <button class="col btn btn-success button wi_select cityopt" data-toggle="modal" data-target="#west">
                                West India<br><h6 style="font-size:12px"><?php echo $row->WestIndia; ?> Leads Available</h6>
                            </button>
                            <button class="col btn btn-success button ci_select cityopt" data-toggle="modal" data-target="#central">
                                Central India<br><h6 style="font-size:12px"><?php echo $row->CentralIndia; ?> Leads Available</h6>
                            </button>
                            <button class="col btn btn-success button ei_select cityopt" data-toggle="modal" data-target="#east">
                                East India<br><h6 style="font-size:12px"><?php echo $row->EastIndia; ?> Leads Available</h6>
                            </button>
                            <button class="col btn btn-success button nei_select cityopt" data-toggle="modal" data-target="#northeast">
                                North East India <br><h6 style="font-size:12px"><?php echo $row->NorthEastIndia; ?> Leads Available</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col btn btn-success button ni_select cityopt" data-toggle="modal" data-target="#north">
                                North India<br><h6 style="font-size:12px"><?php echo $row->NorthIndia; ?> Leads Available</h6>
                            </button>
                            <button class="col btn btn-success button si_select cityopt" data-toggle="modal" data-target="#south">
                                South India <br><h6 style="font-size:12px"><?php echo $row->SouthIndia; ?> Leads Available</h6>
                            </button>
                            <button class="col btn btn-success button others_select cityopt" data-toggle="modal" data-target="#others">
                                Others<br><h6 style="font-size:12px">0 Leads Available</h6>
                            </button>
                        </div>
                    </div>
									<?php }		
																		?>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn filterlocation  btn-outline-success" onclick="display_loc()">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--=========================
    Location Filter
    ===============================-->
    <!--West India-->
    <div class="modal fade" id="west">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#goa">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Goa</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="goa" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="goa_all" value="" onclick="set_loc('Goa')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#goac">
                                                <label><h6 style="display:inline-block"><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="goac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#gujarat">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Gujarat</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="gujarat" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="gujarat_all" value="" onclick="gujarat_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="gujarat1" value="" onclick="set_loc('Ahmedabad')"> <h6 style="display:inline-block">Ahmedabad</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="gujarat1" value="" onclick="set_loc('Surat')"> <h6 style="display:inline-block">Surat</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="gujarat1" value="" onclick="set_loc('Vadodra')"> <h6 style="display:inline-block">Vadodra</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#gujaratc">
                                                <label><h6 style="display:inline-block"><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="gujaratc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#maharashtra">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Maharashtra</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="maharashtra" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="maharashtra_all" value="" onclick="maharashtra_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="maharashtra1" value="" onclick="set_loc('Mumbai')"> <h6 style="display:inline-block">Mumbai</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="maharashtra1" value="" onclick="set_loc('Pune')"> <h6 style="display:inline-block">Pune</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="maharashtra1" value="" onclick="set_loc('Nagpur')"> <h6 style="display:inline-block">Nagpur</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#maharashtrac">
                                                <label><h6 style="display:inline-block"><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="maharashtrac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn wifilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of West India-->
    <!--Central India-->
    <div class="modal fade" id="central">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#mp">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Madhya Pradesh</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="mp" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="mp_all" value="" onclick="madhyapradesh_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="mp1" value="" onclick="set_loc('Bhopal')"><h6 style="display:inline-block">Bhopal</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="mp1" value="" onclick="set_loc('Indore')"><h6 style="display:inline-block">Indore</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#mpc">
                                                <label><h6 style="display:inline-block"><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="mpc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#chattisgarh">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Chattisgarh</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="chattisgarh" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="chattisgarh_all" value="" onclick="set_loc('Chattisgarh')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#chattisgarhc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="chattisgarhc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#delhi">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Delhi</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="delhi" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="delhi_all" value="" onclick="delhi_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="delhi1" value="" onclick="set_loc('New Delhi')"><h6 style="display:inline-block">New Delhi</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="delhi1" value="" onclick="set_loc('Delhi NCR')"><h6 style="display:inline-block">Delhi NCR</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#delhic">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="delhic" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn cifilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Central India-->
    <!--East India-->
    <div class="modal fade" id="east">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#bihar">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Bihar</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="bihar" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="bihar_all" value="" onclick="set_loc('Bihar')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#biharc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="biharc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#westbengal">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>West Bengal</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="westbengal" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="westbengal_all" value=""><h6 style="display:inline-block" onclick="set_loc('West Bengal')"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#westbengalc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="westbengalc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#orrisa">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Orrisa</label></h4>
                                    </div>
                                    <div class="checkbox">
                                        <div id="orrisa" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="orrisa_all" value="" onclick="set_loc('Orrisa')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#orrisac">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="orrisac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#jharkhand">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Jharkhand</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="jharkhand" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="jharkhand_all" value="" onclick="set_loc('Jharkhand')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#jharkhandc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="jharkhandc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn eifilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of East India-->
    <!--NEI-->
    <div class="modal fade" id="northeast">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#ap">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Arunachal Pradesh</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="ap" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="ap_all" value="" onclick="set_loc('Arunachal Pradesh')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#apc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="apc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#assam">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Assam</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="assam" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="assam_all" value="" onclick="set_loc('Assam')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#assamc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="assamc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#manipur">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Manipur</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="manipur" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="manipur_all" value="" onclick="set_loc('Manipur')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#manipurc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="manipurc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#Meghalaya">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Meghalaya</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="Meghalaya" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="meghalaya_all" value="" onclick="set_loc('Meghalaya')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#Meghalayac">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="Meghalayac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#mizoram">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Mizoram</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="mizoram" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="mizoram_all" value="" onclick="set_loc('Mizoram')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#mizoramc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="mizoramc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#nagaland">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Nagaland</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="nagaland" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="nagaland_all" value="" onclick="set_loc('Nagaland')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#nagalandc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="nagalandc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#sikkim">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Sikkim</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="sikkim" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="sikkim_all" value="" onclick="set_loc('Sikkim')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#sikkimc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="sikkimc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#tripura">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Tripura</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="tripura" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="tripura_all" value="" onclick="set_loc('Tripura')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#tripurac">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="tripurac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn neifilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of NEI-->
    <!--North India-->
    <div class="modal fade" id="north">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#haryana">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Haryana</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="haryana" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="haryana_all" value="" onclick="haryana_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="haryana1" value="" onclick="set_loc('Gurgaon')"> <h6 style="display:inline-block">Gurgaon</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="haryana1" value="" onclick="set_loc('Faridabad')"> <h6 style="display:inline-block">Faridabad</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#haryanac">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="haryanac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#hp">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Himachal Pradesh</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="hp" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="hp_all" value="" onclick="set_loc('Himachal Pradesh')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#hpc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="hpc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#jk">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Jammu And Kashmir</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="jk" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="jk_all" value="" onclick="set_loc('Jammu And Kashmir')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#jkc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="jkc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#punjab">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Punjab</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="punjab" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="punjab_all" value="" onclick="punjab_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="punjab1" value=""> <h6 style="display:inline-block">Amritsar</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#punjabc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="punjabc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#rj">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Rajasthan</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="rj" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="rj_all" value="" onclick="rajasthan_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="rj1" value="" onclick="set_loc('Jaipur')"> <h6 style="display:inline-block">Jaipur</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#rjc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="rjc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#up">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Uttar Pradesh</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="up" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="up_all" value="" onclick="uttarpradesh_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="up1" value="" onclick="set_loc('Noida')"> <h6 style="display:inline-block">Noida</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="up1" value="" onclick="set_loc('Lucknow')"> <h6 style="display:inline-block">Lucknow</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="up1" value="" onclick="set_loc('Agra')"> <h6 style="display:inline-block">Agra</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="up1" value="" onclick="set_loc('Aligarh')"> <h6 style="display:inline-block">Aligarh</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="up1" value="" onclick="set_loc('Allahabad')"> <h6 style="display:inline-block">Allahabad</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="up1" value="" onclick="set_loc('Ballia')"> <h6 style="display:inline-block">Ballia</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#upc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="upc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#ut">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Uttaranchal</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="ut" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="ut_all" value="" onclick="set_loc('Uttaranchal')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#utc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="utc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn nifilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of North India-->
    <!--South India-->
    <div class="modal fade" id="south">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#kt">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Karnataka</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="kt" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="kt_all" value="" onclick="karnataka_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="kt1" value="" onclick="set_loc('Bengaluru /Bangalore')"> <h6 style="display:inline-block">Bengaluru /Bangalore</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="kt1" value="" onclick="set_loc('Mysore')"> <h6 style="display:inline-block">Mysore</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#ktc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="ktc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#anp">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Andhara Pradesh</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="anp" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="anp_all" value="" onclick="andharapradesh_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="anp1" value="" onclick="set_loc('Vijayawada')"> <h6 style="display:inline-block">Vijayawada</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="anp1" value="" onclick="set_loc('Vishakhapatanam')"> <h6 style="display:inline-block">Vishakhapatnam</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#anpc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="anpc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#td">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Tamil Nadu</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="td" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="td_all" value="" onclick="tamilnadu_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="td1" value="" onclick="set_loc('Chennai')"> <h6 style="display:inline-block">Chennai</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="td1" value="" onclick="set_loc('Coimbatore')"> <h6 style="display:inline-block">Coimbatore</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#tdc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="tdc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#kerala">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Kerala</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="kerala" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="kerala_all" value="" onclick="set_loc('Kerala')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#keralac">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="keralac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#telengana">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Telangana</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="telengana" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="telengana_all" value="" onclick="telangana_all_select(this)"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="telengana1" value="" onclick="set_loc('Hyderabad / Secunderabad')"> <h6 style="display:inline-block">Hyderabad / Secunderabad</h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#telenganac">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="telenganac" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn sifilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of South India-->
    <!--Others-->
    <div class="modal fade" id="others">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Select State and City</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#Pondicherry">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Pondicherry</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="Pondicherry" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="pondicherry_all" value="" onclick="set_loc('Pondicherry')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#Pondicherryc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="Pondicherryc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#dnh">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Dadra & Nagar Havelli</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="dnh" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="dnh_all" value="" onclick="set_loc('Dadra & Nagar Havelli')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#dnhc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="dnhc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#dnd">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Daman & Diu</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="dnd" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="dnd_all" value="" onclick="set_loc('Daman & Diu')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#dndc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="dndc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#andnic">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Andaman & Nicobar</label></h5>
                                    </div>
                                    <div class="checkbox">
                                        <div id="andnic" class="collapse">
                                            <div class="checkbox">
                                                <label> <input type="checkbox" id="andnic_all" value="" onclick="set_loc('Andaman & Nicobar')"><h6 style="display:inline-block"><b>All Cities</b></h6></label>
                                            </div>
                                            <div class="checkbox" data-toggle="collapse" data-target="#andnicc">
                                                <label><h6><span class="fa fa-arrow-right"></span>Other</h6></label>
                                            </div>
                                            <div id="andnicc" class="collapse">
                                                <input class="other_city_name " type="text" placeholder="Enter your City" style="font-size:15px" />
                                                <button type="button" onclick="add_other_city()">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn othersfilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Others-->
    <!--End of Location Filter-->
    <!--End of Location-->
    <!--Industry-->
    <div class="modal fade" id="industry">
        <div class="modal-dialog modal-lg" style="float:left;margin-left:35px">
            <div class="modal-content" style="width:1300px;">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which Sectors are your target companies in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <form>
											<?php foreach ($result->result() as $row) {?>

                        <div class="container">
                            <div class="row justify-content-cente">
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#media">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Media & Entertainment</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->MediaEntertainment;?> Leads</span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="media" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="media_all" onclick="media_all_select(this)" value=""><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <input type="checkbox" class="media1" value="" onclick="set_industry('Advertising')"><h6 style="display:inline-block">Advertising</h6>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="media1" value="" onclick="set_industry('Associations')"><h6 style="display:inline-block">Associations</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="media1" value="" onclick="set_industry('Broadcast')"><h6 style="display:inline-block">Broadcast</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="media1" value="" onclick="set_industry('Newspapers & Magazines')"><h6 style="display:inline-block">Newspapers & Magazines</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="media1" value="" onclick="set_industry('Entertainment')"><h6 style="display:inline-block">Entertainment</h2></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="media1" value="" onclick="set_industry('Printing & Publishing')"><h6 style="display:inline-block">Printing & Publishing</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#net">
                                        <h5><span class="fa fa-arrow-right"></span> <label>Networking & Telecommunications</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->NetworkingTelecommunications	;?> Leads</span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="net" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="net_all" value="" onclick="telecommunications_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="net1" value="" onclick="set_industry('Telecommunications')"><h6 style="display:inline-block">Telecommunications</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="net1" value="" onclick="set_industry('Wireless networking')"><h6 style="display:inline-block">Wireless Networking</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="net1" value="" onclick="set_industry('Network Management')"><h6 style="display:inline-block">Network Managemnet</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#energy">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Energy & Utilities</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->EnergyUtilities; ?> Leads</span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="energy" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="energy_all" value="" onclick="energy_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="energy1" value="" onclick="set_industry('Electricity')"><h6 style="display:inline-block">Electricity</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="energy1" value="" onclick="set_industry('Gas')"><h6 style="display:inline-block">Gas</h4></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="energy1" value="" onclick="set_industry('Petroleum')"><h6 style="display:inline-block">Petroleum</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="energy1" value="" onclick="set_industry('Waste Management')"><h6 style="display:inline-block">Waste Managemen</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="energy1" value="" onclick="set_industry('Water Resources')"><h6 style="display:inline-block">Water Resources</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="energy1" value="" onclick="set_industry('Environment')"><h6 style="display:inline-block">Environment</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#it">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>IT / ITES</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->ITITES	; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="it" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="it_all" value="" onclick="ites_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('Computer Leasing And Rental')"><h6 style="display:inline-block">Computer Leasing And Rental</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('Computer Maintainance & Services')"><h6 style="display:inline-block">Computer Maintenance & Services</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('IT Software')"><h6 style="display:inline-block">IT Software</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('IT Outsourcing')"><h6 style="display:inline-block">IT Outsourcing</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('System Integration')"><h6 style="display:inline-block">System Integration</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('It Consultancy')"><h6 style="display:inline-block">It Consultancy</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="it1" value="" onclick="set_industry('Hardware')"><h6 style="display:inline-block">Hardware</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#consumer">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Consumer Packaged Goods</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->ConsumerPackagedGoods	; ?> Leads</span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="consumer" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="consumer_all" value="" onclick="consumer_packaged_goods_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="consumer1" value="" onclick="set_industry('Consumer Packed Goods')"><h6 style="display:inline-block">Consumer Packaged Goods</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="consumer1" value="" onclick="set_industry('Beverages & Tobacco')"><h6 style="display:inline-block">Beverages & Tobacco</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="consumer1" value="" onclick="set_industry('Food Manufacturing')"><h6 style="display:inline-block">Food Manufacturing</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="consumer1" value="" onclick="set_industry('General Processing')"><h6 style="display:inline-block">General Processing</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="consumer1" value="" onclick="set_industry('Other Agro Products')"><h6 style="display:inline-block">Other Agro Products</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#business">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Business Services</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->BusinessServices; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="business" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="business_all" value="" onclick="business_services_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Household/health & Beauty')"><h6 style="display:inline-block">Household/health & Beauty</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Accounting And Auditing')"><h6 style="display:inline-block">Accounting And Auditing</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Legal Services')"><h6 style="display:inline-block">Legal Services</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Bpo')"><h6 style="display:inline-block">Bpo</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Kpo')"><h6 style="display:inline-block">Kpo</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Hr consultancy')"><h6 style="display:inline-block">Hr Consultancy</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Management Consulting')"><h6 style="display:inline-block">Management Consulting</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="business1" value="" onclick="set_industry('Professional Services')"><h6 style="display:inline-block">Professional Services</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#bfsi">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>BFSI</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->BFSI; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="bfsi" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="bfsi_all" value="" onclick="bfsi_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="bfsi1" value="" onclick="set_industry('Retail Banking')"><h6 style="display:inline-block">Retail Banking</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="bfsi1" value="" onclick="set_industry('Stock Broking')"><h6 style="display:inline-block">Stock Broking</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="bfsi1" value="" onclick="set_industry('Retail Finance')"><h6 style="display:inline-block">Retail Finance</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="bfsi1" value="" onclick="set_industry('Insurance')"><h6 style="display:inline-block">Insurance</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#retail">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Retail & Trading</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->RetailTrading	; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="retail" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="retail_all" value="" onclick="retail_all_select(checkboxElem)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail')"><h6 style="display:inline-block">Retail</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Apparel Manufacturing')"><h6 style="display:inline-block">Retail- Apparel Manufacturing</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Department Stores')"><h6 style="display:inline-block">Retail- Department Stores</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Direct Marketing')"><h6 style="display:inline-block">Retail- Direct Marketing</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Food')"><h6 style="display:inline-block">Retail- Food</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Hard Goods')"><h6 style="display:inline-block">Retail- Hard Goods</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Pharmacy/drug')"><h6 style="display:inline-block">Retail- Pharmacy/drug</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Speciality')"><h6 style="display:inline-block">Retail- Speciality</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="retail1" value="" onclick="set_industry('Retail- Others')"><h6 style="display:inline-block">Retail- Others</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#metal">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Metal & Mining</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->MetalMining	; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="metal" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="metal_all" value="" onclick="metal_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="metal1" value="" onclick="set_industry('Metals(Name)')"><h6 style="display:inline-block">Metals(Name)</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="metal1" value="" onclick="set_industry('Mining(Name)')"><h6 style="display:inline-block">Mining(Name)</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#logistic">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Logistics & Transportation</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->LogisticsTransportation; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="logistic" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="logistic_all" value="" onclick="logistics_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Car Rental')"><h6 style="display:inline-block">Car Rental</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Freight Airlines')"><h6 style="display:inline-block">Freight Airlines</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Freight Forwarders & Arrangers')"><h6 style="display:inline-block">Freight Forwarders & Arrangers</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Freight Services')"><h6 style="display:inline-block">Freight Services</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Lodging')"><h6 style="display:inline-block">Lodging</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Passenger Airlines')"><h6 style="display:inline-block">Passenger Airlines</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Rail')"><h6 style="display:inline-block">Rail</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Shipping')"><h6 style="display:inline-block">Shipping</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Courier & Postal')"><h6 style="display:inline-block">Courier & Postal</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Travel & Transportation')"><h6 style="display:inline-block">Travel & Transportation</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Travel Agencies')"><h6 style="display:inline-block">Travel Agencies</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="logistic1" value="" onclick="set_industry('Urban transportation')"><h6 style="display:inline-block">Urban Transportation</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#aero">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Aerospace & Defense</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->AerospaceDefense; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="aero" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="aero_all" value="" onclick="aerospace_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="aero1" value="" onclick="set_industry('Aerospace')"><h6 style="display:inline-block">Aerospace</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="aero1" value="" onclick="set_industry('Defense')"><h6 style="display:inline-block">Defense</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="aero1" value="" onclick="set_industry('GIS')"><h6 style="display:inline-block">GIS</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--=========================-->
                                <div class="col-sm-6">
                                    <div type="" class="" data-toggle="collapse" data-target="#automobile">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Automobile & Auto Ancillaries</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->AutomobileAutoAncillaries	; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="automobile" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="automobile_all" value="" onclick="automobile_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="automobile1" value="" onclick="set_industry('Automotive')"><h6 style="display:inline-block">Automotive</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="automobile1" value="" onclick="set_industry('Automobile Manufacturer')"><h6 style="display:inline-block">Automobile Manufacturer</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="automobile1" value="" onclick="set_industry('Auto Parts Manufacture')"><h6 style="display:inline-block">Auto Parts Manufacture</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="automobile1" value="" onclick="set_industry('Automotive Suppliers')"><h6 style="display:inline-block">Automotive Suppliers</h6></label>
                                            </div>
                                            <div class="checkbox ">
                                                <label><input type="checkbox" class="automobile1" value="" onclick="set_industry('Heavy Equipment')"><h6 style="display:inline-block">Heavy Equipment</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#chemical">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Chemicals</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->Chemicals; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="chemical" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="chemical_all" value="" onclick="chemicals_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="chemical1" value="" onclick="set_industry('Chemicals')"><h6 style="display:inline-block">Chemicals</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#electrical">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Electrical & Electronics</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->ElectricalElectronics; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="electrical" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="electrical_all" value="" onclick="electrical_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="electrical1" value="" onclick="set_industry('Electrical')"><h6 style="display:inline-block">Electrical</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="electrical1" value="" onclick="set_industry('Electronics')"><h6 style="display:inline-block">Electronics</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#infrastructure">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Infrastructure</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->Infrastructure; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="infrastructure" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="infrastructure_all" value="" onclick="infrastructure_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="infrastructure1" value="" onclick="set_industry('Construction')"><h6 style="display:inline-block">Construction</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="infrastructure1" value="" onclick="set_industry('Cement')"><h6 style="display:inline-block">Cement</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="infrastructure1" value="" onclick="set_industry('Building Materials')"><h6 style="display:inline-block">Building Materials</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="infrastructure1" value="" onclick="set_industry('Real Estate')"><h6 style="display:inline-block">Real Estate</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#engineering">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Engineering</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->Engineering; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="engineering" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="engineering_all" value="" onclick="engineering_industry_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="engineering1" value="" onclick="set_industry('Fabrication & Assembly')"><h6 style="display:inline-block">Fabrication & Assembly</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="engineering1" value="" onclick="set_industry('Designing')"><h6 style="display:inline-block">Designing</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#manufacturing">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Manufacturing & Production</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->ManufacturingProduction; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="manufacturing" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="manufacturing_all" value="" onclick="manufacturing_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="manufacturing1" value="" onclick="set_industry('Product Name')"><h6 style="display:inline-block">Product Name</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="manufacturing1" value="" onclick="set_industry('Industrial Goods')"><h6 style="display:inline-block">Industrial Goods</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="manufacturing1" value="" onclick="set_industry('Machinery')"><h6 style="display:inline-block">Machinery</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="manufacturing1" value="" onclick="set_industry('Furniture')"><h6 style="display:inline-block">Furniture</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#textile">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Textiles & Garments</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->TextilesGarments; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="textile" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="textile_all" value="" onclick="textiles_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="textile1" value="" onclick="set_industry('Textiles')"><h6 style="display:inline-block">Textiles</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="textile1" value="" onclick="set_industry('Garments')"><h6 style="display:inline-block">Garments</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="textile1" value="" onclick="set_industry('Leather & Footwear')"><h6 style="display:inline-block">Leather & Footwear</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="textile1" value="" onclick="set_industry('Fabrics')"><h6 style="display:inline-block">Fabrics</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#education">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Education</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->Education; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="education" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="education_all" value="" onclick="education_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="education1" value="" onclick="set_industry('Basic/secondary Education')"><h6 style="display:inline-block">Basic/secondary Education</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="education1" value="" onclick="set_industry('Tertiary/colleges')"><h6 style="display:inline-block">Tertiary/colleges</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="education1" value="" onclick="set_industry('University')"><h6 style="display:inline-block">University</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="education1" value="" onclick="set_industry('Higher Education')"><h6 style="display:inline-block">Higher Education</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#government">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Government & NGO</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->GovernmentNGO; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="government" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="government_all" value="" onclick="government_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="government1" value="" onclick="set_industry('Central/federal')"><h6 style="display:inline-block">Central/federal</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="government1" value="" onclick="set_industry('Provincial/state')"><h6 style="display:inline-block">Provincial/state</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="government1" value="" onclick="set_industry('Local')"><h6 style="display:inline-block">Local</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="government1" value="" onclick="set_industry('NGO')"><h6 style="display:inline-block">NGO</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#lifescience">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Life Science</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->LifeScience; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="lifescience" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="lifescience_all" value="" onclick="life_science_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="lifescience1" value="" onclick="set_industry('Pharmaceuticals')"><h6 style="display:inline-block">Pharmaceuticals</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="lifescience1" value="" onclick="set_industry('Hospital')"><h6 style="display:inline-block">Hospitals</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="lifescience1" value="" onclick="set_industry('Clinics & Laboratories')"><h6 style="display:inline-block">Clinics & Laboratories</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="lifescience1" value="" onclick="set_industry('Medical Research')"><h6 style="display:inline-block">Medical Research</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div type="" class="" data-toggle="collapse" data-target="#hospitality">
                                        <h5> <span class="fa fa-arrow-right"></span> <label>Hospitality</label></h5><span style="float:right;color:#28a745;margin-top:-30px"><?php echo $row->Hospitality; ?> Leads<span>
                                    </div>
                                    <div class="checkbox">
                                        <div id="hospitality" class="collapse">
                                            <div class="checkbox">
                                                <input type="checkbox" id="hospital_all" value="" onclick="hospitality_science_all_select(this)"><h6 style="display:inline-block"><b>All</b></h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="hospital1" value="" onclick="set_industry('Hotels & Resorts')"><h6 style="display:inline-block">Hotels & Resorts</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="hospital1" value="" onclick="set_industry('Restaurants & Clubs')"><h6 style="display:inline-block">Restaurants & Clubs</h6></label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" class="hospital1" value="" onclick="set_industry('Travel & Tourism ')"><h6 style="display:inline-block">Travel & Tourism</h6></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--========================-->
                            </div>
                        </div>
																			<?php }		
																		?>
                    </form>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn industryfilter btn-outline-success" onclick="display_industry()">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Industry-->
    <!--=================
        3rd Slider Filter
    ======================-->
    <!--Marketing-->
    <div class="modal fade" id="marketing">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Marketing are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col marketopt sel button" onclick="marketing_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('CMO')">
                                <h6> CMO</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('VP of Marketing')">
                                <h6>  VP of Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('VP of Product Marketing')">
                                <h6> VP of Product Marketing</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col marketopt sel button" onclick="set_dept('VP of Corporate Marketing')">
                                <h6> VP of Corporate Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('VP of Online Marketing')">
                                <h6> VP of Online Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('VP of public Marketing')">
                                <h6> VP of public Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('VP of Content Marketing')">
                                <h6> VP of Content Marketing</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col marketopt sel button" onclick="set_dept('Director of Marketing')">
                                <h6> Director of Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Director of Digital Marketing')">
                                <h6>Director of Digital Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Director of Product Marketing')">
                                <h6> Director of Product Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Director of Corporate Marketing')">
                                <h6>Director of Corporate<br> Marketing</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col marketopt sel button" onclick="set_dept('Director of Public Relations')">
                                <h6> Director of<br> Public Relations</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Director of Content Marketing')">
                                <h6>Director of<br> Content Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Director of Online Marketing')">
                                <h6>Director of<br> Online Marketing</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Digital Marketing Manager')">
                                <h6> Digital Marketing<br> Manager</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col marketopt sel button" onclick="set_dept('Online Marketing Manager')">
                                <h6> Online Marketing<br> Manager</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Product Marketing Manager')">
                                <h6> Product Marketing<br> Manager</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Public Relations Manager')">
                                <h6>Public Relations<br> Manager</h6>
                            </button>
                            <button class="col marketopt sel button" onclick="set_dept('Social Media Manager')">
                                <h6> Social Media<br> Manager</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn marketfilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Marketing-->
    <!--Sales-->
    <div class="modal fade" id="sales">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Sales are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col saleopt sel button" onclick="sales_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('Head of Sales')">
                                <h6> Head of Sales</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('VP of Sales')">
                                <h6>  VP of Sales</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('VP of Sales Development')">
                                <h6> VP of Sales Development</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col saleopt sel button" onclick="set_dept('Director of Inside Sales')">
                                <h6> Director of Inside Sales</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('Director of Sales')">
                                <h5> Director of Sales</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('Director of Sales Development')">
                                <h6> Director of Sales Development</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('Sales Manager')">
                                <h6> Sales Manager</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col saleopt sel button" onclick="set_dept('SDR Manager')">
                                <h6> SDR Manager</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('ISR Manager')">
                                <h6>ISR Manager</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('Account Executive')">
                                <h6> Account Executive</h6>
                            </button>
                            <button class="col saleopt sel button" onclick="set_dept('Account Manager')">
                                <h6>Account Manager</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn salefilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Sales-->
    <!--It-->
    <div class="modal fade" id="infot">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in IT are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col itopt sel button" onclick="it_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col itopt sel button" onclick="set_dept('CIO')">
                                <h6> CIO</h6>
                            </button>
                            <button class="col itopt sel button" onclick="set_dept('CISCO')">
                                <h6> CISO</h6>
                            </button>
                            <button class="col itopt sel button" onclick="set_dept('VP of IT')">
                                <h6> VP of IT</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col itopt sel button" onclick="set_dept('VP of Cloud Service')">
                                <h6>VP of Cloud Service</h6>
                            </button>
                            <button class="col itopt sel button" onclick="set_dept('VP of IT infrastructure')">
                                <h6> VP of IT infrastructure</h6>
                            </button>
                            <button class="col itopt sel button" onclick="set_dept('Sys Admin')">
                                <h6>Sys Admin</h6>
                            </button>
                            <button class="col itopt sel button" onclick="set_dept('Director of IT')">
                                <h6> Director of IT</h6>
                            </button>
                        </div>
                        <div class="row justify-content-cente">
                            <button class="col-sm-3 itopt sel button" onclick="set_dept('IT Manager')">
                                <h6> IT Manager</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn itfilter btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of It-->
    <!--Product-->
    <div class="modal fade" id="product">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Product are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col productopt sel button" onclick="product_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col productopt sel button" onclick="set_dept('CPO')">
                                <h6> CPO</h6>
                            </button>
                            <button class="col productopt sel button" onclick="set_dept('VP of Product')">
                                <h6> VP of Product</h6>
                            </button>
                            <button class="col productopt sel button" onclick="set_dept('Director of Product')">
                                <h6> Director of Product</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col-sm-3 productopt sel button" onclick="set_dept('VP of Cloud Service')">
                                <h6>VP of Cloud Service</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn productfilter btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Product-->
    <!--Engineering-->
    <div class="modal fade" id="eng">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Engineering are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col engopt sel button" onclick="engineering_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('CTO')">
                                <h6> CTO</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('VP of Engineering')">
                                <h6>  VP of Engineering</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('VP of Infrastructure')">
                                <h6> VP of Infrastructure</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col engopt sel button" onclick="set_dept('VP of Data Centers')">
                                <h6> VP of Data Centers</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('Director of Engineering')">
                                <h6> Director of Engineering</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('Engineering Manager')">
                                <h6> Engineering Manager</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('Devops')">
                                <h6> Devops</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col engopt sel button">
                                <h6> SRE</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('System Engineer')">
                                <h6>System Engineer</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('Infrastructure Engineer')">
                                <h6> Infrastructure Engineer</h6>
                            </button>
                            <button class="col engopt sel button" onclick="set_dept('Cloud Engineer')">
                                <h6>Cloud Engineer</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn engfilter btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Engineer-->
    <!--Founder-->
    <div class="modal fade" id="founder">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Founders are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col founderopt sel button" onclick="founders_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col founderopt sel button" onclick="set_dept('CEO')">
                                <h6> CEO</h6>
                            </button>
                            <button class="col founderopt sel button" onclick="set_dept('Founder')">
                                <h6> Founder</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn founderfilter  btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Founder-->
    <!--HR-->
    <div class="modal fade" id="hr">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in HR are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col hropt sel button" onclick="hr_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Chef People Office')">
                                <h6> Chef People Office</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Chef Talent Office')">
                                <h6> Chef Talent Office</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Head of HR')">
                                <h6> Head of HR</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col hropt sel button" onclick="set_dept('VP of HR')">
                                <h6> VP of HR</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('VP of Recruiting')">
                                <h6> VP of Recruiting</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('VP of Talent Acquisition')">
                                <h6> VP of Talent Acquisition</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Head of Recruiting')">
                                <h6> Head of Recruiting</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col hropt sel button" onclick="set_dept('Director of Talent Acquisition')">
                                <h6> Director of Talent Acquisition</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Director of Recruiting')">
                                <h6>Director fo Recruiting</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Director of People')">
                                <h6> Director of People</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Director of Talent')">
                                <h6>Director of Talent</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col hropt sel button" onclick="set_dept('Lead Recruiter')">
                                <h6> Lead Recruiter</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('HR Director')">
                                <h6>HR Director</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('HR Manager')">
                                <h6>HR Manager</h6>
                            </button>
                            <button class="col hropt sel button" onclick="set_dept('Director of People Analytics')">
                                <h6>Director of People Analytics</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn hrfilter btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of HR-->
    <!--Finance-->
    <div class="modal fade" id="finance">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Finance are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col financeopt sel button" onclick="finance_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('CFO')">
                                <h6> CFO</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('VP of Finance')">
                                <h6> VP of Finance</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('Director of Finance')">
                                <h6> Director of Finance</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col financeopt sel button" onclick="set_dept('VP of Payments')">
                                <h6> VP of Payments</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('VP of Corporate Finance')">
                                <h6> VP of Corporate Finance</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('Director of Corporate Finance')">
                                <h6>Director fo Corporate Finance</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('Procurement')">
                                <h6> Procurement</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col financeopt sel button" onclick="set_dept('Accounting')">
                                <h6> Accounting</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('Director of Recruiting')">
                                <h6>Director of Recruiting</h6>
                            </button>
                            <button class="col financeopt sel button" onclick="set_dept('Finance System')">
                                <h6> Finance System</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn financefilter btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <!--End of Finance-->
    <!--Operations-->
    <div class="modal fade" id="operation">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="text-align:center">
                    <h5 class="modal-title">Which decision makers in Operations are you interested in?</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center">
                            <button class="col operationopt sel button" onclick="operations_all_select(this)">
                                <h6> All</h6>
                            </button>
                            <button class="col operationopt sel button" onclick="set_dept('COO')">
                                <h6> COO</h6>
                            </button>
                            <button class="col operationopt sel button" onclick="set_dept('VP of Operations')">
                                <h6> VP of Operations</h6>
                            </button>
                            <button class="col operationopt sel button" onclick="set_dept('Director of Operations')">
                                <h6> Director of Operations</h6>
                            </button>
                        </div>
                        <div class="row justify-content-center">
                            <button class="col-sm-3 operationopt sel button" onclick="set_dept('Operations Manager')">
                                <h6> Operations Manager</h6>
                            </button>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button data-dismiss="modal" class="btn operationfilter btn-outline-success">Apply Filter</button>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Operations-->
<!--End of Modal-->
<script>
    clicked = true;

    // =============

    $(".sel").click(function () {
        if ($(this).css("background-color") == "rgb(40, 167, 69)") {
            $(this).css('background-color', 'white');
            $(this).css('color', 'green');
            $(this).css('border', '1px solid green');

        }
        else {

            $(this).css('background-color', '#28a745');
            $(this).css('color', 'white');
            $(this).css('border', '0');


        }
    });
    
    disabledVal=0;
$(".disab").click(function(){
    if(disabledVal==0)
    {
    $(".company_select").attr("disabled", "disabled");
    $(".location_select").attr("disabled", "disabled");
    $(".industry_select").attr("disabled", "disabled");
    $(".revenue_select").attr("disabled", "disabled");
    disabledVal++;
    }
    else{
        
        $(".company_select").removeAttr("disabled");
        $(".location_select").removeAttr("disabled");
        $(".industry_select").removeAttr("disabled");
          $(".revenue_select").removeAttr("disabled");
          disabledVal--;
    }
})
</script>
<script>


</script>
<style>
    .onselect {
        background-color: white;
        color: green;
        border: 1px solid green;
    }
</style>

<script src="<?php echo base_url('application')?>/views/business/js/list_maker_checkbox.js"></script>
    <?php include 'include/footer.php'; ?>
