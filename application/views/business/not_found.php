<?php $pg='not_found';?>
<?php include 'include/header.php'; ?>
<title>Home</title>
<?php include 'include/afterLogin_navbar.php'; ?>
<style>
   
    </style>
<div style="height:110px">
  
</div>
<!--==========================
     Search Filter
    ========================-->
    <div class="container">

    <form class="form" action="<?php echo base_url();?>business/lead_lookup/search" method="post">
        <div class="form-group">
            <div class="col-sm-12" style="text-align:right">
                <input type="text" class="form-control" id="search" placeholder="Which Company Are You Trying To Contact?" name="searchBox" style="width:91%;height:60px;float:left;">
                <button type="submit" class="btn btn-lg btn-success fa fa-search" style="float:right;height:60px;"> Search</button>
								<ul style="width: 80%;">  
       							<div style="background-color: #f9f9f9; border: none; box-shadow: none;" class="well" id="result"></div>  
  							</ul>  
            </div>
        </div>
    </form>

</div>
<!--End of Search Filter-->
<br><br><br><br>
<div class="container">
<div class="row">
<div class="col-sm-12" style="text-align:center">
 <button type="button" class="btn-danger btn btn-lg"><?php echo $searched_company; ?> Is Not Present In Our Database</button>
</div>
</div>
</div>
<br><br>

<header class="section-header wow fadeInUp" style="text-align:center">
                <h3 >Do you wish to suggest this addition to our database?</h3>
            </header>
<br><br><br>
            <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="start-project-now" style="text-align:center">
                <button class="g-t-project" data-toggle="modal" data-target="#myModal">
                    SUGGEST
                </button>
            </div>
        </div>
    </div>
</div>
<br><br>


    <!--===================================
	             Modal Suggestion
     ===================================-->
     <div class="modal fade" id="myModal" role="dialog">
     <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
             <div class="modal-body" style="height:500px">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <!--Log In-->
                 <div class="limiter">
                     <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
                         <div class="wrap-login300 p-l-55 p-r-55 p-t-6 p-b-44" style="margin-top:-70px">
                             <form class="login100-form validate-form" style="margin-top:-60px">
                                 <span class="login100-form-title p-b-49">
                                     <h4>Suggest Another Company</h4>
                                 </span>
                                <!-- <div class="flex-c-m">
                                     <a href="#" class="login100-social-item bg1">
                                         <i class="fa fa-facebook"></i>
                                     </a>

                                     <a href="#" class="login100-social-item bg2">
                                         <i class="fa fa-twitter"></i>
                                     </a>

                                     <a href="#" class="login100-social-item bg3">
                                         <i class="fa fa-google"></i>
                                     </a>
                                 </div>-->
                                 <div class="wrap-input100 ">
                                     <span class="label-input100">Company Name</span>
                                     <input class="input100" type="text" name="company_name" placeholder="Company Name">
                                     <span class="focus-input100"></span>
                                 </div><br>
                                 <div class="wrap-input100 " >
                                     <span class="label-input90 ">Website</span>
                                     <input class="input100" type="text" name="Com_website" placeholder="Enter Company Website" required>
                                     <span class="focus-input100"></span>
                                 </div>
                                 <div class="text-right p-t-8 p-b-31">
                                     <a href="#">
                                     </a>
                                 </div>
                                 <div class="container-login100-form-btn">
                                     <div class="wrap-login100-form-btn">
                                         <div class="login100-form-bgbtn"></div>
                                         <button class="login100-form-btn" onclick="alert('Thank You');" data-dismiss="modal">
                                             SUGGEST
                                         </button>
                                     </div>
                                 </div>
                                 <div class="txt1 text-center p-t-10 ">
                                     <span>
                                     </span>
                                 </div>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>

	<!--End of Suggestion Form 1-->
<?php include 'include/footer.php';  ?>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/base/jquery-ui.css" type="text/css" media="all" />  
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/   css" media="all" />  
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript">
$(document).ready( function() {  
            $("#search").autocomplete({  
                minLength: 1,  
                source:   
                function(req, add){  
                    $.ajax({  
                        url: "<?php echo base_url(); ?>business/search_suggestions",  
                        dataType: 'json',  
                        type: 'POST',  
                        data: req,  
                        success:      
                        function(data){  
                            if(data.response =="true"){  
                                add(data.message);  
                            }  
                        },  
                    });  
                },  
                     
            });  
        }); 

</script> 