
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Home</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link href="<?php echo base_url() ?>assest/img/favicon.png" rel="icon">
    <link href="<?php echo base_url() ?>assest/img/apple-touch-icon.png" rel="apple-touch-icon">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
    <link href="<?php echo base_url() ?>application/views/public/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>application/views/public/responsive.css" rel="stylesheet">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link href="<?php echo base_url() ?>application/views/public/login.css" rel="stylesheet" type="text/css">
	
	
   <script src="<?php echo base_url() ?>assest/ad_assest/plugins/bower_components/jquery/dist/jquery.min.js"></script>
	
	<script type="text/javascript">
$(document).ready(function() {
		var user_id=window.location.search.substr(1).substr(3);
   $('#signup_form_submit').click(function(){
    $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>Welcome/register_user/?="+user_id,    
    data: $("#signup_form").serialize(),
    success: function(res){
					if(res!=""){
						alert(res);
					}else{
						window.location = "<?php echo base_url('welcome'); ?>";
					}
        }
    });
  });
});
</script>
	
	
	</head>

   


    <!--====================<!-- #header -->
    <!--==========================
      Intro Section
    ============================-->
    <section id="intro">
        <div class="intro-container">
            <nav class="navbar navbar-default " data-spy="affix" data-offset-top="60">
        
              <div class="navbar-header" id="header">
                <div id="logo">
                  <a class="navbar-brand" href="#"><img src="<?php echo base_url() ?>assest/img/logo.png" alt="saletancy logo" width="140" height="90"></a>
                </div>
                <div class="collapse navbar-collapse " id="log" style="float:right;display:inline;">
                  <button onclick="document.getElementById('id02').style.display='block'" class="w3-button w3-green w3-large">Login</button>  
 <div id="id02" class="w3-modal" style="display: none;">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
     <div class="w3-center"><br>
        <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
      </div>



<body data-lang-list="en,es-xl,pt-br,de,fr" class="og-context-node-17848">


<!-- SIGNUP PART STARTS -->
<div class="main">
  <div id="signupcontainer"> 
   
    <div class="signup-box"> 
      <!-- SIGNUP FORM STARTS -->
      <form  name="signupform" method="post" class="form" action="<?php echo base_url('login/user_login'); ?>">
        <section class="signupcontainer">
          <div class="za-email-container added-placeholder1 sgfrm"> <span class="placeholder">Email</span>
            <input type="email" autofocus  name="email" class="form-input sgnemail" tabindex="1" id="emailfield" value=""  >
          </div>
          <div class="za-password-container sgfrm"> <span class="placeholder">Password</span>
            <input type="password"   name="password" id="password" class="form-input sgnpaswrd"  tabindex="1" maxlength="250">
            <div class="field-msg">
              <div onclick="togglePasswordField(8);" id="show-password" class="column show-password"> <span id="show-password-icon" class="icon-medium uncheckedpass"></span>
                <label id="show-password-label">Show</label>
              </div>
              <p class="message"><span id="errormg" class="pwderror">Minimum 8 characters</span></p>
              <div class="pwdparent">
                <div id="pwdstrength"></div>
                <div class="pwdtext"></div>
              </div>
            </div>
          </div>
          
          <div class="sgnbtnmn">
            

            <input  type="checkbox" checked="checked"> Remember me
            <div class="sgnbtn">

              <input type="submit" tabindex="1" class="signupbtn " id="signupbtn" value="Submit" onclick="">
               <span class="w3-right w3-padding w3-hide-small">Forgot <a href="">password?</a></span>
            
            </div>
          </div>
        </section>
      </form>
      
      
      <div class="socl-signup">
        <p> or sign in using
         <i class="fa fa-google-plus" style="font-size:25px;"></i>
</p>      </div>
       
    </div>
    
  
  </div>
</div>

</body>
        </div>
      </div>


<!-- The Modal -->



  <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-green w3-large" href="#signup">signup</button>

  <div id="id01" class="w3-modal" style="display: none;">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

      <div class="w3-center"><br>
        <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
        <h1>Sign-Up</h1>
      </div>
			<div class="w3-container">
      
				<form name="signup_form" id="signup_form" action="" method="post">
        <div class="w3-section">
         
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter First name" name="firstname" required>
          <input class="w3-input w3-border" type="text" placeholder="Enter Last name" name="lastname" required>
          <input class="w3-input w3-border" type="text" placeholder="Enter Email" name="email" required>
          <input class="w3-input w3-border" type="text" placeholder="Enter Username" name="username" required>
          <input class="w3-input w3-border" type="text" placeholder="Enter company name" name="company" required>
          <input class="w3-input w3-border" type="text" placeholder="Enter Job Title" name="jobtitle" required>
          <input class="w3-input w3-border" type="tel" placeholder="Enter phone no" name="telephone" required>
          <input class="w3-input w3-border" type="password" placeholder="Enter Password" name="password" required>
          <input class="w3-input w3-border" type="password" placeholder="confirm Password" name="cpassword" required>
          <input type="Checkbox" required>I agree to the terms and conditions
          <button class="w3-button w3-block w3-green w3-section w3-padding" id="signup_form_submit" type="button">Signup</button>
          <p style="color:black;"> or signup using <a href="" style="text-decoration:none;">Google+ </a> or <a href="" style="text-decoration:none;">Outlook</a></p>
        </div>
      </form>
				</div>

      

    </div>
  </div>
</div>
                 
				
      </div>
             
            <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators"></ol>
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active" style="background-image: url('<?php echo base_url() ?>assest/img/pic2.png');">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 style="font-size:24px;">Never waste time searching for prospect emails again.</h2>
                                <p>Find any buisness email address.Target your leads with laser precision.</p>
                                <button type="submit" class="btn btn-gradient" onclick="landingnext.html">JOIN THE BETA,IT'S FREE</button>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
              


         <div class="footer">
  <p>© Copyright ?. All Rights Reserved</p>
</div>