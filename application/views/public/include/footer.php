<!--Footer-->
<footer id="footer">

<div class="container">
    <div class="copyright">
        <a href="http://saletancy.com/PrivacyPolicy" style="color:#149dcc;">Privacy Policy</a> | <a href="http://saletancy.com/Terms_conditions" style="color:#149dcc;">Terms & Conditions</a> |  Copyright &copy; 2018 Saletancy Consulting Pvt. Ltd. All Rights Reserved
    </div>
    <div class="credits">
        Made with <span style="color:red;font-size:19px">♥</span> by <a href="http://saletancy.com" style="color:#149dcc;" >Saletancy</a>
    </div>
</div>
</footer>
<!-- End of footer -->
<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<!-- JavaScript Libraries -->
<script src="<?php echo base_url('assest') ?>/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/easing/easing.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/superfish/hoverIntent.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/superfish/superfish.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/wow/wow.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/waypoints/waypoints.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/counterup/counterup.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/lightbox/js/lightbox.min.js"></script>
<script src="<?php echo base_url('assest') ?>/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
<script src="<?php echo base_url('assest') ?>/contactform/contactform.js"></script>
<script src="<?php echo base_url('assest') ?>/js/main.js"></script>
</body>
</html>