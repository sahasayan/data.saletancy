<?php include 'include/header.php'; ?>
<title>Home</title>
<?php include 'include/navbar.php'; ?>
<style>
    #intro {
        display: table;
        width: 0%;
        height: 100vh;
        background: #000;
    }
	body{
		background-image: url("//res.cloudinary.com/saletancy-com/image/upload/v1522495492/4683_mlqtse.jpg");
		background-size: 100%;
	}
    span.error {
        color: red;
        font-size: 12px;
    }
    .swal2-popup .swal2-styled.swal2-confirm {
        background-color: #50af5b;
    }
</style>
<div style="height:110px">
</div>

<script src="<?php echo base_url() ?>assest/ad_assest/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.19.3/sweetalert2.all.min.js" integrity="sha256-cyt/DHptlO1OQL33IXISo2te54rSEJoego7veSm4koc=" crossorigin="anonymous"></script>
<script type="text/javascript">

    function findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }

    $(document).ready(function() {
        var user_id=window.location.search.substr(1).substr(3);
        $('#signup_form_submit').click(function(){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>Welcome/register_user/?="+user_id,
                data: $("#signup_form").serialize(),
                success: function(res){
                    $('span.error').empty();
                    if (res !== '') {
                        res = JSON.parse(res);
                        if (res.firstname !== undefined ) {
                            $('#firstname').parent().find('span.error').text(res.firstname);
                        }

                        if (res.lastname) {
                            $('#lastname').parent().find('span.error').text(res.lastname);
                        }
                        if (res.email) {
                            $('#email').parent().find('span.error').text(res.email);
                        }
                        if (res.cpassword) {
                            $('#cpassword').parent().find('span.error').text(res.cpassword);
                        }
                        if (res.password) {
                            $('#password').parent().find('span.error').text(res.password);
                        }
                        if (res.telephone) {
                            $('#telephone').parent().find('span.error').text(res.telephone);
                        }
                    } else {
                        swal({
                            type: 'success',
                            title: 'Congratulations!',
                            text: 'You have successfully registered with us, Please check your inbox for a verification e-mail!'
                        });
                    }
                }
            });
        });


        $('#forgotPassword').click(function () {
            console.log($('#forgotPasswordEmail').val());
            $.ajax({
                url: 'Welcome/forgot_password',
                data: {
                    email: $('#forgotPasswordEmail').val()
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.error) {
                        swal({
                            type: 'error',
                            title: '',
                            text: res.message
                        });
                    } else if (res.error === false) {
                        swal({
                            type: 'success',
                            title: '',
                            text: res.message
                        });
                    }
                }
            });
        });

        if (location.hash === '#changePassword') {
            $('#modalChangePassword').modal();
        }


        $('#changePassword').click(function () {
            console.log();
            $.ajax({
                url: 'Welcome/change_password',
                method: 'POST',
                data: {
                    token: findGetParameter('token'),
                    email: findGetParameter('email'),
                    password: $('#newPassword').val()
                },
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.error) {
                        swal({
                            type: 'error',
                            title: '',
                            text: res.message
                        });
                    } else if (res.error === false) {
                        swal({
                            type: 'success',
                            title: '',
                            text: res.message
                        });
                    }
                }
            });
        });
    });
</script>
	
<!--=================================
        Body Content
===================================-->
<header class="section-header container" style="text-align: center;">
</header>
<div style="height:70px">
</div>
<header class="section-header container">
    <h3>Never Pay For Business Contact Information Again</h3>
    <p>A Free and crowd sourced database for business Contact Information</p>
</header>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="start-project-now" style="text-align:center">
                <button class="g-t-project" onclick="$('#myModal2').modal()">Join the revolution</button>
            </div>
        </div>
    </div>
</div>
<section id="service">
    <div class="container">
        <div class="row about-cols">

            <div class="col-md-4 wow fadeInUp">
                <div class="about-col single-service">
                    <div class="img">
                        <img src="//res.cloudinary.com/saletancy-com/image/upload/v1521885858/pexels-photo-262438-min_g31k8d.jpg" alt="" class="img-fluid">
                    </div>
                    <p>We take business information given to us anonymously by users and collected from
                        various data sources and combine that with a vast array of publicly crawled data through
                        Machine Learning and AI.</p>
                </div>
            </div>
            <div class="col-md-4 wow fadeInUp">
            </div>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
                <div class="about-col single-service">
                    <div class="img">
                        <img src="//res.cloudinary.com/saletancy-com/image/upload/v1522086672/saletancy_b2b_database_hjcs53.jpg" alt="" class="img-fluid">
                    </div>
                    <p>It&#39;s impossible to achieve “perfect” data quality - we know that. With the help of
                        our users, we are helping you to get Quality Emails and more direct dials.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End of Body Content-->


<div class="container">
    <!--===================================
	             Modal LOGIN
     ===================================-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!--Log In-->
                    <div class="limiter">
                        <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
                            <div class="wrap-login100 p-l-55 p-r-55 p-t-6 p-b-54" style="margin-top:-70px">
                                <form class="login100-form validate-form" method="post" action="<?php echo base_url('Login/user_login'); ?>">
                                    <span class="login100-form-title p-b-49">
                                        Login
                                    </span>
                                    <div class="flex-c-m">
                                        <a href="#" class="login100-social-item bg1">
                                            <i class="fa fa-facebook"></i>
                                        </a>

                                        <a href="#" class="login100-social-item bg2">
                                            <i class="fa fa-twitter"></i>
                                        </a>

                                        <a href="#" class="login100-social-item bg3">
                                            <i class="fa fa-google"></i>
                                        </a>
                                    </div>
                                    <div class="wrap-input100 validate-input m-b-23" data-validate="Email is reauired">
                                        <span class="label-input100">Email</span>
                                        <input class="input100" type="email" name="email" placeholder="Business Email">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                    <div class="wrap-input100 validate-input" data-validate="Password is required">
                                        <span class="label-input100">Password</span>
                                        <input class="input100" type="password" name="password" placeholder="Type your password">
                                        <span class="focus-input100" data-symbol="&#xf190;"></span>
                                    </div>
                                    <div class="text-right p-t-8 p-b-31">
                                        <a data-dismiss="modal" data-toggle="modal" href="#modalforgotPassword">
                                            Forgot password?
                                        </a>
                                    </div>
                                    <div class="container-login100-form-btn">
                                        <div class="wrap-login100-form-btn">
                                            <div class="login100-form-bgbtn"></div>
                                            <button class="login100-form-btn" type="submit">
                                                Login
                                            </button>
                                        </div>
                                    </div>
                                    <div class="txt1 text-center p-t-10 ">
                                        <span>
                                            Don't have an account? <a class="txt2" data-dismiss="modal" data-toggle="modal" href="#myModal2">Sign Up</a>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!--End of LOGIN-->

    <!--======================================
            MODAL forgot password
    =======================================-->
    <div class="modal fade" id="modalforgotPassword" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!--Log In-->
                    <div class="limiter">
                        <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
                            <div class="wrap-login100 p-l-55 p-r-55 p-t-6 p-b-54" style="margin-top:-70px">
                                <form class="login100-form validate-form" action="javascript:void(0)">
                                    <span class="login100-form-title p-b-49">
                                        Forgot Password
                                    </span>
                                    <div class="wrap-input100 validate-input m-b-23" data-validate="Email is required">
                                        <span class="label-input100">Email</span>
                                        <input class="input100" type="email" name="email" placeholder="Business Email" id="forgotPasswordEmail">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                    <div class="container-login100-form-btn">
                                        <div class="wrap-login100-form-btn">
                                            <div class="login100-form-bgbtn"></div>
                                            <button class="login100-form-btn" type="submit" id="forgotPassword">
                                                Reset
                                            </button>
                                        </div>
                                    </div>
                                    <div class="txt1 text-center p-t-10 ">
                                        <span>
                                            Don't have an account? <a class="txt2" data-dismiss="modal" data-toggle="modal" href="#myModal2">Sign Up</a>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of forgot password -->

    <!--======================================
            MODAL change password
    =======================================-->
    <div class="modal fade" id="modalChangePassword" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!--Log In-->
                    <div class="limiter">
                        <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
                            <div class="wrap-login100 p-l-55 p-r-55 p-t-6 p-b-54" style="margin-top:-70px">
                                <form class="login100-form validate-form" action="javascript:void(0)">
                                    <span class="login100-form-title p-b-49">
                                        Change Password
                                    </span>
                                    <div class="wrap-input100 validate-input m-b-23" >
                                        <span class="label-input100">Password</span>
                                        <input class="input100" type="password" name="password" placeholder="Enter a new password" id="newPassword">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                    <div class="container-login100-form-btn">
                                        <div class="wrap-login100-form-btn">
                                            <div class="login100-form-bgbtn"></div>
                                            <button class="login100-form-btn" type="submit" id="changePassword">
                                                Change
                                            </button>
                                        </div>
                                    </div>
                                    <div class="txt1 text-center p-t-10 ">
                                        <span>
                                            Don't have an account? <a class="txt2" data-dismiss="modal" data-toggle="modal" href="#myModal2">Sign Up</a>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of change password -->


    <!--==========================================
        Modal Sign Up
    ==========================================-->
<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--Log In-->
                <div class="limiter">
                    <div class="container-login100" style="background-image: url('images/bg-01.jpg');">
                        <div class="wrap-login200 p-l-55 p-r-55 p-t-6 p-b-44">
                            <form class="login100-form validate-form" style="margin-top:-50px" name="signup_form" id="signup_form">
                                <span class="login100-form-title  p-b-49">
                                    Sign Up
                                </span>
                                <div class="flex-c-m" style="margin-top:-50px">
                                    <a href="#" class="login100-social-item bg1">
                                        <i class="fa fa-facebook"></i>
                                    </a>

                                    <a href="#" class="login100-social-item bg2">
                                        <i class="fa fa-twitter"></i>
                                    </a>

                                    <a href="#" class="login100-social-item bg3">
                                        <i class="fa fa-google"></i>
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="wrap-input100 col-sm-6">
                                        <span class="label-input100"></span>
                                        <span class="error"></span>
                                        <input class="input100" type="text" name="firstname" id="firstname" placeholder="First name">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                    <div class="wrap-input100 col-sm-6">
                                        <span class="label-input100"></span>
                                        <span class="error"></span>
                                        <input class="input100" type="text" name="lastname" id="lastname" placeholder="Last name">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="wrap-input100 col-sm-6">
                                        <span class="label-input100"></span>
                                        <span class="error"></span>
                                        <input class="input100" type="text" name="email" id="email" placeholder="Business Email" data-validation="email">
                                        <span class="focus-input100" data-symbol="&#x2709;"></span>
                                    </div>
                                    <div class="wrap-input100 col-sm-6">
                                        <span class="label-input100"></span>
                                        <span class="error"></span>
                                        <input class="input100" type="text" name="company" id="company" placeholder="Company Name">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="wrap-input100 col-sm-6">
                                        <span class="label-input100"></span>
                                        <span class="error"></span>
                                        <input class="input100" type="text" name="jobtitle" id="jobtitle" placeholder="Job Title">
                                        <span class="focus-input100" data-symbol="&#xf206;"></span>
                                    </div>
                                    <div class="wrap-input100 col-sm-6">
                                        <span class="label-input100"></span>
                                        <span class="error"></span>
                                        <input class="input100" type="text" name="telephone" id="telephone" placeholder="Contact">
                                        <span class="focus-input100" data-symbol="&#xe091;"></span>
                                    </div>
                                </div>
                                <div class="wrap-input100 ">
                                    <span class="label-input100"></span>
                                    <span class="error"></span>
                                    <input class="input100" type="password" name="password" id="password" placeholder="Password">
                                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                                </div>
                                <div class="wrap-input100 " data-validate="">
                                    <span class="label-input100"></span>
                                    <span class="error"></span>
                                    <input class="input100" type="password" name="cpassword" id="cpassword" placeholder="Confirm Password">
                                    <span class="focus-input100" data-symbol="&#xf190;"></span>
                                </div>
                                <br>
                                <div class="container-login100-form-btn">
                                    <div class="wrap-login100-form-btn">
                                        <div class="login100-form-bgbtn"></div>
                                        <button class="login100-form-btn" id="signup_form_submit" type="button">
                                            Sign Up
                                        </button>
                                    </div>
                                </div>
                                <div class="txt1 text-center p-t-10 ">
                                    <span>
                                        Have an account? <a class="txt2" data-dismiss="modal" data-toggle="modal" href="#myModal">LogIn</a>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             </div>-->
        </div>
    </div>
</div>
<!--End of SIGN UP-->
</div>
<!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
        <div class="container">
            <header class="section-header">
                <h3>Our Clients</h3>
            </header>
            <div class="owl-carousel clients-carousel">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-1-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-2-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-15-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-4-min.jpg" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-5-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-6-min.jpg" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-7-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-8-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-9-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-10-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-11-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-12-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-13-min.jpg" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-14-min.png" alt="">
                <img src="//res.cloudinary.com/saletancy-com/image/upload/v1520971109/Saletancy_clients/client-3-min.png" alt="">
            </div>
        </div>
    </section><!-- #clients -->
<?php include 'include/footer.php'; ?>